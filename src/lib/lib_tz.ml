(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let mu_factor = 1_000_000.

(* (micro)µ-tez to tez

   Int64 is needed or we could end with an: Error: Integer literal exceeds the
   range of representable integers of type int *)
let mutez_to_tez mutez =
  Compute.round_by_2 @@ (Int64.to_float mutez /. mu_factor)

(* attestation weight (percentage) in the current cycle *)
let attestation_weight expected_cycle_activity total_activity =
  Compute.round_by_2
  @@ 100.
     *. Int.to_float expected_cycle_activity
     /. Int.to_float total_activity

(* Number of already missed opportunities *)
let missed_slots expected_cycle_activity minimal_cycle_activity
    remaining_allowed_missed_slots =
  let allowed_to_miss =
    Int.to_float expected_cycle_activity -. Int.to_float minimal_cycle_activity
  in
  let missed = allowed_to_miss -. Int.to_float remaining_allowed_missed_slots in
  missed

(* percentage of missed opportunities (bounded by 1/3) *)
let percent_missed expected_cycle_activity minimal_cycle_activity
    remaining_allowed_missed_slots =
  let missed =
    missed_slots expected_cycle_activity minimal_cycle_activity
      remaining_allowed_missed_slots in
  Compute.round_by_2 @@ (missed *. 100. /. Int.to_float expected_cycle_activity)

let progression_percentage cycle_position blocks_per_cycle =
  Compute.round_by_2 @@ (100. *. (cycle_position +. 1.) /. blocks_per_cycle)

let votings_progression_percentage votes consensus_committee_size =
  Compute.round_by_2
  @@ (100. *. Int.to_float votes /. Int.to_float consensus_committee_size)

let quorum_reached votes consensus_threshold = consensus_threshold <= votes

let attesting_power_percentage attesting_power consensus_committee_size =
  Compute.round_by_2
  @@ 100.
     *. Int.to_float attesting_power
     /. Int.to_float consensus_committee_size

let baker_reward reward = mutez_to_tez @@ Int64.of_float reward

let proposer_reward baking_reward_fixed_portion =
  mutez_to_tez @@ Int64.of_float baking_reward_fixed_portion

module AdaptativeIssuance = struct
  (* This module entirely depends on Q arithmetic. Never remove the following
     line: *)
  open Q

  let mutez_to_tez mutez = mutez / of_float mu_factor

  let total_weight ~attestation_rewards ~fixed_baking_reward
      ~bonus_baking_rewards ~liquidity_baking_subsidy ~nonce_revelation_tip
      ~vdf_tip =
    attestation_rewards + fixed_baking_reward + bonus_baking_rewards
    + liquidity_baking_subsidy + nonce_revelation_tip + vdf_tip

  let issuance_rate ~num ~den = num / den

  let staked_funds_ratio_from_supply ~total_supply ~staked_supply =
    staked_supply / total_supply

  let static_rate ~staked_funds_ratio =
    let staked_funds_ratio_square = staked_funds_ratio * staked_funds_ratio in
    one / of_int 1_600 / staked_funds_ratio_square

  let adaptative ~min_rate ~max_rate ~dynamic_rate ~staked_funds_ratio =
    let adaptative_unclip = dynamic_rate + static_rate ~staked_funds_ratio in
    if adaptative_unclip < min_rate then
      min_rate
    else if adaptative_unclip > max_rate then
      max_rate
    else
      adaptative_unclip

  let number_of_second_in_one_year = of_int 31_536_000 (* 365.24.60.60 *)

  (* maximum produced block with minimal delay in a year (ommiting leap
     years) *)
  let max_block_minimal_delay ~minimal_block_delay =
    number_of_second_in_one_year / minimal_block_delay

  let issuance ~min_rate ~max_rate ~dynamic_rate ~staked_funds_ratio
      ~minimal_block_delay ~total_supply =
    adaptative ~min_rate ~max_rate ~dynamic_rate ~staked_funds_ratio
    / max_block_minimal_delay ~minimal_block_delay
    * total_supply

  let baking_reward ~min_rate ~max_rate ~dynamic_rate ~staked_funds_ratio
      ~minimal_block_delay ~total_supply ~attestation_rewards
      ~fixed_baking_reward ~bonus_baking_rewards ~liquidity_baking_subsidy
      ~nonce_revelation_tip ~vdf_tip =
    let sum_weight =
      total_weight ~attestation_rewards ~fixed_baking_reward
        ~bonus_baking_rewards ~liquidity_baking_subsidy ~nonce_revelation_tip
        ~vdf_tip in
    fixed_baking_reward / sum_weight
    * issuance ~min_rate ~max_rate ~dynamic_rate ~staked_funds_ratio
        ~minimal_block_delay ~total_supply
end

module Reward = struct
  let sum_weights ~baking_reward_fixed_portion_weight
      ~baking_reward_bonus_weight ~attesting_reward_weight
      ~liquidity_baking_subsidy_weight ~seed_nonce_revelation_tip_weight
      ~vdf_revelation_tip_weight =
    baking_reward_fixed_portion_weight +. baking_reward_bonus_weight
    +. attesting_reward_weight +. liquidity_baking_subsidy_weight
    +. seed_nonce_revelation_tip_weight +. vdf_revelation_tip_weight

  let tez_from_weights ~minimal_block_delay ~base_total_rewards_per_minute
      ~baking_reward_fixed_portion_weight ~baking_reward_bonus_weight
      ~attesting_reward_weight ~liquidity_baking_subsidy_weight
      ~seed_nonce_revelation_tip_weight ~vdf_revelation_tip_weight weight =
    let normalizing reward =
      let sum =
        sum_weights ~baking_reward_fixed_portion_weight
          ~baking_reward_bonus_weight ~attesting_reward_weight
          ~liquidity_baking_subsidy_weight ~seed_nonce_revelation_tip_weight
          ~vdf_revelation_tip_weight in
      reward /. sum in
    normalizing
    @@ (base_total_rewards_per_minute *. weight *. minimal_block_delay /. 60.)

  let baking_reward_fixed_portion ~minimal_block_delay
      ~base_total_rewards_per_minute ~baking_reward_fixed_portion_weight
      ~baking_reward_bonus_weight ~attesting_reward_weight
      ~liquidity_baking_subsidy_weight ~seed_nonce_revelation_tip_weight
      ~vdf_revelation_tip_weight =
    tez_from_weights ~minimal_block_delay ~base_total_rewards_per_minute
      ~baking_reward_fixed_portion_weight ~baking_reward_bonus_weight
      ~attesting_reward_weight ~liquidity_baking_subsidy_weight
      ~seed_nonce_revelation_tip_weight ~vdf_revelation_tip_weight
      baking_reward_fixed_portion_weight

  let baking_reward_bonus_per_slot ~minimal_block_delay
      ~consensus_committee_size ~consensus_threshold
      ~base_total_rewards_per_minute ~baking_reward_fixed_portion_weight
      ~baking_reward_bonus_weight ~attesting_reward_weight
      ~liquidity_baking_subsidy_weight ~seed_nonce_revelation_tip_weight
      ~vdf_revelation_tip_weight =
    let reward =
      tez_from_weights ~minimal_block_delay ~base_total_rewards_per_minute
        ~baking_reward_fixed_portion_weight ~baking_reward_bonus_weight
        ~attesting_reward_weight ~liquidity_baking_subsidy_weight
        ~seed_nonce_revelation_tip_weight ~vdf_revelation_tip_weight
        baking_reward_bonus_weight in
    let bonus_committee_size = consensus_committee_size -. consensus_threshold in
    if Float.compare bonus_committee_size 0. <= 0 then
      0.
    else
      reward /. bonus_committee_size
end

let log_and_none log_msg =
  Log.log log_msg ;
  Pretty.none_str

let parse_tezos_time_date timestamp =
  let err_msg = __FUNCTION__ ^ " shorten failed for " in
  let time =
    try String.sub timestamp 11 8 with _ -> log_and_none @@ err_msg ^ "time"
  in
  let date =
    try String.sub timestamp 0 10 with _ -> log_and_none @@ err_msg ^ "date"
  in
  (time, date)

let parse_tezos_timestamp timestamp =
  let time, date = parse_tezos_time_date timestamp in
  let err_msg = __FUNCTION__ ^ " shorten failed for " in
  let tm_sec =
    try String.sub time 6 2 with _ -> log_and_none @@ err_msg ^ "second" in
  let tm_min =
    try String.sub time 3 2 with _ -> log_and_none @@ err_msg ^ "minute" in
  let tm_hour =
    try String.sub time 0 2 with _ -> log_and_none @@ err_msg ^ "hour" in
  let tm_mday =
    try String.sub date 8 2 with _ -> log_and_none @@ err_msg ^ "day" in
  let tm_mon =
    try String.sub date 5 2 with _ -> log_and_none @@ err_msg ^ "month" in
  let tm_year =
    try String.sub date 0 4 with _ -> log_and_none @@ err_msg ^ "year" in
  try
    fst
    @@ Unix.(
         mktime
           {
             tm_sec = int_of_string tm_sec;
             tm_min = int_of_string tm_min;
             tm_hour = int_of_string tm_hour;
             tm_mday = int_of_string tm_mday;
             tm_mon = int_of_string tm_mon - 1;
             tm_year = int_of_string tm_year - 1900;
             (* these are ignored : *)
             tm_wday = -1;
             tm_yday = -1;
             tm_isdst = false;
           })
  with exn ->
    let msg =
      Log.log_sf
        "time = %s\n\
         date = %s\n\
         tm_sec = %s\n\
         tm_min = %s\n\
         tm_hour = %s\n\
         tm_mday = %s\n\
         tm_mon = %s\n\
         tm_year = %s" time date tm_sec tm_min tm_hour tm_mday tm_mon tm_year
    in
    Log.log_exn ~msg ~__FUNCTION__ ~exn () ;
    raise exn

let forbidden_chars = ['0'; 'I'; 'O'; 'l']

(* This is not ideal, dirty but quick, and more importantly enough. *)
let is_address_valid address =
  List.fold_left ( && ) true
  @@ List.map (fun c -> not @@ String.contains address c) forbidden_chars
  && (match String.sub address 0 3 with
     | "tz1" | "tz2" | "tz3" | "tz4" -> true
     | _ -> false)
  && String.length address == 36
