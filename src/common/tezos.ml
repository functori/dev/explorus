(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
[@@@ocamlformat "wrap-comments=false"]
(* @@ File overview :
   ==> RPC
   ------> Types
   ..................> SORU
   ------> Services
   ..................> Params
   ..................> SORU
   ==> Entities
   ------> Attesters *)

module Encoding = struct
  open Json_encoding

  let ignore_enc enc =
    let destruct x = (x, ()) in
    let construct (x, ()) = x in
    let enc = merge_objs enc unit in
    conv destruct construct enc

  let float_of_string_enc = conv Float.to_string Float.of_string string

  let int_of_string_enc = conv string_of_int int_of_string string

  let int64_of_string_enc = conv Int64.to_string Int64.of_string string

  let z_enc = conv Z.to_string Z.of_string string

  let singleton_enc enc =
    let open Json_encoding in
    conv
      (fun x -> [x])
      (function
        | [x] -> x
        | _ -> raise (Cannot_destruct ([], Failure "is not a singleton")))
      (list enc)
end

module RPC = struct
  module Types = struct
    module Dal = struct
      type header_id = {
        hi_published_level : int; [@key "level"]
        hi_index : int;
      }
      [@@deriving jsoo, encoding { ignore }]

      type header = {
        h_id : header_id; [@merge]
        h_commitment : string;
      }
      [@@deriving jsoo, encoding { ignore }]

      type metadata_slot_header = {
        msh_slot_header : header; [@key "slot_header"]
      }
      [@@deriving jsoo, encoding { ignore }]

      type slot_header = {
        sh_slot_index : int;
        sh_commitment : string;
      }
      [@@deriving jsoo, encoding { ignore }]
    end

    module SORU = struct
      module L1 = struct
        type ('content, 'ptr) skip_list = {
          sl_content : 'content;
          sl_back_pointers : 'ptr list;
          sl_index : int; [@encoding Encoding.int_of_string_enc]
        }
        [@@deriving jsoo, encoding { ignore }]

        type genesis_info = {
          gi_level : int;
          gi_commitment_hash : string;
        }
        [@@deriving jsoo, encoding { ignore }]

        type level_proof = {
          lp_hash : string;
          lp_level : int;
        }
        [@@deriving jsoo, encoding { ignore }]

        type old_levels_messages = (level_proof, string) skip_list
        [@@deriving jsoo, encoding { ignore }]

        type rollups_inbox = {
          ri_level : int;
          ri_old_levels_messages : old_levels_messages;
        }
        [@@deriving jsoo, encoding { ignore }]

        type commitment_infos = {
          compressed_state : string;
          inbox_level : int;
          predecessor : string;
          number_of_ticks : string;
        }
        [@@deriving jsoo, encoding { ignore }]

        type commitment_infos_with_hash = {
          cih_info : commitment_infos; [@merge]
          cih_hash : string;
        }
        [@@deriving jsoo, encoding { ignore }]

        type soru_addresses = string list [@@deriving jsoo, encoding]

        type commitment = string [@@deriving jsoo, encoding]

        type commitments = commitment list option [@@deriving jsoo, encoding]

        type staker = string [@@deriving jsoo, encoding]

        type stakers = staker list [@@deriving jsoo, encoding]

        type staker_id = string [@@deriving jsoo, encoding]

        type stakers_ids = staker_id list [@@deriving jsoo, encoding]

        type kind = string [@@deriving jsoo, encoding]

        type conflict = {
          c_other : staker;
          c_their_commitment : commitment_infos;
          c_our_commitment : commitment_infos;
          c_parent_commitment : commitment;
        }
        [@@deriving jsoo, encoding { ignore }]

        type conflicts = conflict list [@@deriving jsoo, encoding]

        type timeout = {
          t_alice : int;
          t_bob : int;
          t_last_turn_level : int;
        }
        [@@deriving jsoo, encoding]

        type timeout_opt = timeout option [@@deriving jsoo, encoding]

        type player =
          | Alice
          | Bob
        [@@deriving jsoo, encoding]

        type game_dissection_chunk = {
          gdc_state : string option;
          gdc_tick : int64; [@encoding Encoding.int64_of_string_enc]
        }
        [@@deriving jsoo, encoding { ignore }]

        type game_dissecting = {
          gd_dissection : game_dissection_chunk list;
          gd_number_of_sections : int; [@key "default_number_of_sections"]
        }
        [@@deriving jsoo, encoding { ignore }]

        type game_final_move = {
          gfm_agreed_start_chunk : game_dissection_chunk;
          gfm_refuted_stop_chunk : game_dissection_chunk;
        }
        [@@deriving jsoo, encoding { ignore }]

        type game_state =
          | Dissecting of game_dissecting [@kind "Dissecting"]
          | Final_move of game_final_move [@kind "Final_move"]
        [@@deriving jsoo, encoding { ignore }]

        type inbox_history_proof = (level_proof, string) skip_list
        [@@deriving jsoo, encoding { ignore }]

        type game = {
          g_turn : player;
          g_inbox_snapshot : inbox_history_proof;
          g_start_level : int;
          g_inbox_level : int;
          g_game_state : game_state;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_game = {
          rg_game : game;
          rg_alice : staker;
          rg_bob : staker;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_games = refutation_game list [@@deriving jsoo, encoding]

        type refutation_game_players = {
          rgp_alice : staker;
          rgp_bob : staker;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_start = {
          rs_player_commitment_hash : commitment;
          rs_opponent_commitment_hash : commitment;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_proof = { pvm_step : string }
        [@@deriving jsoo, encoding { ignore }]

        type refutation_step =
          | Dissection of game_dissection_chunk list
          | Proof of refutation_proof
        [@@deriving jsoo, encoding]

        type refutation_move = {
          rm_choice : int64; [@encoding Encoding.int64_of_string_enc]
          rm_step : refutation_step;
        }
        [@@deriving jsoo, encoding { ignore }]

        type refutation =
          | Start of refutation_start [@kind] [@kind_label "refutation_kind"]
          | Move of refutation_move [@kind] [@kind_label "refutation_kind"]
        [@@deriving jsoo, encoding { ignore }]

        type game_lost_reason =
          | Conflict_resolved
          | Timeout
        [@@deriving jsoo { enum }, encoding]

        type game_result =
          | Loser of {
              reason : game_lost_reason;
              player : staker;
            } [@kind]
          | Draw [@kind]
        [@@deriving jsoo, encoding]

        type game_status =
          | Ongoing
          | Ended of game_result [@wrap "result"]
        [@@deriving jsoo, encoding]

        type refutation_game_metadata = { game_status : game_status }
        [@@deriving jsoo, encoding { ignore }]

        module Cosmetics = struct
          type inbox_level = int

          type commitments = string list
        end

        module C = Cosmetics
      end
    end

    type level_info = {
      cycle : int;
      cycle_position : int;
    }
    [@@deriving
      jsoo { remove_prefix = false }, encoding { ignore; remove_prefix = false }]

    type dec_hex = {
      dh_dec : int;
      dh_hex : string;
    }
    [@@deriving jsoo]

    type dec64_hex = {
      dh64_dec : int64;
      dh64_hex : string;
    }
    [@@deriving jsoo]

    let dec_hex_enc =
      let open Json_encoding in
      let destruct { dh_hex; _ } = dh_hex in
      let construct dh_hex =
        try { dh_dec = Hex.to_int dh_hex; dh_hex }
        with exn -> raise (Json_encoding.Cannot_destruct ([], exn)) in
      conv destruct construct string

    let dec_hex_enc_opt =
      let open Json_encoding in
      let destruct = function
        | None -> ""
        | Some { dh_hex; _ } -> dh_hex in
      let construct dh_hex =
        try Some { dh_dec = Hex.to_int dh_hex; dh_hex } with _ -> None in
      conv destruct construct string

    let dec64_hex_enc =
      let open Json_encoding in
      let destruct { dh64_hex; _ } = dh64_hex in
      let construct dh64_hex =
        try { dh64_dec = Hex.to_int64 dh64_hex; dh64_hex }
        with exn -> raise (Json_encoding.Cannot_destruct ([], exn)) in
      conv destruct construct string

    let dec64_hex_enc_opt =
      let open Json_encoding in
      let destruct = function
        | None -> ""
        | Some { dh64_hex; _ } -> dh64_hex in
      let construct dh64_hex =
        try Some { dh64_dec = Hex.to_int64 dh64_hex; dh64_hex } with _ -> None
      in
      conv destruct construct string

    module Fitness_version = struct
      let emmy = "00"

      let emmy_star = "01"

      let tenderbake = "02"
    end

    type tenderbake_fitness = {
      (* current level of block but in hexadecimal *)
      ft_current_level_hexa : dec_hex;
      (* ε if there is no pqc, else case previous round of pqc *)
      ft_previous_round_of_pqc : dec_hex option;
      (* we "prioritize" the oldest previous round *)
      ft_minus_previous_round : dec_hex;
      (* we "prioritize" the freshest current round *)
      ft_current_round : dec_hex;
    }
    [@@deriving jsoo]

    (* Fitness deconstructed into a readable and understandable type *)
    type fitness =
      | Genesis
      | Emmy of dec64_hex
      | Tenderbake of tenderbake_fitness
    [@@deriving jsoo]

    let fitness_enc =
      let open Json_encoding in
      let genesis =
        case ~title:"genesis" (list empty)
          (function
            | Genesis -> Some []
            | _ -> None)
          (fun _ -> Genesis) in
      let emmy =
        case ~title:"emmy"
          (tup2 (constant Fitness_version.emmy) dec64_hex_enc)
          (function
            | Emmy dec64_hex -> Some ((), dec64_hex)
            | _ -> None)
          (fun ((), dec64_hex) -> Emmy dec64_hex) in
      let tenderbake =
        case ~title:"tenderbake"
          (tup5
             (constant Fitness_version.tenderbake)
             dec_hex_enc dec_hex_enc_opt dec_hex_enc dec_hex_enc)
          (function
            | Tenderbake
                {
                  ft_current_level_hexa;
                  ft_previous_round_of_pqc;
                  ft_minus_previous_round;
                  ft_current_round;
                } ->
              Some
                ( (),
                  ft_current_level_hexa,
                  ft_previous_round_of_pqc,
                  ft_minus_previous_round,
                  ft_current_round )
            | _ -> None)
          (fun ( (),
                 ft_current_level_hexa,
                 ft_previous_round_of_pqc,
                 ft_minus_previous_round,
                 ft_current_round ) ->
            Tenderbake
              {
                ft_current_level_hexa;
                ft_previous_round_of_pqc;
                ft_minus_previous_round;
                ft_current_round;
              }) in
      union [genesis; emmy; tenderbake]

    type header = {
      hd_level : int;
      hd_predecessor : string;
      hd_timestamp : string;
      hd_fitness : fitness;
    }
    [@@deriving jsoo, encoding { ignore }]

    (* balance_kind :
       { kind: "contract"; ... } will be [Contract]
       { kind: "minted"; category: "baking bonuses"; ... } will be [Minted
       Baking_bonuses]
       { kind: "minted"; category: ...; ... } will be [Minted
       OtherBalanceMinted]
       { kind: ...; ... } will be [OtherBalanceKind] *)
    type balance_minted =
      | Baking_bonuses [@kind "baking bonuses"] [@kind_label "category"]
      | OtherBalanceMinted of { category : string } [@ignore]
    [@@deriving jsoo, encoding { ignore }]

    type balance_kind =
      | Contract [@kind]
      | Minted of balance_minted [@kind]
      | OtherBalanceKind of { kind : string } [@ignore]
    [@@deriving jsoo, encoding { ignore }]

    type balance_update = {
      bu_kind : balance_kind; [@merge]
      bu_change : float; [@encoding Encoding.float_of_string_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    type balance_updates = balance_update list [@@deriving jsoo, encoding]

    type metadata = {
      meta_baker : string option;
      meta_proposer : string option;
      meta_level_info : level_info option;
      meta_balance_updates : balance_updates option;
      meta_dal_attestation : Z.t option; [@encoding Encoding.z_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    type co_content_metadata = {
      cocm_consensus_power : int;
      cocm_delegate : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    let (co_content_metadata_enc : co_content_metadata Json_encoding.encoding) =
      let open Json_encoding in
      conv
        (fun x -> (x, ()))
        (fun (x, ()) -> x)
        (merge_objs
           (conv
              (fun { cocm_consensus_power; cocm_delegate } ->
                (cocm_consensus_power, cocm_delegate))
              (fun (cocm_consensus_power, cocm_delegate) ->
                { cocm_consensus_power; cocm_delegate })
              (union
                 [
                   case
                     (obj2 (req "consensus_power" int) (req "delegate" string))
                     Option.some Fun.id;
                   case
                     (obj2
                        (req "endorsement_power" int)
                        (req "delegate" string))
                     Option.some Fun.id;
                   case
                     (obj2
                        (req "preendorsement_power" int)
                        (req "delegate" string))
                     Option.some Fun.id;
                 ]))
           unit)

    type 'successful operation_result =
      | Applied of 'successful [@kind] [@kind_label "status"]
      | OtherOperationResult of { status : string } [@ignore]
    [@@deriving jsoo, encoding]

    type 'successful operation_metadata = {
      operation_result : 'successful operation_result;
    }
    [@@deriving jsoo, encoding { ignore }]

    type consensus_op_content = {
      coc_slot : int;
      coc_level : int;
      coc_round : int;
      coc_metadata : co_content_metadata option;
      coc_dal_attestation : Z.t option; [@encoding Encoding.z_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    type manager = { man_source : string [@key "source"] }
    [@@deriving jsoo, encoding { ignore }]

    type parameters = { entrypoint : string }
    [@@deriving jsoo, encoding { ignore }]

    type contract_transaction = {
      ctr_manager : manager; [@merge]
      ctr_amount : string;
      ctr_parameters : parameters; [@dft { entrypoint = "default" }]
      ctr_destination : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    let contract_transaction_enc =
      let destruct transaction = transaction in
      let construct ({ ctr_destination; _ } as transaction) =
        if String.starts_with ~prefix:"KT" ctr_destination then
          transaction
        else
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "The destination is not a contract")) in
      Json_encoding.conv destruct construct contract_transaction_enc

    type user_transaction = {
      utr_manager : manager; [@merge]
      utr_amount : string;
      utr_destination : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    let user_transaction_enc =
      let destruct transaction = transaction in
      let construct transaction =
        if not @@ String.starts_with ~prefix:"tz" transaction.utr_destination
        then
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "The destination is not a user"))
        else
          transaction in
      Json_encoding.conv destruct construct user_transaction_enc

    type origination = { or_manager : manager [@key "manager"] [@merge] }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_originate = {
      sro_manager : manager; [@key "manager"] [@merge]
    }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_refute = {
      srr_manager : manager; [@merge]
      srr_rollup : string;
      srr_opponent : string;
      srr_refutation : SORU.L1.refutation;
      srr_metadata : SORU.L1.refutation_game_metadata operation_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type smart_rollup_timeout = {
      srt_manager : manager; [@merge]
      srt_rollup : string;
      srt_stakers : SORU.L1.refutation_game_players;
      srt_metadata : SORU.L1.refutation_game_metadata operation_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type dal_publish_commitment = {
      dpc_manager : manager; [@merge]
      dpc_slot_header : Dal.slot_header;
      dpc_metadata : Dal.metadata_slot_header operation_metadata option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type ignored_operation_content =
      | Seed_nonce_revelation
      | Vdf_revelation
      | Double_baking_evidence
      | Activate_account
      | Proposals
      | Ballot
      | Reveal
      | Delegation
      | Set_deposits_limit
      | Increase_paid_storage
      | Update_consensus_key
      | Drain_delegate
      | Failing_noop
      | Register_global_constant
      | Transfer_ticket
      | Smart_rollup_add_messages
      | Smart_rollup_cement
      | Smart_rollup_publish
      | Smart_rollup_execute_outbox_message
      | Smart_rollup_recover_bond
      | Zk_rollup_origination
      | Zk_rollup_publish
      | Zk_rollup_update
      | Double_preattestation_evidence
      | Double_attestation_evidence
      | Attestations_aggregate
      | Dal_entrapment_evidence
    [@@deriving jsoo, encoding]

    let ignored_operation_content_enc =
      let open Json_encoding in
      let ignored_case name op =
        case ~title:name ?description:None
          (Encoding.ignore_enc (obj1 (req "kind" (constant name))))
          (fun op' ->
            if op == op' then
              Some ()
            else
              None)
          (fun () -> op) in
      let unknown_operation_fail _ =
        raise (Cannot_destruct ([], Failure "Unknown operation")) in
      union
        [
          ignored_case "seed_nonce_revelation" Seed_nonce_revelation;
          ignored_case "vdf_revelation" Vdf_revelation;
          ignored_case "double_baking_evidence" Double_baking_evidence;
          ignored_case "activate_account" Activate_account;
          ignored_case "proposals" Proposals;
          ignored_case "ballot" Ballot;
          ignored_case "reveal" Reveal;
          ignored_case "delegation" Delegation;
          ignored_case "set_deposits_limit" Set_deposits_limit;
          ignored_case "increase_paid_storage" Increase_paid_storage;
          ignored_case "update_consensus_key" Update_consensus_key;
          ignored_case "drain_delegate" Drain_delegate;
          ignored_case "failing_noop" Failing_noop;
          ignored_case "register_global_constant" Register_global_constant;
          ignored_case "transfer_ticket" Transfer_ticket;
          ignored_case "smart_rollup_add_messages" Smart_rollup_add_messages;
          ignored_case "smart_rollup_cement" Smart_rollup_cement;
          ignored_case "smart_rollup_publish" Smart_rollup_publish;
          ignored_case "smart_rollup_execute_outbox_message"
            Smart_rollup_execute_outbox_message;
          ignored_case "smart_rollup_recover_bond" Smart_rollup_recover_bond;
          ignored_case "zk_rollup_origination" Zk_rollup_origination;
          ignored_case "zk_rollup_publish" Zk_rollup_publish;
          ignored_case "zk_rollup_update" Zk_rollup_update;
          ignored_case "double_preattestation_evidence"
            Double_preattestation_evidence;
          ignored_case "double_attestation_evidence" Double_attestation_evidence;
          ignored_case "double_preendorsement_evidence"
            Double_preattestation_evidence;
          ignored_case "double_endorsement_evidence" Double_attestation_evidence;
          ignored_case "attestations_aggregate" Attestations_aggregate;
          ignored_case "dal_entrapment_evidence" Dal_entrapment_evidence;
          case ~title:"Unknown operation" ?description:None
            (Encoding.ignore_enc
               (conv unknown_operation_fail
                  (fun kind ->
                    let exn = Failure ("Unknown operation " ^ kind) in
                    Log.log_exn ~__FUNCTION__ ~exn () ;
                    raise (Cannot_destruct ([], exn)))
                  (obj1 (req "kind" string))))
            unknown_operation_fail unknown_operation_fail;
        ]

    type operation_content =
      | Preattestation of consensus_op_content [@kind]
      | Attestation of consensus_op_content [@kind]
      | Contract_transaction of contract_transaction [@kind "transaction"]
      | User_transaction of user_transaction [@kind "transaction"]
      | Origination of origination [@kind]
      | Smart_rollup_originate of smart_rollup_originate [@kind]
      | Smart_rollup_refute of smart_rollup_refute [@kind]
      | Smart_rollup_timeout of smart_rollup_timeout [@kind]
      | Dal_publish_commitment of dal_publish_commitment [@kind]
      | Ignored_operation of ignored_operation_content
    [@@deriving jsoo, encoding]

    let (operation_content_enc : operation_content Json_encoding.encoding) =
      let open Json_encoding in
      union
        [
          case ~title:"preattestation" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "preattestation")))
               consensus_op_content_enc)
            (function
              | Preattestation v13 -> Some ((), v13)
              | _ -> None)
            (fun ((), v13) -> Preattestation v13);
          case ~title:"attestation" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "attestation")))
               consensus_op_content_enc)
            (function
              | Attestation v14 -> Some ((), v14)
              | _ -> None)
            (fun ((), v14) -> Attestation v14);
          case ~title:"attestation_with_dal" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "attestation_with_dal")))
               consensus_op_content_enc)
            (function
              | Attestation v14 -> Some ((), v14)
              | _ -> None)
            (fun ((), v14) -> Attestation v14);
          case ~title:"preendorsement" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "preendorsement")))
               consensus_op_content_enc)
            (function
              | Preattestation v13 -> Some ((), v13)
              | _ -> None)
            (fun ((), v13) -> Preattestation v13);
          case ~title:"endorsement" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "endorsement")))
               consensus_op_content_enc)
            (function
              | Attestation v14 -> Some ((), v14)
              | _ -> None)
            (fun ((), v14) -> Attestation v14);
          case ~title:"endorsement_with_dal" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "endorsement_with_dal")))
               consensus_op_content_enc)
            (function
              | Attestation v14 -> Some ((), v14)
              | _ -> None)
            (fun ((), v14) -> Attestation v14);
          case ~title:"contract_transaction" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "transaction")))
               contract_transaction_enc)
            (function
              | Contract_transaction v15 -> Some ((), v15)
              | _ -> None)
            (fun ((), v15) -> Contract_transaction v15);
          case ~title:"user_transaction" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "transaction")))
               user_transaction_enc)
            (function
              | User_transaction v16 -> Some ((), v16)
              | _ -> None)
            (fun ((), v16) -> User_transaction v16);
          case ~title:"origination" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "origination")))
               origination_enc)
            (function
              | Origination v17 -> Some ((), v17)
              | _ -> None)
            (fun ((), v17) -> Origination v17);
          case ~title:"smart_rollup_originate" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "smart_rollup_originate")))
               smart_rollup_originate_enc)
            (function
              | Smart_rollup_originate v18 -> Some ((), v18)
              | _ -> None)
            (fun ((), v18) -> Smart_rollup_originate v18);
          case ~title:"smart_rollup_refute" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "smart_rollup_refute")))
               smart_rollup_refute_enc)
            (function
              | Smart_rollup_refute v19 -> Some ((), v19)
              | _ -> None)
            (fun ((), v19) -> Smart_rollup_refute v19);
          case ~title:"smart_rollup_timeout" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "smart_rollup_timeout")))
               smart_rollup_timeout_enc)
            (function
              | Smart_rollup_timeout v20 -> Some ((), v20)
              | _ -> None)
            (fun ((), v20) -> Smart_rollup_timeout v20);
          case ~title:"dal_publish_commitment" ?description:None
            (merge_objs
               (obj1 (req "kind" (constant "dal_publish_commitment")))
               dal_publish_commitment_enc)
            (function
              | Dal_publish_commitment v21 -> Some ((), v21)
              | _ -> None)
            (fun ((), v21) -> Dal_publish_commitment v21);
          case ~title:"ignored_operation" ?description:None
            ignored_operation_content_enc
            (function
              | Ignored_operation content -> Some content
              | _ -> None)
            (fun content -> Ignored_operation content);
        ]

    type operation = {
      op_hash : string;
      op_contents : operation_content list;
    }
    [@@deriving jsoo, encoding { ignore }]

    type operations = {
      ops_attestations : operation list;
      ops_votings : operation list;
      ops_revealations_walletactivations_denunciations : operation list;
      ops_monitor_ops : operation list;
    }
    [@@deriving jsoo]

    let operations_enc =
      let open Json_encoding in
      let destruct
          {
            ops_attestations;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          } =
        [
          ops_attestations;
          ops_votings;
          ops_revealations_walletactivations_denunciations;
          ops_monitor_ops;
        ] in
      let construct = function
        | [
            ops_attestations;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          ] ->
          {
            ops_attestations;
            ops_votings;
            ops_revealations_walletactivations_denunciations;
            ops_monitor_ops;
          }
        | [] ->
          {
            ops_attestations = [];
            ops_votings = [];
            ops_revealations_walletactivations_denunciations = [];
            ops_monitor_ops = [];
          }
        | _ :: _ :: _ :: _ :: _ :: _ ->
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "Too many operations list"))
        | _ ->
          raise
            (Json_encoding.Cannot_destruct
               ([], Failure "Not enough operations list")) in
      conv destruct construct (list (list operation_enc))

    type block = {
      bl_protocol : string;
      bl_chain_id : string;
      bl_hash : string;
      bl_header : header;
      bl_metadata : metadata option;
      bl_operations : operations;
    }
    [@@deriving jsoo, encoding { ignore }]

    type delegates = string list [@@deriving jsoo, encoding]

    type participation = {
      pr_expected_cycle_activity : int;
      pr_minimal_cycle_activity : int;
      pr_missed_slots : int;
      pr_missed_levels : int;
      pr_remaining_allowed_missed_slots : int;
      pr_expected_attesting_rewards : int64;
          [@encoding Encoding.int64_of_string_enc]
    }
    [@@deriving jsoo, encoding { ignore }]

    let (participation_enc : participation Json_encoding.encoding) =
      let open Json_encoding in
      conv
        (fun x -> (x, ()))
        (fun (x, ()) -> x)
        (merge_objs
           (conv
              (fun {
                     pr_expected_cycle_activity;
                     pr_minimal_cycle_activity;
                     pr_missed_slots;
                     pr_missed_levels;
                     pr_remaining_allowed_missed_slots;
                     pr_expected_attesting_rewards;
                   } ->
                ( pr_expected_cycle_activity,
                  pr_minimal_cycle_activity,
                  pr_missed_slots,
                  pr_missed_levels,
                  pr_remaining_allowed_missed_slots,
                  pr_expected_attesting_rewards ))
              (fun ( pr_expected_cycle_activity,
                     pr_minimal_cycle_activity,
                     pr_missed_slots,
                     pr_missed_levels,
                     pr_remaining_allowed_missed_slots,
                     pr_expected_attesting_rewards ) ->
                {
                  pr_expected_cycle_activity;
                  pr_minimal_cycle_activity;
                  pr_missed_slots;
                  pr_missed_levels;
                  pr_remaining_allowed_missed_slots;
                  pr_expected_attesting_rewards;
                })
              (union
                 [
                   case
                     (obj6
                        (req "expected_cycle_activity" int)
                        (req "minimal_cycle_activity" int)
                        (req "missed_slots" int) (req "missed_levels" int)
                        (req "remaining_allowed_missed_slots" int)
                        (req "expected_attesting_rewards"
                           Encoding.int64_of_string_enc))
                     Option.some Fun.id;
                   case
                     (obj6
                        (req "expected_cycle_activity" int)
                        (req "minimal_cycle_activity" int)
                        (req "missed_slots" int) (req "missed_levels" int)
                        (req "remaining_allowed_missed_slots" int)
                        (req "expected_endorsing_rewards"
                           Encoding.int64_of_string_enc))
                     Option.some Fun.id;
                 ]))
           unit)

    type participations = participation list [@@deriving jsoo]

    type delegate_info = {
      di_total_staked : int64; [@encoding Encoding.int64_of_string_enc]
      di_total_delegated : int64; [@encoding Encoding.int64_of_string_enc]
      di_delegators : string list;
      di_deactivated : bool;
      di_grace_period : int;
    }
    [@@deriving jsoo, encoding { ignore }]

    type delegate_infos = delegate_info list [@@deriving jsoo]

    type validator = {
      val_level : int;
      val_delegate : string;
      val_slots : int list;
    }
    [@@deriving jsoo, encoding { ignore }]

    type validators = validator list [@@deriving jsoo, encoding]

    type mo_error = { moe_kind : string } [@@deriving jsoo, encoding { ignore }]

    type monitor_operation = {
      mo_hash : string;
      mo_contents : operation_content list;
      mo_errors : mo_error list option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type monitor_operations = monitor_operation list [@@deriving jsoo, encoding]

    type monitor_head = {
      mh_hash : string;
      mh_level : int;
      mh_timestamp : string;
      mh_receipt_timestamp : float;
      mh_fitness : fitness;
    }
    [@@deriving encoding { ignore }]

    let monitor_head_enc =
      let open Json_encoding in
      let destruct { mh_hash; mh_level; mh_timestamp; mh_fitness; _ } =
        (mh_hash, mh_level, mh_timestamp, mh_fitness) in
      let construct (mh_hash, mh_level, mh_timestamp, mh_fitness) =
        let mh_receipt_timestamp = Time.universal () in
        { mh_hash; mh_level; mh_timestamp; mh_receipt_timestamp; mh_fitness }
      in
      Encoding.ignore_enc
      @@ conv destruct construct
           (obj4 (req "hash" string) (req "level" int) (req "timestamp" string)
              (req "fitness" fitness_enc))

    type monitor_heads = monitor_head list [@@deriving encoding]

    type issuance_weights = {
      rw_base_total_issued_per_minute : float;
          [@encoding Encoding.float_of_string_enc]
      rw_baking_reward_fixed_portion_weight : float;
      rw_baking_reward_bonus_weight : float;
      rw_attesting_reward_weight : float;
      rw_liquidity_baking_subsidy_weight : float option;
      rw_seed_nonce_revelation_tip_weight : float;
      rw_vdf_revelation_tip_weight : float;
    }
    [@@deriving jsoo, encoding { ignore }]

    type ratio = {
      numerator : string;
      denominator : string;
    }
    [@@deriving jsoo, encoding { ignore }]

    type adaptive_rewards_params = {
      (* @todo: to be removed when no longer in use: { *)
      issuance_ratio_min : ratio option;
      issuance_ratio_max : ratio option;
      (* } and stop using option *)
      issuance_ratio_final_min : ratio option;
      issuance_ratio_final_max : ratio option;
    }
    [@@deriving jsoo, encoding { ignore; remove_prefix = false }]

    type dal_parametric = {
      dp_feature_enable : bool;
      dp_number_of_slots : int;
      dp_attestation_lag : int;
    }
    [@@deriving jsoo, encoding { ignore }]

    (* @todo: Use the Alpha version when Quebec will be deprecated *)
    let consensus_threshold_enc =
      let open Json_encoding in
      let quebec =
        case ~title:"quebec"
          (obj1 (req "consensus_threshold" int))
          Option.some Fun.id in
      let alpha =
        case ~title:"alpha"
          (obj1 (req "consensus_threshold_size" int))
          Option.some Fun.id in
      union [quebec; alpha]

    type network_constants = {
      nc_blocks_per_cycle : int;
      (* @todo: to be removed when no longer in use *)
      nc_baking_reward_fixed_portion : float option;
          [@encoding Encoding.float_of_string_enc]
      nc_baking_reward_bonus_per_slot : float option;
          [@encoding Encoding.float_of_string_enc]
      nc_issuance_weights : issuance_weights option;
      nc_liquidity_baking_subsidy : Z.t option; [@encoding Encoding.z_enc]
      nc_consensus_committee_size : int;
      nc_consensus_threshold : int; [@encoding consensus_threshold_enc] [@merge]
      nc_minimal_block_delay : int; [@encoding Encoding.int_of_string_enc]
      nc_delay_increment_per_round : int; [@encoding Encoding.int_of_string_enc]
      nc_dal_parametric : dal_parametric option;
      nc_smart_rollup_timeout_period_in_blocks : int option;
      nc_smart_rollup_commitment_period_in_blocks : int option;
      nc_smart_rollup_challenge_window_in_blocks : int option;
      nc_adaptive_rewards_params : adaptive_rewards_params option;
    }
    [@@deriving jsoo, encoding { ignore }]

    type issuance = {
      baking_reward_fixed_portion : string;
      baking_reward_bonus_per_slot : string;
    }
    [@@deriving jsoo, encoding { ignore; remove_prefix = false }]

    type total_supply = string [@@deriving encoding]

    type dynamic_rate = string [@@deriving encoding]

    type total_frozen_stake = string [@@deriving encoding]

    type expected_issuance = issuance list [@@deriving encoding]

    type commit_info = { commit_hash : string }
    [@@deriving jsoo, encoding { ignore }]

    type network_version = { chain_name : string }
    [@@deriving jsoo, encoding { ignore }]

    type node_version = {
      commit_info : commit_info option;
      network_version : network_version;
    }
    [@@deriving jsoo, encoding { ignore }]
  end

  module Error = struct
    type t = { id : string } [@@deriving encoding { ignore }]

    let make_err ~name ~exn ~id =
      EzAPI.Err.make ~code:500 ~name
        ~encoding:(Encoding.singleton_enc @@ enc)
        ~select:(function
          | exn' when exn' = exn -> Some { id }
          | _ -> None)
        ~deselect:(function
          | err when String.ends_with ~suffix:id err.id -> exn
          | _ ->
            raise (Json_encoding.Cannot_destruct ([], Failure "wrong encoding")))

    exception Unknown_seed

    (* example: [{ id: "proto.alpha.seed.unknown_seed"; ... }] *)
    let unknown_seed =
      make_err ~name:"Unknown seed" ~exn:Unknown_seed ~id:"seed.unknown_seed"

    exception Storage_error

    (* example: [{ id: "proto.alpha.context.storage_error"; ... }] *)
    let storage_error =
      make_err ~name:"Storage error" ~exn:Storage_error
        ~id:"context.storage_error"

    exception Smart_rollup_unknown_commitment

    (* example: [{ id: "proto.alpha.smart_rollup_unknown_commitment"; ... }] *)
    let smart_rollup_unknown_commitment =
      make_err ~name:"Smart rollup unknown commitment"
        ~exn:Smart_rollup_unknown_commitment
        ~id:"smart_rollup_unknown_commitment"
  end

  module Services = struct
    module Params = struct
      let active_param = EzAPI.Param.string "active"

      let make_active_param ?active () =
        Option.map (fun active -> (active_param, EzAPI.TYPES.B active)) active

      let level_param = EzAPI.Param.string "level"

      let make_level_param ?level () =
        Option.map (fun level -> (level_param, EzAPI.TYPES.I level)) level

      let applied_param = EzAPI.Param.string "applied"

      let make_applied_param ?applied () =
        Option.map
          (fun applied -> (applied_param, EzAPI.TYPES.B applied))
          applied

      let outdated_param = EzAPI.Param.string "outdated"

      let make_outdated_param ?outdated () =
        Option.map
          (fun outdated -> (outdated_param, EzAPI.TYPES.B outdated))
          outdated

      let get_params params = List.filter_map Fun.id params
    end

    [@@@get
    {
      name = "block";
      path = "/chains/main/blocks/{blockid : string}";
      output = Types.block_enc;
    }]

    [@@@get
    {
      name = "delegates";
      path = "/chains/main/blocks/{blockid : string}/context/delegates";
      params = Params.[active_param];
      output = Types.delegates_enc;
    }]

    [@@@get
    {
      name = "participation";
      path =
        "/chains/main/blocks/{blockid : string}/context/delegates/{delegate : \
         string}/participation";
      output = Types.participation_enc;
    }]

    [@@@get
    {
      name = "delegate_info";
      path =
        "/chains/main/blocks/{blockid : string}/context/delegates/{delegate : \
         string}";
      output = Types.delegate_info_enc;
    }]

    [@@@get
    {
      name = "validators";
      path = "/chains/main/blocks/{blockid : string}/helpers/validators";
      params = Params.[level_param];
      output = Types.validators_enc;
      errors = Error.[unknown_seed; storage_error];
    }]

    [@@@get
    {
      name = "monitor_operations";
      path = "/chains/main/mempool/monitor_operations";
      params = Params.[applied_param; outdated_param];
      output = Types.monitor_operations_enc;
    }]

    [@@@get
    {
      name = "monitor_heads";
      path = "monitor/heads/main";
      output = Types.monitor_heads_enc;
    }]

    [@@@get
    {
      name = "network_constants";
      path = "/chains/main/blocks/{blockid : string}/context/constants";
      output = Types.network_constants_enc;
    }]

    [@@@get
    {
      name = "node_version";
      path = "/version";
      output = Types.node_version_enc;
    }]

    [@@@get
    {
      name = "block_operations_by_index";
      path = "/chains/main/blocks/{blockid : string}/operations/3/{index : int}";
    }]

    [@@@get
    {
      name = "expected_issuance";
      path =
        "/chains/main/blocks/{blockid : \
         string}/context/issuance/expected_issuance";
      output = Types.expected_issuance_enc;
    }]

    [@@@get
    {
      name = "total_supply";
      path = "/chains/main/blocks/{blockid : string}/context/total_supply";
      output = Types.total_supply_enc;
    }]

    [@@@get
    {
      name = "dynamic_rate";
      path =
        "/chains/main/blocks/{blockid : \
         string}/context/issuance/current_yearly_rate";
      output = Types.dynamic_rate_enc;
    }]

    [@@@get
    {
      name = "total_frozen_stake";
      path = "/chains/main/blocks/{blockid : string}/context/total_frozen_stake";
      output = Types.total_frozen_stake_enc;
    }]

    module SORU = struct
      module L1 = struct
        [@@@get
        {
          name = "genesis_info";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/genesis_info";
          output = Types.SORU.L1.genesis_info_enc;
        }]

        [@@@get
        {
          name = "inbox";
          path = "chains/main/blocks/head/context/smart_rollups/all/inbox";
          output = Types.SORU.L1.rollups_inbox_enc;
        }]

        [@@@get
        {
          name = "last_cemented_commitment_hash_with_level";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/last_cemented_commitment_hash_with_level";
          output = Types.SORU.L1.level_proof_enc;
        }]

        [@@@get
        {
          name = "staked_on_commitment";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker_id : string}/staked_on_commitment";
          output =
            Json_encoding.option Types.SORU.L1.commitment_infos_with_hash_enc;
        }]

        [@@@get
        {
          name = "commitment_infos";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/commitment/{commitment_hash : string}";
          output = Types.SORU.L1.commitment_infos_enc;
          errors = Error.[smart_rollup_unknown_commitment];
        }]

        [@@@get
        {
          name = "commitments";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/inbox_level/{inbox_level : int}/commitments";
          output = Types.SORU.L1.commitments_enc;
        }]

        [@@@get
        {
          name = "stakers_ids";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/commitment/{commitment : string}/stakers_indexes";
          output = Types.SORU.L1.stakers_ids_enc;
          errors = Error.[storage_error];
        }]

        [@@@get
        {
          name = "staker_id";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker : string}/index";
          output = Types.SORU.L1.staker_id_enc;
        }]

        [@@@get
        {
          name = "stakers";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/stakers";
          output = Types.SORU.L1.stakers_enc;
        }]

        [@@@get
        {
          name = "conflicts";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/staker/{staker_id : string}/conflicts";
          output = Types.SORU.L1.conflicts_enc;
        }]

        [@@@get
        {
          name = "ongoing_refutation_games";
          path =
            "chains/main/blocks/{blockid : \
             string}/context/smart_rollups/smart_rollup/{smart_rollup_id : \
             string}/staker/{staker_id : string}/games";
          output = Types.SORU.L1.refutation_games_enc;
        }]

        [@@@get
        {
          name = "timeout";
          path =
            "chains/main/blocks/{blockid : \
             string}/context/smart_rollups/smart_rollup/{smart_rollup_id : \
             string}/staker1/{staker1_id : string}/staker2/{staker2_id : \
             string}/timeout";
          output = Types.SORU.L1.timeout_opt_enc;
        }]

        [@@@get
        {
          name = "soru_addresses";
          path = "chains/main/blocks/head/context/smart_rollups/all";
          output = Types.SORU.L1.soru_addresses_enc;
        }]

        [@@@get
        {
          name = "kind";
          path =
            "chains/main/blocks/head/context/smart_rollups/smart_rollup/{smart_rollup_id \
             : string}/kind";
          output = Types.SORU.L1.kind_enc;
        }]
      end
    end
  end
end

module Entities = struct
  module AttesPreattes = struct
    type kind =
      [ `Attestation
      | `Preattestation ]

    type 'a t = {
      attestation : 'a;
      preattestation : 'a;
    }
    [@@deriving jsoo]

    let init v = { attestation = v; preattestation = v }

    let get_with_kind ~(kind : kind) ep =
      match kind with
      | `Attestation -> ep.attestation
      | `Preattestation -> ep.preattestation

    let map f { attestation; preattestation } =
      { attestation = f attestation; preattestation = f preattestation }

    let map_with_kind ~(kind : kind) f ep =
      match kind with
      | `Attestation -> { ep with attestation = f ep.attestation }
      | `Preattestation -> { ep with preattestation = f ep.preattestation }

    let merge merge ep1 ep2 =
      {
        attestation = merge ep1.attestation ep2.attestation;
        preattestation = merge ep1.preattestation ep2.preattestation;
      }

    let iter (f : kind:kind -> 'a -> unit) { attestation; preattestation } =
      f ~kind:`Attestation attestation ;
      f ~kind:`Preattestation preattestation

    let fold (f : kind:kind -> 'a -> 'b -> 'b) { attestation; preattestation }
        (acc : 'b) =
      let acc = f ~kind:`Attestation attestation acc in
      let acc = f ~kind:`Preattestation preattestation acc in
      acc
  end

  module Attester = String
  module AttesterSet = Set.Make (Attester)

  module Baker = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module BakerMap = Map.Make (Baker)

  module Validator = struct
    type t = RPC.Types.validator

    let compare = compare
  end

  module ValidatorSet = Set.Make (Validator)

  module Hash = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module HashMap = Map.Make (Hash)
  module HashSet = Set.Make (Hash)

  module Level = struct
    type t = int [@@deriving jsoo]

    let compare = Int.compare
  end

  module LevelMap = Map.Make (Level)

  module Staker = struct
    type t = string [@@deriving jsoo]

    let compare = String.compare
  end

  module StakerSet = Set.Make (Staker)
  module StakerMap = Map.Make (Staker)
  module Id = String
  module IdMap = Map.Make (Id)

  module Round = struct
    type t = int [@@deriving jsoo]

    let compare = Int.compare
  end

  module RoundSet = Set.Make (Round)
  module RoundMap = Map.Make (Round)

  module LevelRound = struct
    type t = {
      level : int;
      round : int;
    }

    let compare lr1 lr2 =
      match compare lr1.level lr2.level with
      | 0 -> compare lr1.round lr2.round
      | i -> i
  end

  module LevelRoundMap = Map.Make (LevelRound)

  module SORU_DATA = struct
    module Commitment_tree = struct
      module type DataGetter = sig
        open RPC.Types

        val commitment_period : unit -> int

        (** [predecessors_storage_capacity] is an approximation of the "worst
            case scenario" of what would be needed to store the predecessor of
            each commitment of the tree.

            Note that the predecessors storage will be restored each time a new
            tree is built, the only time this storage is relevant is when you're
            working on a single commitment tree and you rely on the automatic
            update at each head fetched, the update will be way much smoother. *)
        val predecessors_storage_capacity : unit -> int

        val get_lcc :
          rollup_id:string -> (SORU.L1.commitment * SORU.L1.C.inbox_level) Lwt.t

        val get_commitments :
          rollup_id:string -> inbox_level:int -> SORU.L1.commitments Lwt.t

        val get_stakers_ids :
          rollup_id:string -> commitment:string -> SORU.L1.stakers_ids Lwt.t

        val get_staker_id :
          rollup_id:string -> staker_hash:string -> SORU.L1.staker_id Lwt.t

        val get_stakers : rollup_id:string -> SORU.L1.stakers Lwt.t

        val get_commitment_infos :
          rollup_id:string ->
          commitment:string ->
          SORU.L1.commitment_infos option Lwt.t
      end

      module Make (DG : DataGetter) = struct
        open RPC.Types

        type t = {
          commitment : SORU.L1.commitment;
          stakers : SORU.L1.stakers;
          childs : t list;
        }

        module Commitments_infos : sig
          val find_opt : SORU.L1.commitment -> SORU.L1.commitment_infos option

          val add : SORU.L1.commitment -> SORU.L1.commitment_infos -> unit

          val filter_clear :
            (SORU.L1.commitment -> SORU.L1.commitment_infos -> bool) -> unit

          val clear_on_change : rollup_id:string -> unit
        end = struct
          let rollup_id = ref ""

          let tbl : (SORU.L1.commitment, SORU.L1.commitment_infos) Hashtbl.t =
            Hashtbl.create @@ DG.predecessors_storage_capacity ()

          let find_opt commitment = Hashtbl.find_opt tbl commitment

          let add commitment commitment_info =
            Hashtbl.add tbl commitment commitment_info

          let filter_clear filter =
            let filter_map commitment commitment_info =
              if filter commitment commitment_info then
                Some commitment_info
              else
                None in
            Hashtbl.filter_map_inplace filter_map tbl

          let clear_on_change ~rollup_id:rollup_id' =
            if String.equal !rollup_id rollup_id' then begin
              Hashtbl.clear tbl ;
              rollup_id := rollup_id'
            end
        end

        let next_inbox_level inbox_level = inbox_level + DG.commitment_period ()

        module Maps = struct
          (** @staker_id_to_hash: staker's index to a staker's hash
              @inbox_lvl_to_cmts: inbox level to commitments
              @cmt_to_stakers_ids: commitment to stakers' indexes *)
          type t = {
            mutable staker_id_to_hash : SORU.L1.staker IdMap.t;
            mutable inbox_lvl_to_cmts : SORU.L1.C.commitments LevelMap.t;
            mutable cmt_to_stakers_ids : SORU.L1.stakers_ids HashMap.t;
          }

          let make () =
            {
              staker_id_to_hash = IdMap.empty;
              inbox_lvl_to_cmts = LevelMap.empty;
              cmt_to_stakers_ids = HashMap.empty;
            }

          let fill_staker_id_to_hash ~rollup_id ~stakers_hashes maps =
            let set staker_id staker_hash =
              maps.staker_id_to_hash <-
                IdMap.add staker_id staker_hash maps.staker_id_to_hash in
            Lwt_list.iter_s
              (fun staker_hash ->
                let%lwt staker_id = DG.get_staker_id ~rollup_id ~staker_hash in
                set staker_id staker_hash ;
                Lwt.return_unit)
              stakers_hashes

          let fill_cmt_to_stakers_ids ~rollup_id commitments maps =
            let set commitment stakers_ids =
              maps.cmt_to_stakers_ids <-
                HashMap.add commitment stakers_ids maps.cmt_to_stakers_ids in
            Lwt_list.iter_s
              (fun commitment ->
                try%lwt
                  let%lwt stakers_ids =
                    DG.get_stakers_ids ~rollup_id ~commitment in
                  set commitment stakers_ids ;
                  Lwt.return_unit
                with RPC.Error.Storage_error ->
                  set commitment [] ;
                  Lwt.return_unit)
              commitments

          let fill_inbox_lvl_to_cmt ~rollup_id ~start_inbox_level maps =
            let set inbox_level commitments =
              maps.inbox_lvl_to_cmts <-
                LevelMap.add inbox_level commitments maps.inbox_lvl_to_cmts
            in
            let rec fill inbox_level =
              let%lwt commitments_opt =
                DG.get_commitments ~rollup_id ~inbox_level in
              match commitments_opt with
              | None -> Lwt.return_unit
              | Some commitments ->
                set inbox_level commitments ;
                let%lwt () =
                  fill_cmt_to_stakers_ids ~rollup_id commitments maps in
                fill (next_inbox_level inbox_level) in
            fill start_inbox_level
        end

        let get_predecessor ~rollup_id ~commitment =
          match Commitments_infos.find_opt commitment with
          | None -> begin
            match%lwt DG.get_commitment_infos ~rollup_id ~commitment with
            | Some commitment_infos ->
              Commitments_infos.add commitment commitment_infos ;
              Lwt.return_some commitment_infos.SORU.L1.predecessor
            | None -> Lwt.return_none
          end
          | Some commitment_infos ->
            Lwt.return_some commitment_infos.SORU.L1.predecessor

        let make_tree ~rollup_id ~lcc_hash ~lcc_inbox_level maps =
          let rec make commitment inbox_level =
            let stakers =
              let stakers_ids =
                HashMap.find_opt commitment maps.Maps.cmt_to_stakers_ids in
              let stakers_ids = Option.value ~default:[] stakers_ids in
              List.filter_map
                (fun staker_id ->
                  IdMap.find_opt staker_id maps.Maps.staker_id_to_hash)
                stakers_ids in
            let child_inbox_level = next_inbox_level inbox_level in
            let childs_commitments =
              LevelMap.find_opt child_inbox_level maps.Maps.inbox_lvl_to_cmts
            in
            let%lwt childs =
              Lwt_list.filter_map_s
                (fun child_commitment ->
                  match%lwt
                    get_predecessor ~rollup_id ~commitment:child_commitment
                  with
                  | Some predecessor when predecessor = commitment ->
                    let%lwt child = make child_commitment child_inbox_level in
                    Lwt.return_some child
                  | _ -> Lwt.return_none)
                (Option.value ~default:[] childs_commitments) in
            Lwt.return { commitment; stakers; childs } in
          make lcc_hash lcc_inbox_level

        let build ~rollup_id =
          Commitments_infos.clear_on_change ~rollup_id ;
          let%lwt lcc_hash, lcc_inbox_level = DG.get_lcc ~rollup_id in
          let _clear_commitments_infos_below_lcc =
            Commitments_infos.filter_clear
            @@ fun _commitment SORU.L1.{ inbox_level; _ } ->
            lcc_inbox_level < inbox_level in
          let%lwt stakers_hashes = DG.get_stakers ~rollup_id in
          let maps = Maps.make () in
          let%lwt () =
            Maps.fill_staker_id_to_hash ~rollup_id ~stakers_hashes maps in
          let () (* we add [lcc_inbox_level] and [lcc_hash] at first *) =
            maps.Maps.inbox_lvl_to_cmts <-
              LevelMap.add lcc_inbox_level [] maps.Maps.inbox_lvl_to_cmts ;
            maps.Maps.cmt_to_stakers_ids <-
              HashMap.add lcc_hash [] maps.Maps.cmt_to_stakers_ids in
          let%lwt () =
            let start_inbox_level = next_inbox_level lcc_inbox_level in
            Maps.fill_inbox_lvl_to_cmt ~rollup_id ~start_inbox_level maps in
          let tree = make_tree ~rollup_id ~lcc_hash ~lcc_inbox_level maps in
          tree
      end
    end
  end
end
