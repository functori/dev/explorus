(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2023, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Soru = struct
  module Commitment_tree = struct
    module Dummy = struct
      (** In this module commitment will be represented by capital letters
          (\[A-Z\]+ as strings), stakers public key hashes by lowercase letters
          (\[a-z\] as strings), and their indexes by numbers (\[0-9\]+ as
          strings).

          Note that `ILX` stands for `Inbox Level X`.

          To have a better understanding of the overall staking behaviour of
          stakers see: https://gitlab.com/tezos/tezos/-/merge_requests/7067 *)

      (* *************************************************************
          The dummy commitment tree will look like this:

          IL0                            LCC >>> A
                                         /\
                                        /  \
          IL1                          B    G:{c->2}
                                    __/      \_________
                                   /         / \       \
          IL2                    _C_  {a->0}:H  I   {b->1;d->3}:K
                                /   \            \
          IL3                  D {b->1}:F      {c->2;d->3}:J
                              /
          IL4             {a->0}:E
                            /
          IL5              X >>> isolated commitment

          +------------------+------------------+----------------------+
          |   STAKERS PKHS   |    STAKERS IDS   | CURRENTLY STAKING ON |
          +------------------+------------------+----------------------+
          |         a        |         0        |     E(IL4),H(IL2)    |
          |         b        |         1        |     F(IL3),K(IL2)    |
          |         c        |         2        |     G(IL1),J(IL3)    |
          |         d        |         3        |     J(IL3),K(IL2)    |
          +------------------+------------------+----------------------+
         ************************************************************* *)
      [@@@ocamlformat "wrap-comments=false"]

      [@@@ocamlformat "wrap-comments=true"]

      let rollup_id = "sr1FakeFakeFakeFakeFakeFakeFakeFake"

      let lcc (* commitment_hash x inbox_level *) = ("A", 0)

      let commitment_period = 1

      let stakers = ["a"; "b"; "c"; "d"]

      let staker_pkh_id = [("a", "0"); ("b", "1"); ("c", "2"); ("d", "3")]

      let inboxlevel_commitments =
        [
          (0, ["A"]);
          (1, ["B"; "G"]);
          (2, ["C"; "H"; "I"; "K"]);
          (3, ["D"; "F"; "J"]);
          (4, ["E"]);
          (5, ["X"]);
        ]

      let commitment_stakers_ids =
        [
          ("A", []);
          ("B", []);
          ("C", []);
          ("D", []);
          ("E", ["0"]);
          ("F", ["1"]);
          ("G", ["2"]);
          ("H", ["0"]);
          ("I", []);
          ("J", ["2"; "3"]);
          ("K", ["1"; "3"]);
          ("X", []);
        ]

      let predecessors =
        [
          ("B", (1, "A"));
          ("G", (1, "A"));
          ("C", (2, "B"));
          ("D", (3, "C"));
          ("F", (3, "C"));
          ("E", (4, "D"));
          ("H", (2, "G"));
          ("I", (2, "G"));
          ("K", (2, "G"));
          ("J", (3, "I"));
          ("X", (5, "E"));
        ]

      module FakeDataGetter = struct
        let commitment_period () = commitment_period

        let get_lcc ~rollup_id:_ = Lwt.return lcc

        let predecessors_storage_capacity () = 10

        let get_commitments ~rollup_id:_ ~inbox_level =
          Lwt.return @@ List.assoc_opt inbox_level inboxlevel_commitments

        let get_stakers_ids ~rollup_id:_ ~commitment =
          Lwt.return @@ List.assoc commitment commitment_stakers_ids

        let get_staker_id ~rollup_id:_ ~staker_hash =
          Lwt.return @@ List.assoc staker_hash staker_pkh_id

        let get_stakers ~rollup_id:_ = Lwt.return stakers

        let get_commitment_infos ~rollup_id:_ ~commitment =
          let inbox_level, predecessor = List.assoc commitment predecessors in
          Lwt.return_some
            Tezos.RPC.Types.SORU.L1.
              {
                compressed_state = "";
                inbox_level;
                predecessor;
                number_of_ticks = "";
              }
      end

      module Commitment_tree =
        Tezos.Entities.SORU_DATA.Commitment_tree.Make (FakeDataGetter)

      let tree =
        let make_node commitment stakers childs =
          Commitment_tree.{ commitment; stakers; childs } in
        let _X = make_node "X" [] [] in
        let _E = make_node "E" ["a"] [_X] in
        let _D = make_node "D" [] [_E] in
        let _F = make_node "F" ["b"] [] in
        let _C = make_node "C" [] [_D; _F] in
        let _B = make_node "B" [] [_C] in
        let _J = make_node "J" ["c"; "d"] [] in
        let _I = make_node "I" [] [_J] in
        let _H = make_node "H" ["a"] [] in
        let _K = make_node "K" ["b"; "d"] [] in
        let _G = make_node "G" ["c"] [_H; _I; _K] in
        let _A = make_node "A" [] [_B; _G] in
        _A
    end

    module Utils = struct
      let compare_stakers stakers_1 stakers_2 =
        (* we sort them just in case *)
        let stakers_1 = List.sort String.compare stakers_1 in
        let stakers_2 = List.sort String.compare stakers_2 in
        List.equal ( = ) stakers_1 stakers_2

      let sort_childs childs =
        let open Dummy.Commitment_tree in
        List.sort
          (fun child_1 child_2 ->
            String.compare child_1.commitment child_2.commitment)
          childs

      let rec compare_trees tree_1 tree_2 =
        let open Dummy.Commitment_tree in
        if
          tree_1.commitment = tree_2.commitment
          && compare_stakers tree_1.stakers tree_2.stakers
        then
          try
            List.fold_left2
              (fun equality child_1 child_2 ->
                equality && compare_trees child_1 child_2)
              true
              (sort_childs tree_1.childs)
              (sort_childs tree_2.childs)
          with _ -> false
        else
          false
    end

    (** tree coequality check *)
    let tree_coequality_test _ () =
      let%lwt built_tree = Dummy.(Commitment_tree.build ~rollup_id) in
      Lwt.return
      @@ Alcotest.(check bool) "return true" true
      @@ Utils.compare_trees built_tree Dummy.tree
  end
end
