(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let wrap_path_json ~dir path = "/" ^ dir ^ path

let log_ez_req_lwt_err = function
  | EzReq_lwt_S.KnownError { code; error } ->
    Log.log_f "code : %d | error : %s" code (Printexc.to_string error)
  | EzReq_lwt_S.UnknownError { code; msg } ->
    Log.log_f "code : %d | error : %s" code
      (Option.value ~default:"Lib_tst.log_ez_req_lwt_err : no_err_msg" msg)

let log_path (EzAPI.Url.URL path) = Log.log_f "\nRequest: %s\n" path

exception Destruction_Failed

let destruct_json_lwt json encoding =
  match%lwt EzReq_lwt.get (EzAPI.URL json) with
  | Ok result -> Lwt.return @@ EzEncoding.destruct encoding result
  | Error _ -> Lwt.fail_with "Test_destruct_json_lwt : cannot get result"

let destruct_json json encoding =
  try
    let ic = open_in (String.sub json 1 (String.length json - 1)) in
    let buffer = really_input_string ic (in_channel_length ic) in
    close_in ic ;
    EzEncoding.destruct encoding buffer
  with _ ->
    Log.log_f "Test_destruct_json : could not open file %s !\n" json ;
    raise Destruction_Failed

let destruct_json_encoding json encoding =
  let open Json_encoding in
  try
    let ic = open_in (String.sub json 1 (String.length json - 1)) in
    let buffer = really_input_string ic (in_channel_length ic) in
    close_in ic ;
    try
      let _destruction = EzEncoding.destruct encoding buffer in
      true
    with
    | Cannot_destruct (path, exn) ->
      Format.eprintf "Error during destruction path %a : %s\n\n %s\n%!"
        (Json_query.print_path_as_json_path ~wildcards:true)
        path (Printexc.to_string exn) buffer ;
      false
    | Unexpected_field field ->
      Format.eprintf "Error during destruction path, unexpected field %S %s\n%!"
        field buffer ;
      false
  with _ ->
    Log.log_f "Test_.destruct_json_encoding : could not open file %s !\n" json ;
    false

let destruct_json_path json _encoding =
  try
    let ic = open_in (String.sub json 1 (String.length json - 1)) in
    let _buffer = really_input_string ic (in_channel_length ic) in
    close_in ic ;
    true
  with _ ->
    Log.log_f "Test_.destruct_json_ok : could not open file %s !\n" json ;
    false
