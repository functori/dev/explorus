(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Json_types

let components_states_file =
  Lib_tst.wrap_path_json ~dir:"static" Paths.components_states

let components_states_path_ok () =
  Lib_tst.destruct_json_path components_states_file components_states_enc

let components_states_encoding () =
  Lib_tst.destruct_json_encoding components_states_file components_states_enc

(* In our case, generally we don't want any of the components to be deactivated,
   we have to make sure of it *)
let all_components_are_active () =
  let {
    cp_baking_rights;
    cp_round_clock;
    cp_latest_blocks;
    cp_consensus_ops;
    cp_bakers_activity;
    cp_explorer;
    cp_soru;
    cp_dal;
    cp_info_bar;
    cp_consensus_progression;
    cp_network_constants;
    cp_share_url;
    cp_pyrometer;
    cp_protocol_injection;
    cp_logs;
    cp_settings;
  } =
    Lib_tst.destruct_json components_states_file components_states_enc in
  cp_baking_rights && cp_latest_blocks && cp_consensus_ops && cp_bakers_activity
  && cp_info_bar && cp_consensus_progression && cp_network_constants
  && cp_pyrometer && cp_settings && cp_dal && cp_round_clock && cp_explorer
  && cp_share_url && cp_protocol_injection && cp_soru && cp_logs

let test_path _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ components_states_path_ok ()

let test_encoding _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ components_states_encoding ()

let test_all_components_are_active _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ all_components_are_active ()
