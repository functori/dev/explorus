(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
module HashShortening = Central_state.Settings.HashShortening

module Category = struct
  type t =
    | Account
    | Account_hash
    | Commit
    | Hash
    | Protocol

  let of_string = function
    | "account" -> Some Account
    | "account_hash" -> Some Account_hash
    | "commit" -> Some Commit
    | "hash" -> Some Hash
    | "protocol" -> Some Protocol
    | _ -> None
end

type data = {
  copied : bool; [@mutable]
  key : int; [@mutable]
  destructible : Lib_js.Destructible.t;
  id : int;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-hash"

  let element =
    Vue_component.CRender
      (Render.hash_displayer_render, Render.hash_displayer_static_renders)

  let props = Some (Vue_component.PrsArray ["hash"; "category"])
end)

let shorten ~known_bakers _app hash category =
  let hash = Js.to_string hash in
  let category = Js.to_string category in
  let shorten_with shorten =
    let hash_shorten_setting = HashShortening.get () in
    shorten hash_shorten_setting hash in
  let hash =
    let open Category in
    match of_string category with
    | Some Account -> begin
      match Rpc.get_alias known_bakers hash with
      | None -> shorten_with HashShortening.shorten_tz
      | Some alias -> alias
    end
    | Some Account_hash -> shorten_with HashShortening.shorten_tz
    | Some Commit -> shorten_with HashShortening.shorten_commit
    | Some Hash -> shorten_with HashShortening.shorten_hash
    | Some Protocol -> shorten_with HashShortening.shorten_protocol
    | None -> hash in
  Js.string hash

let copy app hash =
  let hash = Js.to_string hash in
  try
    let () = Js.Unsafe.global##.navigator##.clipboard##writeText hash in
    app##.copied := _true ;
    EzLwtSys.run @@ fun () ->
    let%lwt () = EzLwtSys.sleep 0.5 in
    app##.copied := _false ;
    Lwt.return_unit
  with exn ->
    Log.log
    @@ Printf.sprintf "Error when trying to copy %s : %s" hash
         (Printexc.to_string exn)

let refresh_on_hash_shorten_setting_change ~app =
  Listener.connect HashShortening.listener @@ fun _ ->
  app##.key := app##.key + 1

let init () =
  let%lwt known_bakers = Central_state.DJson.known_bakers in
  let gen_id = GenId.create () in
  let data _ =
    object%js
      val mutable copied = _false

      val id = GenId.gen gen_id

      val mutable key = 0

      val destructible = Lib_js.Destructible.init ()
    end in
  C.add_method1 "shorten" (shorten ~known_bakers) ;
  C.add_method1 "copy" copy ;
  Lwt.return
  @@ C.make
       ~lifecycle:
         [
           ( "beforeMount",
             fun app ->
               let hash_shorten_connection_id =
                 refresh_on_hash_shorten_setting_change ~app in
               Lib_js.Destructible.at_destroy app##.destructible @@ fun () ->
               Listener.disconnect hash_shorten_connection_id );
           ( "destroyed",
             fun app -> Lib_js.Destructible.destroy app##.destructible );
         ]
       ~data ()
