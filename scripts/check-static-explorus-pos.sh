#!/bin/sh

static_index="static/index.html"
expected_output="explorus.js"
get_line=$(sed '62q;d' $static_index)
extract_output=$(echo $get_line | head -c 25 | tail -c 11)

if [ "$extract_output" = "$expected_output" ] ; then
    echo "==> explorus.js script is well placed"
    exit 0
else 
    echo "Failure: explorus.js script is at a wrong position"
    exit 1
fi
