(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let verbose ~__FUNCTION__ =
  Central_state.Debug.(log ~verbose:Dangerous_access __FUNCTION__)

(* These strings are needed to colorate cells for baking rights *)
module CellColor = struct
  let current_head_class = "current-head"

  let finalized_content_class = "finalized-content"

  let finalized_block_class = "finalized-block"

  let seen_alt_block_class = "seen-alt-block"

  let offline_baker_class = "offline-baker line"

  let neutral_class = String.empty
end

(* ================================= TYPES ================================= *)

(* Mandatory informations to perform a coloration at an instance x *)
type infos_for_coloration = {
  is_misser : bool;
  original_color : string;
  current_round : int option;
  pred_block : Tezos_process.Blockchain.block_details option;
  slot : int option;
  level : int;
  delegate : string;
}

type br_bakers = {
  br_baker : string;
  br_color : string;
}
[@@deriving jsoo]

type baking_right = {
  br_level : int;
  br_bakers : br_bakers list;
}
[@@deriving jsoo]

type baking_rights = baking_right list [@@deriving jsoo]

type data = {
  baking_rights : baking_rights option; [@mutable]
  displayed_round : int option; [@mutable]
  protocol_transition_level : int;
}
[@@deriving jsoo { remove_prefix = false }]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-baking_rights"

  let element =
    Vue_component.CRender
      (Render.baking_rights_render, Render.baking_rights_static_renders)

  let props = None
end)

(* ========================= DYNAMIC DISPLAY ROUNDS ========================= *)

module Currently_Displayed_Rounds : sig
  (* we never let direct access to references here, to prevent the risk of
     breaking something *)
  val set : round:int -> level:int -> unit

  val get : unit -> int
end = struct
  let actual_max_round = ref Konstant.base_round_displayed

  let current_level = ref (-1)

  (* when rounds exceed the base max rounds number we want to display the
     new_maximum rounds during at least @levels_window more levels *)
  let levels_window = 4

  let display_counter = ref 0

  let reset_dc () = display_counter := levels_window

  let reset_amr () =
    if !display_counter = 0 then
      actual_max_round := Konstant.base_round_displayed

  let set_current_level level = current_level := level

  let trigger_dc level =
    if !display_counter > 0 && !current_level <> level then (
      display_counter := !display_counter - 1 ;
      set_current_level level
    ) ;
    reset_amr ()

  (* actual_max_round can NEVER be < max_round because we never want to display
     less than @max_round rounds *)
  let set ~round ~level =
    if round > Konstant.base_round_displayed then (
      actual_max_round := Int.max round !actual_max_round ;
      reset_dc () ;
      set_current_level level
    ) else
      trigger_dc level

  let get () = !actual_max_round
end

module CDR = Currently_Displayed_Rounds

let find_validator slot validators =
  let exception Slot_exceeded in
  let find_slot slots =
    List.exists
      (fun potential_slot ->
        if potential_slot > slot then
          raise Slot_exceeded
        else
          slot = potential_slot)
      slots in
  (* [List.find] is never supposed to fail in our case since a slot is always
     attributed to a validator. *)
  List.find
    (fun { val_slots; _ } ->
      try find_slot val_slots with Slot_exceeded -> false)
    validators

let extend_bakers slot br_level br_bakers =
  let br_color =
    (* since we don't have any further information on this cell: *)
    CellColor.neutral_class in
  let%lwt validators =
    Cache.Validators.get br_level Rpc.get_validators
      (ConcurrentValue.get Central_state.Cache.validators) in
  let { val_delegate = br_baker; _ } = find_validator slot validators in
  let extension = [{ br_color; br_baker }] in
  Lwt.return @@ br_bakers @ extension

let extract_all_bakers_for_a_level ~level ~final_slot bakers =
  let last_unknown_slot = List.length bakers in
  let rec extract_all next_slot bakers =
    if next_slot > final_slot then
      Lwt.return bakers
    else
      let%lwt bakers = extend_bakers next_slot level bakers in
      extract_all (next_slot + 1) bakers in
  extract_all last_unknown_slot bakers

let extend_validators_vue app slot =
  Js_of_ocaml.Js.Optdef.case app##.baking_rights_
    (fun () -> Lwt.return_unit)
    (fun baking_rights ->
      let baking_rights = baking_rights_of_jsoo baking_rights in
      let%lwt baking_rights =
        Lwt_list.map_p
          (fun { br_level; br_bakers } ->
            let%lwt br_bakers =
              extract_all_bakers_for_a_level ~level:br_level ~final_slot:slot
                br_bakers in
            Lwt.return { br_level; br_bakers })
          baking_rights in
      app##.baking_rights_ := def (baking_rights_to_jsoo baking_rights) ;
      Lwt.return_unit)

let set_displayed_round app head_level =
  let Tezos_process.Clock.{ curr_round; _ } =
    Tezos_process.Clock.get_round_info () in
  (* always try to set the actual max round value in CDR to extend it if we need
     to: *)
  CDR.set ~round:(curr_round + 1) ~level:head_level ;
  app##.displayed_round_ := def @@ CDR.get ()

(* ============== BAKING RIGHTS FOR CURRENT LEVELS AND ROUNDS ============== *)

(* To understand how cells are colored, you have to read
   [original_color_condition] [get_coloration] and [fill_baking_rights].

   - [original_color_condition] is used to retrieve current blockchain color
   state (beige, light green, green)

   - `default_condition` is mainly used for levels lower than head level, we're
   trying to guess for each cell from lower levels if the current proposed head
   is trying to build on top of them, to do that we are looking if baked block
   from previous round matches current head previous block's baker and round.

   - `slot = current_round_slot` is used for current level, if we received a
   block on current level/round and attributed slot matches then we're on the
   currrent head.

   @todo : this is one implementation to attribute baking rights, it can
   possibly be written differently according to the comment above to avoid
   reading it, we should be able to understand clearly what the functions below
   are doing without reading this comment *)
let original_color_condition current_round pred_block delegate slot =
  let default_condition =
    match pred_block with
    | None -> false
    | Some Tezos_process.Blockchain.{ bd_baker; bd_round; _ } ->
      (* bd_baker = pred_baker | bd_round = pred_round *)
      delegate = bd_baker && slot = bd_round in
  match current_round with
  | None -> default_condition
  | Some current_round_slot -> slot = current_round_slot || default_condition

let get_coloration
    {
      is_misser;
      original_color;
      current_round;
      pred_block;
      slot;
      level;
      delegate;
    } =
  let is_gray = is_misser in
  let is_pink () =
    Option.perform slot ~default:false ~perform:(fun round ->
        Cache.LevelRoundBlocks.has_been_seen
          (ConcurrentValue.get Central_state.Cache.lr_blocks)
          Tezos.Entities.LevelRound.{ level; round }) in
  let is_greenish_or_white () =
    Option.perform slot ~default:false
      ~perform:(original_color_condition current_round pred_block delegate)
  in
  if is_gray then
    CellColor.offline_baker_class
  else if is_greenish_or_white () then
    original_color
  else if is_pink () then
    CellColor.seen_alt_block_class
  else
    (* if is_white () then *)
    CellColor.neutral_class

let fill_baking_rights acc slots max_round ~infos_for_coloration =
  let rec look_for_fitting_slot baking_rights count =
    match List.nth_opt slots (max_round - 1 - count) with
    | Some slot when slot >= 0 && slot < max_round ->
      let infos_for_coloration =
        { infos_for_coloration with slot = Some slot } in
      let color = get_coloration infos_for_coloration in
      look_for_fitting_slot
        ((slot, (infos_for_coloration.delegate, color)) :: baking_rights)
        (count - 1)
    | _ -> baking_rights in
  look_for_fitting_slot acc (max_round - 1)

let get_all_missers blocks_details nb_scanned_blocks =
  try
    List.fold_left
      (fun missers Tezos_process.Blockchain.{ miss_attester; miss_slots } ->
        if miss_slots >= nb_scanned_blocks then
          Tezos.Entities.AttesterSet.add miss_attester missers
        else
          missers)
      Tezos.Entities.AttesterSet.empty
      (List.hd blocks_details).Tezos_process.Blockchain.bd_missing_attesters
  with _ -> Tezos.Entities.AttesterSet.empty

(** [check_range] checks if the given [level] is in protocol's transition level
    range *)
let check_range ~protocol_transition_level level =
  let lower_bound = protocol_transition_level in
  let upper_bound =
    protocol_transition_level
    + Konstant.validators_rights_range_on_protocol_transition in
  level > lower_bound && level <= upper_bound

let get_validators_rights ~protocol_transition_level level =
  if check_range ~protocol_transition_level level then
    (* we explicitely call the function from Rpc instead of trying to dig in the
       cache to "force" the update of the cache and getting the updated
       validators rights from the upcoming protocol computation *)
    Rpc.get_validators level
  else
    Cache.Validators.get level Rpc.get_validators
      (ConcurrentValue.get Central_state.Cache.validators)

(** [get_baking_rights] is linear in the number of validators 'n' up to a
    constant k = n. It is at most in O(2n + ε) where ε is the negligible
    residuals (accessing the next slot just before breaking in a slot list for
    example). *)
let get_baking_rights ~protocol_transition_level level color blocks_details
    current_round pred_block =
  let nb_scanned_blocks = List.length blocks_details in
  let missers = get_all_missers blocks_details nb_scanned_blocks in
  let%lwt validators = get_validators_rights ~protocol_transition_level level in
  let max_round = CDR.get () in
  let baking_rights_per_round =
    List.fold_left
      (fun acc validator ->
        if List.length acc > max_round then
          acc
        else
          let delegate = validator.val_delegate in
          let is_misser = Tezos.Entities.AttesterSet.mem delegate missers in
          let infos_for_coloration =
            {
              is_misser;
              original_color = color;
              current_round;
              pred_block;
              slot = None;
              level;
              delegate;
            } in
          fill_baking_rights acc validator.val_slots max_round
            ~infos_for_coloration)
      [] validators in
  Lwt.return @@ snd @@ List.split
  @@ List.sort (fun a b -> compare (fst a) (fst b)) baking_rights_per_round

type init_level_info = {
  level : int;
  cell_color : string;
  round : int option;
  pred_block : Tezos_process.Blockchain.block_details option;
}

let make_level_info level cell_color round pred_block =
  { level; cell_color; round; pred_block }

let get_initial_levels blocks_details =
  verbose ~__FUNCTION__ ;
  let head = List.hd blocks_details in
  let head_level = head.Tezos_process.Blockchain.bd_level in
  let head_round = head.Tezos_process.Blockchain.bd_round in
  let initial_list =
    [
      make_level_info (head_level + 4) CellColor.neutral_class None None;
      make_level_info (head_level + 3) CellColor.neutral_class None None;
      make_level_info (head_level + 2) CellColor.neutral_class None None;
      make_level_info (head_level + 1) CellColor.neutral_class None None;
      make_level_info head_level CellColor.current_head_class (Some head_round)
        None;
    ] in
  let get_level color shift =
    [
      make_level_info (head_level - shift) color None
      @@ List.nth_opt blocks_details shift;
    ] in
  let number_of_levels_available = head_level + 1 in
  let shift_limit = min number_of_levels_available 4 in
  let construct_all_levels head_level =
    let rec construct levels shift is_content = function
      | i when i <= 0 -> levels
      | i ->
        if shift = shift_limit then
          levels
        else if is_content then
          construct
            (levels @ get_level CellColor.finalized_content_class shift)
            (shift + 1) false (i - 1)
        else
          construct
            (levels @ get_level CellColor.finalized_block_class shift)
            (shift + 1) false (i - 1) in
    construct initial_list 1 true head_level in
  Lwt.return @@ construct_all_levels head_level

let contain_genesis needed_levels =
  let last = List.last needed_levels in
  last.level = Konstant.genesis_level
  && List.length needed_levels
     <= Konstant.validators_rights_range_on_protocol_transition + 1

let construct_baking_rights ~protocol_transition_level needed_levels
    blocks_details =
  if contain_genesis needed_levels then
    Lwt.return
    @@ List.map
         (fun { level; _ } -> { br_level = level; br_bakers = [] })
         needed_levels
  else
    Lwt_list.map_p
      (fun { level; cell_color; round; pred_block } ->
        let%lwt br_bakers =
          get_baking_rights ~protocol_transition_level level cell_color
            blocks_details round pred_block in
        (* @todo : don't map here *)
        let br_bakers =
          List.map
            (fun (br_baker, br_color) -> { br_baker; br_color })
            br_bakers in
        Lwt.return { br_level = level; br_bakers })
      needed_levels

let set_all_baking_rights app ~protocol_transition_level blocks_details =
  verbose ~__FUNCTION__ ;
  let%lwt baking_rights =
    let%lwt needed_levels = get_initial_levels blocks_details in
    construct_baking_rights ~protocol_transition_level needed_levels
      blocks_details in
  app##.baking_rights_ := def (baking_rights_to_jsoo baking_rights) ;
  Lwt.return_unit

(* =========================== REFRESH MECHANISMS =========================== *)

let backup_round_extension app =
  let safe_update = ConcurrentValue.create () in
  let callback () =
    Lwt.async_exn ~__FUNCTION__ @@ fun () ->
    let%lwt head = Rpc.get_block () in
    set_displayed_round app head.bl_header.hd_level ;
    ConcurrentValue.update safe_update (fun () ->
        extend_validators_vue app @@ CDR.get ()) in
  let open Central_state.Intervals in
  let { nc_minimal_block_delay; _ } =
    Central_state.Network.get_network_constants () in
  let minimal_block_delay =
    if nc_minimal_block_delay = 0 then
      5
    else
      nc_minimal_block_delay in
  add_intervals ~clearable_type:Baking_rights_rounds ~callback
    ~loop_frequency:(Int.to_float minimal_block_delay)

let update_baking_rights_vue app ~protocol_transition_level chain =
  Lwt.async_exn ~__FUNCTION__ @@ fun () ->
  let%lwt () = set_all_baking_rights app ~protocol_transition_level chain in
  let head_level = (List.hd chain).Tezos_process.Blockchain.bd_level in
  set_displayed_round app head_level ;
  Lwt.return_unit

let init ~protocol_transition_level () =
  let data _ =
    object%js
      val mutable baking_rights_ = undefined

      val mutable displayed_round_ = undefined

      val protocol_transition_level_ = protocol_transition_level
    end in
  let at_destroy = PendingFunctions.create () in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            backup_round_extension app ;
            let id =
              Listener.connect ~use_storage:true
                Tezos_process.Blockchain.chain_update_listener
                (update_baking_rights_vue app ~protocol_transition_level) in
            PendingFunctions.add at_destroy @@ fun () ->
            Central_state.Intervals.(clear_intervals Baking_rights_rounds) ;
            Listener.disconnect id );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ()
