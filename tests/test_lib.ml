(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ============================ Tempotbl tests ============================ *)

let tempotbl_case case = "tempotbl : " ^ case

module SpecWithUpdate = struct
  type key = int

  type data = int

  let capacity = 25

  let duration = 3.

  let update_on_access = true

  let sleep = Lwt_unix.sleep
end

module SpecWithoutUpdate = struct
  type key = int

  type data = int

  let capacity = 25

  let duration = 3.

  let update_on_access = false

  let sleep = Lwt_unix.sleep
end

module Tbl = Tempotbl.Make (SpecWithUpdate)
module Tbl2 = Tempotbl.Make (SpecWithoutUpdate)

let insert_value tbl i = Tbl.add tbl i i

let insert_multiple_values ~stop tbl =
  Int.iter ~start:0 ~stop (insert_value tbl)

(** test to see if one value added is properly cleaned after
    [SpecWithUpdate.duration] *)
let is_one_value_properly_cleaned_test _ () =
  let tbl = Tbl.create ~trigger_routine_from_start:false in
  let key, data = (1, 1) in
  Tbl.add tbl key data ;
  (* we can assume that the time between adding and triggering the cleaning
     routine is neglectable *)
  Tbl.clean_routine tbl ;
  (* we sleep during [Spec.duration] to let the Tempotbl clean itself *)
  let%lwt () = SpecWithUpdate.sleep SpecWithUpdate.duration in
  (* if value is cleaned we should find [None] *)
  let result =
    match Tbl.find_opt tbl key with
    | None -> true
    | Some _ -> false in
  Lwt.return @@ Alcotest.(check bool) "return true" true result

(** test to see if values ([Konstant.extra_large_cache_size] here) added are
    properly cleaned after [SpecWithUpdate.duration] *)
let are_values_properly_cleaned_test _ () =
  let tbl = Tbl.create ~trigger_routine_from_start:false in
  let xl_capacity = Konstant.extra_large_cache_size in
  let b_length_before_insert = Tbl.length tbl = 0 in
  insert_multiple_values ~stop:xl_capacity tbl ;
  let b_length_after_insert = Tbl.length tbl = xl_capacity in
  Tbl.clean_routine tbl ;
  let%lwt () = SpecWithUpdate.sleep @@ (SpecWithUpdate.duration *. 2.) in
  let b_length_after_cleaning = Tbl.length tbl = 0 in
  Lwt.return
  @@ Alcotest.(check bool)
       "return true" true
       (b_length_before_insert && b_length_after_insert
      && b_length_after_cleaning)

(** test to see if values' duration are properly extended on access :
    [SpecWithUpdate.update_on_access] *)
let is_value_duration_properly_extended_test _ () =
  let delta = 2. in
  let tbl = Tbl.create ~trigger_routine_from_start:false in
  let search_for () = Lwt.return (-1, 0.) in
  let key, data = (1, 1) in
  Tbl.add tbl key data ;
  Tbl.clean_routine tbl ;
  let%lwt () = SpecWithUpdate.sleep delta in
  let%lwt _implicitly_extend_duration = Tbl.get tbl key ~search_for in
  let%lwt () = SpecWithUpdate.sleep @@ (SpecWithUpdate.duration -. delta) in
  (* at this point value should be cleaned if it was not extended by accessing
     the value *)
  (* if value is properly extended we should find [Some _] *)
  let result =
    match Tbl.find_opt tbl key with
    | None -> false
    | Some _ -> true in
  Lwt.return @@ Alcotest.(check bool) "return true" true result

(** test to see if values' duration are NOT extended on access :
    [SpecWithoutUpdate.update_on_access] *)
let is_value_duration_not_extended_test _ () =
  let delta = 2. in
  let tbl = Tbl2.create ~trigger_routine_from_start:false in
  let search_for () = Lwt.return (-1, 0.) in
  let key, data = (1, 1) in
  Tbl2.add tbl key data ;
  Tbl2.clean_routine tbl ;
  let%lwt () = SpecWithoutUpdate.sleep delta in
  let%lwt _implicitly_extend_duration = Tbl2.get tbl key ~search_for in
  let%lwt () = SpecWithoutUpdate.sleep @@ (SpecWithUpdate.duration -. delta) in
  (* if value is not extended we should find None *)
  let result =
    match Tbl2.find_opt tbl key with
    | None -> true
    | Some _ -> false in
  Lwt.return @@ Alcotest.(check bool) "return true" true result

(** test to see if [Testtbl.cleaning_routine] is indeed only triggered once *)
let is_cleaning_routine_triggered_once_and_only_once_test _ () =
  let delta = 1. in
  let capacity = 10 in
  let tbl = Tbl.create ~trigger_routine_from_start:false in
  let clean_routine () = Tbl.clean_routine tbl in
  clean_routine () ;
  let%lwt () = SpecWithUpdate.sleep delta in
  clean_routine () ;
  let%lwt () = SpecWithUpdate.sleep @@ (SpecWithUpdate.duration -. delta) in
  insert_multiple_values ~stop:capacity tbl ;
  let%lwt () = SpecWithUpdate.sleep delta in
  let length_after_cleaning = Tbl.length tbl = capacity in
  (* if multiple cleaning routines we're triggered at least one of the condition
     above would be false since *)
  Lwt.return @@ Alcotest.(check bool) "return true" true length_after_cleaning

(* ========================= ConcurrentValue tests ========================= *)

let concurrent_case case = "concurrent value : " ^ case

let create_then_get_test _ () =
  let v = "Success" in
  let c = ConcurrentValue.create v in
  let v' = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check string) "return true" v v'

let apply_test _ () =
  let v = "Success" in
  let f s = s ^ " achieved" in
  let c = ConcurrentValue.create v in
  let%lwt r = ConcurrentValue.apply c @@ fun v -> Lwt.return (f v) in
  Lwt.return @@ Alcotest.(check string) "return true" (f v) r

let apply_with_update_test _ () =
  let v = "Success" in
  let f s = s ^ " achieved" in
  let c = ConcurrentValue.create v in
  let%lwt r =
    ConcurrentValue.apply_with_update c @@ fun ~update v ->
    update (f v) ;
    Lwt.return v in
  let v' = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check bool) "return true" true (f v = v' && v = r)

let update_test _ () =
  let v = "Success" in
  let f s = s ^ " achieved" in
  let c = ConcurrentValue.create v in
  let%lwt () = ConcurrentValue.update c @@ fun v -> Lwt.return (f v) in
  let r = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check string) "return true" (f v) r

let simple_update_test _ () =
  let v = "Success" in
  let f s = s ^ " achieved" in
  let c = ConcurrentValue.create v in
  let%lwt () = ConcurrentValue.simple_update c f in
  let r = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check string) "return true" (f v) r

let update0_test _ () =
  let v = "Success" in
  let f () = "FAIL" in
  let c = ConcurrentValue.create v in
  let%lwt () = ConcurrentValue.update0 c @@ fun () -> Lwt.return (f ()) in
  let r = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check string) "return true" (f ()) r

let update1_test _ () =
  let v = "Success" in
  let v' = "FAIL" in
  let f s = s in
  let c = ConcurrentValue.create v in
  let%lwt () = ConcurrentValue.update1 c v' @@ fun s -> Lwt.return (f s) in
  let r = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check string) "return true" (f v') r

let update_order_test _ () =
  let lwt_list_add e l = Lwt.return (e :: l) in
  let c = ConcurrentValue.create [] in
  let%lwt () = ConcurrentValue.update c (lwt_list_add 1) in
  let%lwt () = ConcurrentValue.update c (lwt_list_add 2) in
  let%lwt () = ConcurrentValue.update c (lwt_list_add 3) in
  let r = ConcurrentValue.get c in
  Lwt.return @@ Alcotest.(check (list int)) "return true" [3; 2; 1] r

let unpriority_same_value_test _ () =
  let priority_c = ConcurrentValue.create 42 in
  let unpriority_c = ConcurrentValue.unpriority priority_c in
  let r = ConcurrentValue.get priority_c in
  let r' = ConcurrentValue.get unpriority_c in
  Lwt.return @@ Alcotest.(check int) "return true" r r'

let unpriority_same_value_update_test _ () =
  let priority_c = ConcurrentValue.create 21 in
  let unpriority_c = ConcurrentValue.unpriority priority_c in
  let _ = ConcurrentValue.update priority_c (fun i -> Lwt.return (i * 2)) in
  let r = ConcurrentValue.get priority_c in
  let r' = ConcurrentValue.get unpriority_c in
  Lwt.return @@ Alcotest.(check int) "return true" r r'

let unpriority_order_test _ () =
  let lwt_list_add e l =
    let%lwt () = Lwt_unix.sleep 1. in
    Lwt.return (e :: l) in
  let high_p_c = ConcurrentValue.create [] in
  let normal_p_c = ConcurrentValue.unpriority high_p_c in
  let low_p_c = ConcurrentValue.unpriority normal_p_c in
  let l1 = ConcurrentValue.update low_p_c (lwt_list_add "Low1") in
  let n1 = ConcurrentValue.update normal_p_c (lwt_list_add "Normal1") in
  let h1 = ConcurrentValue.update high_p_c (lwt_list_add "High1") in
  let l2 = ConcurrentValue.update low_p_c (lwt_list_add "Low2") in
  let n2 = ConcurrentValue.update normal_p_c (lwt_list_add "Normal2") in
  let h2 = ConcurrentValue.update high_p_c (lwt_list_add "High2") in
  let%lwt () = Lwt_list.iter_p Fun.id [l1; n1; h1; l2; n2; h2] in
  let r = ConcurrentValue.get low_p_c in
  Lwt.return
  @@ Alcotest.(check (list string))
       "return true"
       ["Low2"; "Normal2"; "Normal1"; "High2"; "High1"; "Low1"]
       r

let unpriority_dynamic_test1 _ () =
  let v = ConcurrentValue.create [] in
  let v' = ConcurrentValue.unpriority v in
  let p_a =
    let ea l =
      let%lwt () = Lwt_unix.sleep 3. in
      Lwt.return ("A" :: l) in
    let%lwt () = Lwt_unix.sleep 0. in
    ConcurrentValue.update v' ea in
  let p_b =
    let eb l =
      let%lwt () = Lwt_unix.sleep 1. in
      Lwt.return ("B" :: l) in
    let%lwt () = Lwt_unix.sleep 1. in
    ConcurrentValue.update v' eb in
  let p_c =
    let ec l =
      let%lwt () = Lwt_unix.sleep 1. in
      Lwt.return ("C" :: l) in
    let%lwt () = Lwt_unix.sleep 2. in
    ConcurrentValue.update v ec in
  let%lwt _run_until_the_end = Lwt.join [p_a; p_b; p_c] in
  let r = ConcurrentValue.get v in
  Lwt.return @@ Alcotest.(check (list string)) "return true" ["B"; "C"; "A"] r

let unpriority_dynamic_test2 _ () =
  let v = ConcurrentValue.create [] in
  let v' = ConcurrentValue.unpriority v in
  let p_a =
    let ea l =
      let%lwt () = Lwt_unix.sleep 3. in
      Lwt.return ("A" :: l) in
    let%lwt () = Lwt_unix.sleep 0. in
    ConcurrentValue.update v ea in
  let p_b =
    let eb l =
      let%lwt () = Lwt_unix.sleep 1. in
      Lwt.return ("B" :: l) in
    let%lwt () = Lwt_unix.sleep 1. in
    ConcurrentValue.update v' eb in
  let p_c =
    let ec l =
      let%lwt () = Lwt_unix.sleep 1. in
      Lwt.return ("C" :: l) in
    let%lwt () = Lwt_unix.sleep 2. in
    ConcurrentValue.update v ec in
  let%lwt _run_until_the_end = Lwt.join [p_a; p_b; p_c] in
  let r = ConcurrentValue.get v in
  Lwt.return @@ Alcotest.(check (list string)) "return true" ["C"; "B"; "A"] r

(* ============================= Listener tests ============================= *)

let listener_case case = "listener : " ^ case

let create_then_connect_then_trigger_test _ () =
  let listener = Listener.create () in
  let r = ref "FAIL" in
  let _connection_id = Listener.connect listener @@ fun v -> r := v in
  let v = "Success" in
  Listener.trigger listener v ;
  Lwt.return @@ Alcotest.(check string) "return true" v !r

let multiple_connect_and_trigger_test _ () =
  let listener = Listener.create () in
  let r1 = ref 1 in
  let r2 = ref 2 in
  let _connection_id = Listener.connect listener @@ fun v -> r1 := !r1 * v in
  let _connection_id = Listener.connect listener @@ fun v -> r2 := !r2 + v in
  Listener.trigger listener 2 ;
  Listener.trigger listener 3 ;
  let r = !r1 * !r2 in
  Lwt.return @@ Alcotest.(check int) "return true" 42 r

let disconnect_test _ () =
  let listener = Listener.create () in
  let v = "Success" in
  let r = ref v in
  let connection_id = Listener.connect listener @@ fun v -> r := v in
  let v' = "FAIL" in
  Listener.disconnect connection_id ;
  Listener.trigger listener v' ;
  Lwt.return @@ Alcotest.(check string) "return true" v !r

let override_storage_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 1 ;
  Listener.trigger listener 2 ;
  let r = ref 0 in
  let _connection_id = Listener.connect listener @@ fun v -> r := !r + v in
  Listener.trigger listener 3 ;
  Lwt.return @@ Alcotest.(check int) "return true" (2 + 3) !r

let use_storage_test _ () =
  let listener = Listener.create () in
  let v = "Success" in
  Listener.trigger listener v ;
  let r = ref "FAIL" in
  let _connection_id =
    Listener.connect ~use_storage:true listener @@ fun v -> r := v in
  Lwt.return @@ Alcotest.(check string) "return true" v !r

let do_not_use_storage_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 2 ;
  Listener.trigger listener 19 ;
  let r = ref 1 in
  let _connection_id =
    Listener.connect ~use_storage:false listener @@ fun v -> r := !r * v in
  Listener.trigger listener 2 ;
  Lwt.return @@ Alcotest.(check int) "return true" 2 !r

let fold_listener_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 1 ;
  let _connection_id, fold_listener = Listener.fold ( + ) 2 listener in
  Listener.trigger listener 3 ;
  let r = ref 0 in
  let _connection_id = Listener.connect fold_listener @@ fun v -> r := v in
  Listener.trigger listener 4 ;
  Lwt.return @@ Alcotest.(check int) "return true" (2 + 1 + 3 + 4) !r

let map_listener_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 1 ;
  let _connection_id, map_listener = Listener.map (fun v -> v * 2) listener in
  let r = ref 0 in
  let _connection_id = Listener.connect map_listener @@ fun v -> r := !r + v in
  Listener.trigger listener 2 ;
  Lwt.return @@ Alcotest.(check int) "return true" ((1 * 2) + (2 * 2)) !r

let filter_listener_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 2 ;
  let _connection_id, filter_listener =
    Listener.filter (fun v -> v mod 2 = 0) listener in
  Listener.trigger listener 3 ;
  let r = ref 0 in
  let _connection_id =
    Listener.connect filter_listener @@ fun v -> r := !r + v in
  Listener.trigger listener 4 ;
  Lwt.return @@ Alcotest.(check int) "return true" (2 + 4) !r

let filter_map_listener_test _ () =
  let listener = Listener.create () in
  Listener.trigger listener 2 ;
  let _connection_id, filter_map_listener =
    Listener.filter_map
      (fun v ->
        if v mod 2 = 0 then
          Some (v / 2)
        else
          None)
      listener in
  Listener.trigger listener 3 ;
  let r = ref 0 in
  let _connection_id =
    Listener.connect filter_map_listener @@ fun v -> r := !r + v in
  Listener.trigger listener 4 ;
  Lwt.return @@ Alcotest.(check int) "return true" ((2 / 2) + (4 / 2)) !r

(* ============================ BoundedMap tests ============================ *)

let bounded_map_case case = "bounded map : " ^ case

let invariant_test _ () =
  let module IntMap = Map.Make (Int) in
  let capacity = 3 in
  let module Capacity = struct
    let capacity = capacity
  end in
  let module BoundedIntMap = BoundedMap.Make (IntMap) (Capacity) in
  let assert_invariant bm = BoundedIntMap.cardinal bm <= capacity in
  let bm1 = BoundedIntMap.empty in
  let bm1 = BoundedIntMap.add 1 "1" bm1 in
  let bm1 = BoundedIntMap.add 2 "2" bm1 in
  let bm1 = BoundedIntMap.add 3 "3" bm1 in
  let bm1 = BoundedIntMap.add 4 "4" bm1 in
  let bm1 = BoundedIntMap.add 0 "0" bm1 in
  let b1 =
    assert_invariant bm1
    && (not (BoundedIntMap.mem 1 bm1))
    && not (BoundedIntMap.mem 0 bm1) in
  let bm2 =
    BoundedIntMap.of_seq @@ List.to_seq [(3, "3"); (5, "5"); (1, "1"); (0, "0")]
  in
  let b2 = assert_invariant bm2 && not (BoundedIntMap.mem 0 bm2) in
  let bm3 = BoundedIntMap.simple_union (fun _ _ v -> v) bm1 bm2 in
  let b3 = assert_invariant bm3 in
  let bm4 = BoundedIntMap.update_default 6 ~default:"6" (fun _ -> "6") bm3 in
  let b4 = assert_invariant bm4 in
  let l = BoundedIntMap.bindings bm4 |> List.map fst in
  let r = l = [4; 5; 6] in
  Lwt.return
  @@ Alcotest.(check bool) "return true" true (b1 && b2 && b3 && b4 && r)
