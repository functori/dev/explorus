(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2023, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_process.Dal
open Tezos.Entities

(* ================================= TYPES ================================= *)

module Slot = struct
  include Slot

  module Index = struct
    include Index

    type t = int [@@deriving jsoo]
  end

  type nonrec t = t = {
    header : Tezos.RPC.Types.Dal.slot_header;
    source : string;
    attested : bool option;
  }
  [@@deriving jsoo]
end

module SlotIndexes = Lib_js.Jsoo.Native.Map.Make (Slot.Index) (SlotIndexes)

module LevelMap = struct
  include Lib_js.Jsoo.Native.Map.Make (Level) (LevelMap)

  let reverse_jsoo_conv _a_conv_jsoo =
    let compare (level1, _) (level2, _) = Int.compare level2 level1 in
    sorted_jsoo_conv ~compare _a_conv_jsoo
end

type slots = Slot.t option SlotIndexes.t [@@deriving jsoo]

type state =
  (slots LevelMap.t[@jsoo.conv LevelMap.reverse_jsoo_conv slots_jsoo_conv])
[@@deriving jsoo]

type data = {
  tab : state; [@mutable]
  enabled : bool; [@mutable]
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-dal"

  let element =
    Vue_component.CRender (Render.dal_render, Render.dal_static_renders)

  let props = None
end)

(* ============================ DYNAMIC DISPLAY ============================ *)

let feature_enable () =
  let network_constants = Central_state.Network.get_network_constants () in
  Option.perform ~default:false
    ~perform:(fun { dp_feature_enable; _ } -> dp_feature_enable)
    network_constants.nc_dal_parametric

let dal_state_connection ~app =
  Listener.connect Tezos_process.Dal.listener @@ fun dal_states ->
  app##.tab := state_to_jsoo dal_states ;
  app##.enabled := Js_of_ocaml.Js.bool (feature_enable ())

let init () =
  let data _ =
    object%js
      val mutable tab = state_to_jsoo LevelMap.empty

      val mutable enabled = Js_of_ocaml.Js.bool (feature_enable ())
    end in
  let at_destroy = PendingFunctions.create () in
  C.make
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            let dal_state_connection_id = dal_state_connection ~app in
            PendingFunctions.add at_destroy @@ fun () ->
            Listener.disconnect dal_state_connection_id );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ~data ()
