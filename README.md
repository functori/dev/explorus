# Explorus

An explorer, yes, but a consensus explorer.

A live version of **Explorus** can be found [here](https://explorus.io/).

## Dependencies

Create a local opam switch or ignore this step if you want to use a global switch:

    make opam-switch

Then install ocaml dependencies:

    make deps

To install the dependencies related to development:

    make dev-deps

## Configuration

### Network

There is a configuration file at `json/parameters.json` that looks like:

```json
{
    "mainnet": {
        "chain_name": "TEZOS_MAINNET",
        "addresses": ["https://mainnet.api.tez.ie"]
    }
}
```

It is the minimal configuration in order for **Explorus** to work, you can of course add your own
api/node addresses instead of those we use.

**Explorus** is currently compatible with `Mainnet`, `Ghostnet`, `Quebecnet`, `Rionet`, `Nextnet` and `Weeklynet`, so you can add a section just like for `Mainnet` in `json/parameters.json`.

For example, let's say you want to add a `Ghostnet` section:

```json
{
    "mainnet": {
        "chain_name": "TEZOS_MAINNET",
        "addresses": ["https://mainnet.api.tez.ie"]
    },
    "ghostnet": {
        "chain_name": "TEZOS_ITHACANET_2022-01-25T15:00:00Z",
        "addresses": ["https://rpc.ghostnet.teztnets.com", "https://ghostnet.ecadinfra.com"]
    }
}
```

You can add multiple addresses on each network. If one of the node crashes, an other one will take
the lead.

### Components

**Explorus** has a multi-components architecture. This way, we made it possible to _enable_ or
_disable_ any component that we want (very easily) directly from `json/components_states.json`:

```json
{
    "baking_rights": true,
    "round_clock": true,
    "latest_blocks": true,
    "consensus_ops": true,
    "bakers_activity": true,
    "explorer": true,
    "soru": true,
    "dal": true,
    "info_bar": true,
    "consensus_progression": true,
    "network_constants": true,
    "share_url": true,
    "pyrometer": true,
    "protocol_injection": true,
    "logs": true,
    "settings": true
}
```

You just need to put at _true_ or _false_ any of these attributes to _enable_ or _disable_ a
specific component.

## Build

Start building the project:

    make

If you want to launch the app locally, you can use the following:

    make php

## Tests

Several tests can be done on this project.

OCaml files formatting can be checked by doing:

    make lint

HTML files formatting can be checked by doing:

    make lint-html

Any trace of dead css code can be checked by doing:

    make track-dead-minimal-css

Source code is partially checked (see the details in `tests` folder) by doing:

    make alcotest

To check if the hard refresh will not break (on deploy):

    make check-explorus-index

## Format and indentation

Use the .ocamlformat in this repository, and use the following makefile-rule to format all the ocaml
files:

    make format

Use the .prettierrc.json in this repository, and use the following makefile-rule to format all the
html files:

    make format-html

## Additional Informations

* All the static files are in `static/` directory and the generate files are in `_www/`.
* Explorus supports baker aliases. In case, there's a need to add one or multiple aliases, please
  submit an issue on the repository and/or contact the maintainers on Slack or Discord.

## Maintainers

-   Rodi-Can Bozman [@rodibozman](https://gitlab.com/rodibozman)
-   Sébastien Palmer [@spalmer25](https://gitlab.com/spalmer25)

## License

Copyright © 2022-2024, Functori <contact@functori.com>. Released under the
[MIT License](https://gitlab.com/functori/dev/explorus/-/blob/main/LICENSE).
