(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos.Entities
open Rpc

type operation = {
  sc_op_contents : operation_content list;
  sc_op_path : string;
}
[@@deriving jsoo]

type block_info = {
  sc_bi_hash : string;
  sc_bi_level : int;
  sc_bi_round : int;
}
[@@deriving jsoo]

type block = {
  sc_bl_info : block_info;
  sc_bl_operations : operation list;
  sc_bl_nb_operations : int;
}
[@@deriving jsoo]

type packed_blocks =
  | Block of block
  | Empty_block of block_info
  | Empty_blocks of {
      first : block_info;
      last : block_info;
    }
[@@deriving jsoo]

type blocks = packed_blocks list [@@deriving jsoo]

type round_info = Tezos_process.Clock.round_info = {
  curr_round : int;
  curr_max_round : int;
  curr_pos_round : int;
}
[@@deriving jsoo]

type data = {
  blocks : blocks; [@mutable]
  round_info : round_info option; [@mutable]
  cached_blocks : int;
}
[@@deriving jsoo { remove_prefix = false }]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-explorer"

  let element =
    Vue_component.CRender
      (Render.explorer_render, Render.explorer_static_renders)

  let props = None
end)

module PackedBlocksMap =
  BoundedMap.Make
    (LevelRoundMap)
    (struct
      let capacity = Konstant.maximum_non_empty_blocks
    end)

module PBM = PackedBlocksMap

let filter_operation_content = function
  | Smart_rollup_originate _
  | Contract_transaction _
  | User_transaction _
  | Origination _ -> true
  | Attestation _
  | Preattestation _
  | Dal_publish_commitment _
  | Smart_rollup_refute _
  | Smart_rollup_timeout _
  | Ignored_operation _ -> false

let filter_operation ~hash index { op_contents; _ } =
  match List.filter filter_operation_content op_contents with
  | [] -> Lwt.return_none
  | sc_op_contents ->
    let%lwt sc_op_path =
      get_block_operations_by_index_path ~bid:(BlockId.Hash hash) ~index in
    Lwt.return_some { sc_op_contents; sc_op_path }

let operations_weight operations =
  List.fold_left
    (fun acc operation -> acc + List.length operation.sc_op_contents)
    0 operations

let info_ops_of block LevelRound.{ level; round } =
  let hash = block.bl_hash in
  let operations = block.bl_operations.ops_monitor_ops in
  let%lwt operations =
    Lwt_list.filter_mapi_s (filter_operation ~hash) operations in
  let nb_operations = operations_weight operations in
  let block_info =
    { sc_bi_hash = hash; sc_bi_level = level; sc_bi_round = round } in
  Lwt.return
    {
      sc_bl_info = block_info;
      sc_bl_operations = operations;
      sc_bl_nb_operations = nb_operations;
    }

let packed_block_of info_ops =
  if List.is_empty info_ops.sc_bl_operations then
    Empty_block info_ops.sc_bl_info
  else
    Block info_ops

let packed_blocks_weight = function
  | Block block -> block.sc_bl_nb_operations
  | Empty_block _ | Empty_blocks _ -> 0

let get_shrunk_list blocks =
  let operation_slot_available (weight, _) _ =
    weight < Konstant.threshold_explorer_operations in
  let add (weight, blocks) packed_block =
    let weight = packed_blocks_weight packed_block + weight in
    let blocks = packed_block :: blocks in
    (weight, blocks) in
  let _weight, blocks_as_list =
    List.fold_left_while operation_slot_available add (0, [])
      (PBM.bindings_without_keys blocks) in
  List.rev blocks_as_list

let set_new_block app blocks block =
  let%lwt () =
    ConcurrentValue.update blocks @@ fun blocks ->
    let latest_block = PBM.max_binding_opt blocks in
    let key = Process.get_level_round block in
    let%lwt info_ops = info_ops_of block key in
    let get_new_blocks ?(key = key) block = PBM.add key block blocks in
    let new_blocks =
      match latest_block with
      | None -> PBM.add key (packed_block_of info_ops) blocks
      | Some (latest_block_key, latest_block_added) -> (
        match (info_ops.sc_bl_operations, latest_block_added) with
        | [], (Empty_blocks { first = first_block; _ } | Empty_block first_block)
          ->
          get_new_blocks ~key:latest_block_key
            (Empty_blocks { first = first_block; last = info_ops.sc_bl_info })
        | [], _ -> get_new_blocks (Empty_block info_ops.sc_bl_info)
        | _not_empty, _ -> get_new_blocks (Block info_ops)) in
    Lwt.return new_blocks in
  let blocks_as_list = get_shrunk_list (ConcurrentValue.get blocks) in
  app##.blocks := blocks_to_jsoo blocks_as_list ;
  Lwt.return_unit

let refresh_progression_bar app =
  let callback () =
    let round_info = Tezos_process.Clock.get_round_info () in
    app##.round_info_ := def (round_info_to_jsoo round_info) in
  let open Central_state.Intervals in
  add_intervals ~clearable_type:Explorer ~callback ~loop_frequency:1.

let fill_with_cache app blocks =
  Cache.LevelRoundBlocks.iter_intra_lwt
    (fun block -> set_new_block app blocks block)
    (ConcurrentValue.get Central_state.Cache.lr_blocks)

let init () =
  let data _ =
    object%js
      val mutable blocks = blocks_to_jsoo []

      val mutable round_info_ = undefined

      val cached_blocks_ = Konstant.small_cache_size
    end in
  let set_head app blocks =
    Lwt.async_exn ~__FUNCTION__ @@ fun () ->
    if
      Cache.LevelRoundBlocks.is_empty
      @@ ConcurrentValue.get Central_state.Cache.lr_blocks
    then
      fill_with_cache app blocks
    else
      let%lwt head = get_block () in
      set_new_block app blocks head in
  let at_destroy = PendingFunctions.create () in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            (* we pre-emptively set head to display something as fast as
               possible regardless of what the listener is processing *)
            let packed_blocks = ConcurrentValue.create PBM.empty in
            let () = set_head app packed_blocks in
            let () = refresh_progression_bar app in
            let id =
              Listener.connect ~use_storage:true Tezos_process.heads_listener
              @@ fun heads ->
              Lwt.async_exn ~__FUNCTION__ @@ fun () ->
              Lwt_list.iter_s
                (fun Tezos_process.TS.{ block; _ } ->
                  set_new_block app packed_blocks block)
                heads in
            PendingFunctions.add at_destroy @@ fun () -> Listener.disconnect id
        );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ()
