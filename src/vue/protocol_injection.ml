(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type data = {
  head_level : int option; [@mutable]
  protocol_transition_level : int;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  type data = data_jsoo

  type all = data

  let name = "v-protocol_injection"

  let element =
    Vue_component.CRender
      ( Render.protocol_injection_render,
        Render.protocol_injection_static_renders )

  let props = None
end)

let set_head ~app { mh_level; _ } = app##.head_level_ := def mh_level

let init ~protocol_transition_level () =
  let data _ =
    object%js
      val mutable head_level_ = undefined

      val protocol_transition_level_ = protocol_transition_level
    end in
  let at_destroy = PendingFunctions.create () in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            let id =
              Listener.connect Rpc.HDStream.listener (List.iter (set_head ~app))
            in
            PendingFunctions.add at_destroy @@ fun () -> Listener.disconnect id
        );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ()
