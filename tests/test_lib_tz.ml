(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2024, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let address_validity_true_test _ () =
  let cases =
    [
      "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YRs";
      "tz2C87aV3ugBdkeKyU1YxcLM7MbvS4utDCQW";
      "tz3ZmB8oWUmi8YZXgeRpgAcPnEMD8VgUa4Ve";
      "tz4HVR6aty9GReMNma1NjVQGFFghhYQD8YCM";
    ] in
  let cases_validity = List.map Lib_tz.is_address_valid cases in
  let valid = List.fold_left ( && ) true cases_validity in
  Lwt.return @@ Alcotest.(check bool) "return true" true valid

let address_validity_false_test _ () =
  let cases =
    [
      "tz1Zt8QQ9aBznYNk5LUBjtME9luExomw9YRs";
      (* tz1 with 'l' *)
      "tz2C87aV3ugBdkeKyU1YxcL07MbvS4utDCQW";
      (* tz2 with '0' *)
      "tz3ZmB8oWUmi8YZXgeRpgAIPnEMD8VgUa4Ve";
      (* tz3 with 'I' *)
      "tz4HVR6aty9GReMNma1NjVQGFFghhOQD8YCM";
      (* tz4 with 'O' *)
      "tz1Zt8QQ9aBznYNk5LUBjtME9DuExomw9YR";
      (* wrong size *)
      "NetXdQprcVkpaWU" (* wrong prefix *);
    ] in
  let cases_validity = List.map Lib_tz.is_address_valid cases in
  let not_valid = List.fold_left ( || ) false cases_validity in
  Lwt.return @@ Alcotest.(check bool) "return false" false not_valid
