NODE_PATH ?= $(PWD)/node_modules

.PHONY: tests

all: copy

build:
	@dune build --profile release

copy:
	@dune build --profile release
	@mkdir -p _www
	@rsync -r static/* _www
	@cp -f _build/default/src/vue/main.bc.js _www/explorus.js

clean:
	@dune clean
	@rm -rf _www

opam-switch:
	@opam switch create . 4.14.1 -y --no-install
	eval $(opam env)

deps:
	@scripts/install-deps.sh

dev-deps:
	@opam install ocaml-lsp-server
	@npm install --save-dev --save-exact prettier

install:
	@opam install .

# =========> DEPLOYMENT-COMMANDS

hard-refresh:
	@scripts/auto-hard-refresh.sh

restore-refresh:
	git checkout static/index.html

build-for-deploy:
	@dune build --profile release
	@scripts/auto-hard-refresh.sh
	@mkdir -p _www
	@rsync -r static/* _www
	@cp -f _build/default/src/vue/main.bc.js _www/explorus.js

# =========> LOCAL-LAUNCH

php:
	cd _www/ && php -S localhost:8888

local:
	@make all
	@cd _www/ && php -S localhost:8888

# =========> FORMATTING

format:
	@dune build @fmt --auto-promote --display=quiet 2> /dev/null || exit 0

format-html:
	@npx prettier --write html/*.html

# =========>  TEST-SCRIPTS

lint:
	@scripts/lint.sh

lint-html:
	@scripts/lint-html.sh

track-dead-minimal-css:
	@scripts/track-dead-css-from.sh

check-explorus-index:
	@scripts/check-static-explorus-pos.sh

alcotest:
	@scripts/alcotest.sh

ci:
	@echo "=======> LINT <======="
	@scripts/lint.sh
	@echo "=======> LINT HTML <======="
	@scripts/lint-html.sh
	@echo "=======> TRACK DEAD CSS <======="
	@scripts/track-dead-css-from.sh
	@echo "=======> CHECK HARD REFRESH <======="
	@scripts/check-static-explorus-pos.sh
	@echo "=======> ALCOTEST <======="
	@scripts/alcotest.sh

# =========>  ALCOTEST-COMMANDS

build-alcotest:
	@PPX_JSOO_FAKE=true dune build tests/main.exe

exec-alcotest:
	@./_build/default/tests/main.exe

exec-alcotest-quick:
	@./_build/default/tests/main.exe -q

all-alcotest:
	@make build-alcotest
	@make exec-alcotest

-include Makefile.config
