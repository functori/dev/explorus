(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Rpc

module HashShortening = struct
  module HashShortening = Central_state.Settings.HashShortening

  type kind = HashShortening.kind =
    | End
    | Middle
  [@@deriving jsoo]

  type item = {
    kind : kind;
    descr : string;
  }
  [@@deriving jsoo]

  type items = item list [@@deriving jsoo]

  type data = {
    selected : kind; [@mutable]
    items : items;
  }
  [@@deriving jsoo]

  let items =
    let dummy_hash = "tz1hUXU4DPHPyrEEekqhmEEJvdCpB2gP4qt" in
    let make_item kind =
      { kind; descr = HashShortening.shorten_tz kind dummy_hash } in
    List.map make_item HashShortening.all_kind

  let current () = { selected = HashShortening.get (); items }

  let set data = HashShortening.set (kind_of_jsoo data##.selected)
end

type data = {
  api_node_address : string; [@mutable]
  error_wrong_address : bool; [@mutable]
  hash_shorten_setting : HashShortening.data;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all = data

  let name = "v-settings"

  let element =
    Vue_component.CRender
      (Render.settings_render, Render.settings_static_renders)

  let props = None
end)

let get_form_values app =
  app##.error_wrong_address_ := _false ;
  let address =
    String.trim @@ Pretty.preprocess_local_address
    @@ to_string app##.api_node_address_ in
  Lwt.async_exn ~__FUNCTION__ @@ fun _ ->
  if%lwt get_address_validity_test address then (
    Central_state.API.set_custom_address_persistent_on_success address ;
    Lwt.return @@ Js_of_ocaml.Dom_html.window##.location##reload
  ) else (
    if address <> "" then app##.error_wrong_address_ := _true ;
    Lwt.return_unit
  )

let reset_custom_address _app =
  Central_state.API.reset_custom_address () ;
  Js_of_ocaml.Dom_html.window##.location##reload

let init () =
  C.add_method0 "get_form_values" get_form_values ;
  C.add_method0 "reset_custom_address" reset_custom_address ;
  C.add_method0 "set_hash_shorten" (fun app ->
      HashShortening.set app##.hash_shorten_setting_) ;
  C.make
    ~data:(fun _ ->
      object%js
        val mutable api_node_address_ = string ""

        val mutable error_wrong_address_ = _false

        val hash_shorten_setting_ = HashShortening.(data_to_jsoo (current ()))
      end)
    ()
