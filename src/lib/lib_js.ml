(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

let log_verbose = function
  | None -> ()
  | Some verbose -> Ezjs_min.log "%s\n" verbose

let log_ez_req_lwt_err ?verbose err =
  log_verbose verbose ;
  match err with
  | EzReq_lwt_S.KnownError { code; error } ->
    Ezjs_min.log "%d %s" code (Printexc.to_string error)
  | EzReq_lwt_S.UnknownError { code; msg } ->
    Ezjs_min.log "%d %s" code
      (Option.value ~default:(__FUNCTION__ ^ " : no_err_msg") msg)

let destruct_json json encoding =
  let ic = open_in json in
  let buffer = really_input_string ic (in_channel_length ic) in
  close_in ic ;
  EzEncoding.destruct encoding buffer

let destruct_json_lwt json encoding =
  match%lwt EzReq_lwt.get (EzAPI.URL json) with
  | Ok result -> Lwt.return @@ EzEncoding.destruct encoding result
  | Error _ -> Lwt.fail_with @@ __FUNCTION__ ^ " : cannot get result"

let alert str = Js_of_ocaml.(Dom_html.window##alert (Js.string str))

let scrollTo ?(left = 0.) ?(top = 0.) elt : unit =
  (Js_of_ocaml.Js.Unsafe.coerce elt)##scrollTo left top

let get_child elt i =
  let open Js_of_ocaml in
  let open Js in
  Opt.bind (elt##.childNodes##item i) Dom_html.CoerceTo.element |> Opt.to_option

module Destructible : sig
  type t

  type jsoo

  val to_jsoo : t -> jsoo Ezjs_min.t

  val of_jsoo : jsoo Ezjs_min.t -> t

  val jsoo_conv : (t -> jsoo Ezjs_min.t) * (jsoo Ezjs_min.t -> t)

  val init : ?at_destroy:(unit -> unit) -> unit -> jsoo Ezjs_min.t

  val at_destroy : jsoo Ezjs_min.t -> (unit -> unit) -> unit

  val destroy : jsoo Ezjs_min.t -> unit
end = struct
  type t = { at_destroy : unit -> unit [@mutable] } [@@deriving jsoo]

  let init ?(at_destroy = fun () -> ()) () : jsoo Ezjs_min.t =
    object%js
      val mutable at_destroy_ = at_destroy
    end

  let at_destroy (destructible : jsoo Ezjs_min.t) f =
    let at_destroy = destructible##.at_destroy_ in
    let at_destroy () =
      f () ;
      at_destroy () in
    destructible##.at_destroy_ := at_destroy

  let destroy (destructible : jsoo Ezjs_min.t) : unit =
    destructible##.at_destroy_ ()
end

module Jsoo = struct
  module Make (Js : sig
    type 'a t
  end) =
  struct
    module type OrderedType = sig
      type t

      val compare : t -> t -> int

      type jsoo

      val to_jsoo : t -> jsoo Js.t

      val of_jsoo : jsoo Js.t -> t

      val jsoo_conv : (t -> jsoo Js.t) * (jsoo Js.t -> t)
    end

    module Set = struct
      module type S = sig
        include Set.S

        type key_jsoo

        class type jsoo = [key_jsoo Js.t] Ezjs_min.js_array

        val to_jsoo : t -> jsoo Ezjs_min.t

        val of_jsoo : jsoo Ezjs_min.t -> t

        val jsoo_conv : (t -> jsoo Ezjs_min.t) * (jsoo Ezjs_min.t -> t)

        val to_sorted_jsoo :
          ?compare:(elt -> elt -> int) -> t -> jsoo Ezjs_min.t

        val sorted_jsoo_conv :
          compare:(elt -> elt -> int) ->
          (t -> jsoo Ezjs_min.t) * (jsoo Ezjs_min.t -> t)
      end

      module Make (Ord : OrderedType) (Set : Set.S with type elt = Ord.t) :
        S with type t = Set.t and type elt = Ord.t = struct
        include Set

        type key_jsoo = Ord.jsoo

        class type jsoo = [Ord.jsoo Js.t] Ezjs_min.js_array

        let to_sorted_jsoo ?compare set =
          let elements = elements set in
          let elements =
            match compare with
            | None -> elements
            | Some compare -> List.sort compare elements in
          Ezjs_min.of_listf Ord.to_jsoo elements

        let to_jsoo set = to_sorted_jsoo ?compare:None set

        and of_jsoo elements = of_list (Ezjs_min.to_listf Ord.of_jsoo elements)

        let jsoo_conv = (to_jsoo, of_jsoo)

        let sorted_jsoo_conv ~compare = (to_sorted_jsoo ~compare, of_jsoo)
      end
    end

    module Map = struct
      module type S = sig
        include Map.S

        type 'a binding_jsoo

        class type ['a] jsoo = ['a binding_jsoo Ezjs_min.t] Ezjs_min.js_array

        val to_jsoo : ('a -> 'b) * ('b -> 'a) -> 'a t -> 'b jsoo Ezjs_min.t

        val of_jsoo : ('a -> 'b) * ('b -> 'a) -> 'b jsoo Ezjs_min.t -> 'a t

        val jsoo_conv :
          (('a -> 'b) * ('b -> 'a) -> 'a t -> 'b jsoo Ezjs_min.t)
          * (('a -> 'b) * ('b -> 'a) -> 'b jsoo Ezjs_min.t -> 'a t)

        val to_sorted_jsoo :
          ?compare:(key * 'a -> key * 'a -> int) ->
          ('a -> 'b) * ('b -> 'a) ->
          'a t ->
          'b jsoo Ezjs_min.t

        val sorted_jsoo_conv :
          compare:(key * 'a -> key * 'a -> int) ->
          ('a -> 'b) * ('b -> 'a) ->
          ('a t -> 'b jsoo Ezjs_min.t) * ('b jsoo Ezjs_min.t -> 'a t)
      end

      module Make (Ord : OrderedType) (Map : Map.S with type key = Ord.t) :
        S with type 'a t = 'a Map.t and type key = Ord.t = struct
        include Map

        class type ['a] binding_jsoo =
          object
            method _0 : Ord.jsoo Js.t Ezjs_min.readonly_prop

            method _1 : 'a Ezjs_min.readonly_prop
          end

        let binding_to_jsoo (_a_to_jsoo, _a_of_jsoo) (key, binding) =
          [%js
            object (_this, true)
              val _0 = Ord.to_jsoo key

              val _1 = _a_to_jsoo binding
            end]

        and binding_of_jsoo (_a_to_jsoo, _a_of_jsoo) binding =
          (Ord.of_jsoo binding##._0, _a_of_jsoo binding##._1)

        class type ['a] jsoo = ['a binding_jsoo Ezjs_min.t] Ezjs_min.js_array

        let to_sorted_jsoo ?compare _a_conv_jsoo map =
          let bindings = bindings map in
          let bindings =
            match compare with
            | None -> bindings
            | Some compare -> List.sort compare bindings in
          Ezjs_min.of_listf (binding_to_jsoo _a_conv_jsoo) bindings

        let to_jsoo _a_conv_jsoo map =
          to_sorted_jsoo ?compare:None _a_conv_jsoo map

        and of_jsoo _a_conv_jsoo bindings =
          of_list (Ezjs_min.to_listf (binding_of_jsoo _a_conv_jsoo) bindings)

        let jsoo_conv = (to_jsoo, of_jsoo)

        let sorted_jsoo_conv ~compare _a_conv_jsoo =
          (to_sorted_jsoo ~compare _a_conv_jsoo, of_jsoo _a_conv_jsoo)
      end
    end
  end

  include Make (struct
    type 'a t = 'a Ezjs_min.t
  end)

  module Native = Make (struct
    type 'a t = 'a
  end)
end

(* ================================ STORAGE ================================ *)

module Storage = struct
  module type T = sig
    type t

    val name : string

    val to_string : t -> string

    val of_string : string -> t
  end

  module type S = sig
    type t

    val pretty : t -> string

    val get_opt : unit -> t option

    val get : unit -> t

    val set : t -> unit

    val remove : unit -> unit
  end

  module Make (T : T) : S with type t = T.t = struct
    open Js_of_ocaml

    type t = T.t

    let name = Js.string T.name

    let pretty item = Printf.sprintf "{ %s : %s }" T.name (T.to_string item)

    let get_opt_to_string () =
      let local_storage_opt =
        Js.Optdef.to_option Dom_html.window##.localStorage in
      let item_option =
        Option.bind local_storage_opt @@ fun local_storage ->
        Js.Opt.to_option (local_storage##getItem name) in
      Option.map Js.to_string item_option

    let get_opt () = Option.map T.of_string (get_opt_to_string ())

    let get () = Option.value ~default:"" (get_opt_to_string ()) |> T.of_string

    let set item =
      Js.Optdef.iter Dom_html.window##.localStorage @@ fun local_storage ->
      local_storage##setItem name (Js.string (T.to_string item))

    let remove () =
      Js.Optdef.iter Dom_html.window##.localStorage @@ fun local_storage ->
      local_storage##removeItem name
  end

  module MakeString (T : sig
    val name : string
  end) : S with type t = string = Make (struct
    type t = string

    let name = T.name

    let to_string s = s

    let of_string s = s
  end)
end

(* ================================= STREAM ================================= *)

exception Stream_error of string

(** [run_stream ~path fold init] run a stream on [path]. Strings get from the
    stream will be fold by [fold] starting with [init]. Raises Stream_error
    execption at stream error *)
let run_stream ~path fold init =
  let stream_ending_promise, stream_ending = Lwt.task () in
  let promise_canceled = ref false in
  let cancel_promise () = promise_canceled := true in
  let fold acc res =
    (* the fold will stop when the promise is canceled *)
    if !promise_canceled then
      Error None
    else
      Ok (fold acc res) in
  let end_of_stream = function
    | Ok Ezjs_fetch.{ body; _ } -> Lwt.wakeup stream_ending body
    | Error err ->
      let err = Js_of_ocaml.Js.to_string err##toString in
      Lwt.wakeup_exn stream_ending (Stream_error err) in
  let () =
    try Ezjs_fetch.fetch path (Ezjs_fetch.to_str_stream fold init) end_of_stream
    with exn ->
      Lwt.wakeup_exn stream_ending exn ;
      cancel_promise () in
  Lwt.on_cancel stream_ending_promise cancel_promise ;
  stream_ending_promise
