#!/bin/sh

descr (){
    echo "Run a sandbox node for which the default RPC port is 20000."
    echo ""
    echo "Tezos and Flextesa binaries are required to run."
    echo "The Tezos binaries are available at git@gitlab.com:tezos/tezos.git (at least version v15.0)."
    echo "The Flextesa binary is available at git@gitlab.com:tezos/flextesa.git."
    echo ""
}

usage (){
    echo "Usage: ./sandbox_node.sh --flextesa_path string --tezos_path string ..."
    echo ""
    echo "  --flextesa_path string   path to flextesa binary"
    echo "  --tezos_path string      path to tezos binaries"
    echo ""
    echo "The other arguments are those of flextesa"
    echo "Some are listed below:"
    echo ""
    echo "  --protocol-kind=PROTOCOL-KIND                     the protocol type"
    echo "  --hard-fork=LEVEL:PROTOCOL-KIND:[PROTOCOL-HASH]   to do a hard fork"
    echo "  --base-port=PORT                                  the RPC port"
    echo "  --time-between-blocks=COMMA-SEPARATED-SECONDS     time between-blocks"
    echo ""
    echo "Example:"
    echo "  './sandbox_node.sh --flextesa_path <flextesa_bin_path> --tezos_path <tezos_dir_absolute_path> \\"
    echo "                     --protocol-kind=Kathmandu --hard-fork=20:Lima: --base-port=7888 --time-b=5,15"
    echo ""
    echo "  Will run a node on a new network such that :"
    echo "   - the network starts on the 'Kathmandu' protocol"
    echo "   - the network will change its protocol at level 20 to 'Lima'"
    echo "   - the network will have 5s of 'minimal_block_delay' and 15s of 'delay_increment_per_round'"
    echo "   - the port for RPC to the node will be 7888."
    echo ""
    echo "See 'flextesa mini-network --help' for more information about flextesa arguments."
    echo ""
}

help (){
    echo ""
    descr
    usage
    exit 0
}

error (){
    echo ""
    echo "Error: $1."
    echo ""
    usage
    exit 1
}

if ! command -v netstat > /dev/null
then
    echo "The command netstat is needed to run flextesa:"
    sudo apt-get install net-tools
fi

ARGS=""

UNTIL_LEVEL="200_000_000"

while [ $# -gt 0 ]; do
    case "$1" in
	--flextesa_path|--tezos_path)
	    if [ -z "$2" ]
	    then error "Invalid argument"
	    fi
	    export PATH="$2":$PATH
	    shift
	    ;;
	--help)
	    help;;
	--until-level=*)
	    UNTIL_LEVEL="${1#*=}";;
	*)
	    ARGS="$ARGS $1"
    esac
    shift
done

export PATH="$PWD":$PATH

export flextesa_node_cors_origin="*"

if ! command -v flextesa > /dev/null
then error "flextesa not found"
fi

flextesa mini-network --until-level=$UNTIL_LEVEL $ARGS || help
