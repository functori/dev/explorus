(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* `Slow tests are specific to certain protocol *)

let test_case = Alcotest_lwt.test_case

let test_service_encoding =
  let open Test_services_encodings in
  let which_test =
    let open Test_filter in
    (* Here is a more detailed example of how you can customise
       'service/encoding' tests :

       [ Mainnet ([Head (); Genesis ()], All_at_once); Ghostnet (get_bids (),
       One_by_one) ] *)
    all_network ([Head ()], All_at_once) in
  tests ~which_test ()

let lib_test_cases =
  let open Test_lib in
  [
    test_case
      (tempotbl_case "is one value properly cleaned")
      `Quick is_one_value_properly_cleaned_test;
    test_case
      (tempotbl_case "are values properly cleaned")
      `Quick are_values_properly_cleaned_test;
    test_case
      (tempotbl_case "is value duration properly extended")
      `Quick is_value_duration_properly_extended_test;
    test_case
      (tempotbl_case "is value duration not extended")
      `Quick is_value_duration_not_extended_test;
    test_case
      (tempotbl_case "is cleaning routine triggered once and only once")
      `Quick is_cleaning_routine_triggered_once_and_only_once_test;
    test_case (concurrent_case "create then get") `Quick create_then_get_test;
    test_case (concurrent_case "apply") `Quick apply_test;
    test_case
      (concurrent_case "apply_with_update")
      `Quick apply_with_update_test;
    test_case (concurrent_case "update") `Quick update_test;
    test_case (concurrent_case "simple_update") `Quick simple_update_test;
    test_case (concurrent_case "update0") `Quick update0_test;
    test_case (concurrent_case "update1") `Quick update1_test;
    test_case (concurrent_case "update order") `Quick update_order_test;
    test_case
      (concurrent_case "unpriority same value")
      `Quick unpriority_same_value_test;
    test_case
      (concurrent_case "unpriority same value update")
      `Quick unpriority_same_value_update_test;
    test_case (concurrent_case "unpriority order") `Quick unpriority_order_test;
    test_case
      (concurrent_case "unpriority dynamic 1")
      `Quick unpriority_dynamic_test1;
    test_case
      (concurrent_case "unpriority dynamic 2")
      `Quick unpriority_dynamic_test2;
    test_case
      (listener_case "create then connect then trigger")
      `Quick create_then_connect_then_trigger_test;
    test_case
      (listener_case "multiple connect and trigger")
      `Quick multiple_connect_and_trigger_test;
    test_case (listener_case "disconnect") `Quick disconnect_test;
    test_case (listener_case "override storage") `Quick override_storage_test;
    test_case (listener_case "use_storage") `Quick use_storage_test;
    test_case
      (listener_case "do not use storage")
      `Quick do_not_use_storage_test;
    test_case (listener_case "fold listener") `Quick fold_listener_test;
    test_case (listener_case "map listener") `Quick map_listener_test;
    test_case (listener_case "filter listener") `Quick filter_listener_test;
    test_case
      (listener_case "filter_map listener")
      `Quick filter_map_listener_test;
    test_case (bounded_map_case "invariant") `Quick invariant_test;
  ]

let lib_tz_test_cases =
  let open Test_lib_tz in
  [
    test_case "tezos address validity [true]" `Quick address_validity_true_test;
    test_case "tezos address validity [false]" `Quick
      address_validity_false_test;
  ]

let parameters_test_cases =
  let open Test_parameters in
  [
    test_case "path validity" `Quick test_path;
    test_case "encoding validity" `Quick test_encoding;
  ]

let route_paths_test_cases =
  [test_case "route paths invariance" `Quick Test_paths.test_paths]

let known_bakers_test_cases =
  let open Test_known_bakers in
  [
    test_case "path validity" `Quick test_path;
    test_case "encoding validity" `Quick test_encoding;
    test_case "functori existence" `Quick test_functori;
    test_case "yellow existence" `Quick test_yellow;
  ]

let components_states_test_cases =
  let open Test_components_states in
  [
    test_case "path validity" `Quick test_path;
    test_case "encoding validity" `Quick test_encoding;
    test_case "states validity" `Quick test_all_components_are_active;
  ]

let tezos_entities_test_cases =
  let open Test_tezos_entities in
  [
    test_case "soru commitment tree : conform tree construction" `Quick
      Soru.Commitment_tree.tree_coequality_test;
  ]

let () =
  Lwt_main.run
  @@ Alcotest_lwt.run ~show_errors:true "Explorus"
       ([
          ("lib stressing", lib_test_cases);
          ("lib tz stressing", lib_tz_test_cases);
          ("parameters", parameters_test_cases);
          ("route paths", route_paths_test_cases);
          ("known bakers", known_bakers_test_cases);
          ("components states", components_states_test_cases);
          ("tezos entities", tezos_entities_test_cases);
        ]
       @ test_service_encoding)
