(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos.RPC.Types

(* ========================== SIMULATION FUNCTIONS ========================== *)

let get_block ~__FUNCTION__ ~api ~bid =
  match%lwt EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.block bid with
  | Ok block -> Lwt.return block
  | Error err ->
    Lib_tst.log_ez_req_lwt_err err ;
    Lib_tst.log_path
      (EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.block bid []) ;
    Lwt.fail_with
    @@ Log.log_sf "%s API: %s, get_block : cannot get block : %s" __FUNCTION__
         api bid

let get_delegates ~__FUNCTION__ ~api ~bid =
  let active = Tezos.RPC.Services.Params.make_active_param ~active:true () in
  let params = Tezos.RPC.Services.Params.get_params [active] in
  match%lwt
    EzReq_lwt.get1 ~params (EzAPI.BASE api) Tezos.RPC.Services.delegates bid
  with
  | Ok delegates -> Lwt.return delegates
  | Error err ->
    Lib_tst.log_ez_req_lwt_err err ;
    Lib_tst.log_path
      (EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.delegates bid params) ;
    Lwt.fail_with
    @@ Log.log_sf
         "%s API: %s, get_delegates : cannot get delegates from block : %s"
         __FUNCTION__ api bid

module SORU_HELPER = struct
  exception Empty_address_book

  exception No_stakers_available

  let get_one_soru_address ~api =
    match%lwt
      EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.soru_addresses
    with
    | Ok soru_addresses -> begin
      match soru_addresses with
      | [] -> raise Empty_address_book
      | soru_address :: _ -> Lwt.return soru_address
    end
    | Error err ->
      Lib_tst.log_ez_req_lwt_err err ;
      Lib_tst.log_path
        (EzAPI.forge0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.soru_addresses
           []) ;
      Lwt.fail_with
      @@ Log.log_sf "%s API: %s, cannot get soru addresses" __FUNCTION__ api

  let get_lcc ~api ~soru_address =
    match%lwt
      EzReq_lwt.get1 (EzAPI.BASE api)
        Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
        soru_address
    with
    | Ok lcc -> Lwt.return lcc
    | Error err ->
      Lib_tst.log_ez_req_lwt_err err ;
      Lib_tst.log_path
        (EzAPI.forge1 (EzAPI.BASE api)
           Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
           soru_address []) ;
      Lwt.fail_with
      @@ Log.log_sf "%s API: %s, cannot get lcc for rollup %s" __FUNCTION__ api
           soru_address

  let get_inbox_level ~api ~soru_address =
    let%lwt Tezos.RPC.Types.SORU.L1.{ lp_hash; _ } =
      get_lcc ~api ~soru_address in
    match%lwt
      EzReq_lwt.get2 (EzAPI.BASE api)
        Tezos.RPC.Services.SORU.L1.commitment_infos soru_address lp_hash
    with
    | Ok Tezos.RPC.Types.SORU.L1.{ inbox_level; _ } -> Lwt.return inbox_level
    | Error err ->
      Lib_tst.log_ez_req_lwt_err err ;
      Lib_tst.log_path
        (EzAPI.forge1 (EzAPI.BASE api)
           Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
           soru_address []) ;
      Lwt.fail_with
      @@ Log.log_sf
           "%s API: %s, cannot get inbox_level for rollup %s and commitment %s"
           __FUNCTION__ api soru_address lp_hash

  let get_one_staker ~api ~soru_address =
    match%lwt
      EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers
        soru_address
    with
    | Ok stakers -> begin
      match stakers with
      | [] -> raise No_stakers_available
      | staker :: _ -> Lwt.return staker
    end
    | Error err ->
      Lib_tst.log_ez_req_lwt_err err ;
      Lib_tst.log_path
        (EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers
           soru_address []) ;
      Lwt.fail_with
      @@ Log.log_sf "%s API: %s, cannot get stakers from %s" __FUNCTION__ api
           soru_address
end

(* ======================== SERVICES/ENCODINGS TESTS ======================== *)

let check_true = Alcotest.(check bool) "return true" true

let raise_known_error ~get_path err =
  match err with
  | EzReq_lwt_S.KnownError { error; _ } -> raise error
  | _ ->
    Lib_tst.log_ez_req_lwt_err err ;
    Lib_tst.log_path (get_path ()) ;
    false

let is_ok ~get_path result_value =
  match result_value with
  | Ok _ -> true
  | Error err -> raise_known_error ~get_path err

let check_if_one_true l =
  let%lwt exists = Lwt_list.exists_p Fun.id l in
  Lwt.return @@ check_true exists

let check_block ~api ~bid =
  let get_path () =
    EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.block bid [] in
  let%lwt block = EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.block bid in
  Lwt.return @@ is_ok ~get_path block

let check_block ~api_s ~bid =
  let checks = List.map (fun api -> check_block ~api ~bid) api_s in
  check_if_one_true checks

let check_delegates ~api ~bid =
  let active = Tezos.RPC.Services.Params.make_active_param ~active:true () in
  let params = Tezos.RPC.Services.Params.get_params [active] in
  let get_path () =
    EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.delegates bid params in
  let%lwt delegates =
    EzReq_lwt.get1 ~params (EzAPI.BASE api) Tezos.RPC.Services.delegates bid
  in
  Lwt.return @@ is_ok ~get_path delegates

let check_delegates ~api_s ~bid =
  let checks = List.map (fun api -> check_delegates ~api ~bid) api_s in
  check_if_one_true checks

let check_participation ~api ~bid ~delegate =
  let get_path () =
    EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.participation bid delegate
      [] in
  let%lwt participation =
    EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.participation bid
      delegate in
  Lwt.return @@ is_ok ~get_path participation

let check_participation_with_random_delegate ~api ~bid =
  try%lwt
    let%lwt delegates = get_delegates ~__FUNCTION__ ~api ~bid in
    check_participation ~api ~bid ~delegate:(List.hd delegates)
  with _ ->
    Log.log_f "%s : could not get delegates %s !\n" __FUNCTION__ bid ;
    Lwt.return_false

let check_participation_with_random_delegate ~api_s ~bid =
  let checks =
    List.map
      (fun api -> check_participation_with_random_delegate ~api ~bid)
      api_s in
  check_if_one_true checks

let check_delegate_info ~api ~bid ~delegate =
  let get_path () =
    EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.delegate_info bid delegate
      [] in
  let%lwt delegate_info =
    EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.delegate_info bid
      delegate in
  Lwt.return @@ is_ok ~get_path delegate_info

let check_delegate_info_with_random_delegate ~api ~bid =
  try%lwt
    let%lwt delegates = get_delegates ~__FUNCTION__ ~api ~bid in
    check_delegate_info ~api ~bid ~delegate:(List.hd delegates)
  with _ ->
    Log.log_f "%s : could not get delegates %s !\n" __FUNCTION__ bid ;
    Lwt.return_false

let check_delegate_info_with_random_delegate ~api_s ~bid =
  let checks =
    List.map
      (fun api -> check_delegate_info_with_random_delegate ~api ~bid)
      api_s in
  check_if_one_true checks

let check_validators ~api ~bid ~level =
  let level = Tezos.RPC.Services.Params.make_level_param ~level () in
  let params = Tezos.RPC.Services.Params.get_params [level] in
  let get_path () =
    EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.validators bid params in
  let%lwt validators =
    EzReq_lwt.get1 ~params (EzAPI.BASE api) Tezos.RPC.Services.validators bid
  in
  Lwt.return @@ is_ok ~get_path validators

let check_validators_with_max_level ~api ~bid =
  try%lwt
    let%lwt block = get_block ~__FUNCTION__ ~api ~bid in
    check_validators ~api ~bid ~level:block.bl_header.hd_level
  with _ ->
    Log.log_f "%s : could not get block %s !\n" __FUNCTION__ bid ;
    Lwt.return_false

let check_validators_with_max_level ~api_s ~bid =
  let checks =
    List.map (fun api -> check_validators_with_max_level ~api ~bid) api_s in
  check_if_one_true checks

let check_monitor_operations ~api =
  let get_path () =
    EzAPI.forge0 (EzAPI.BASE api) Tezos.RPC.Services.monitor_operations [] in
  let%lwt monitor_operations =
    EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.monitor_operations in
  Lwt.return @@ is_ok ~get_path monitor_operations

let check_monitor_operations ~api_s =
  let checks = List.map (fun api -> check_monitor_operations ~api) api_s in
  check_if_one_true checks

let check_network_constants ~api ~bid =
  let get_path () =
    EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.network_constants bid []
  in
  let%lwt network_constants =
    EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.network_constants bid
  in
  Lwt.return @@ is_ok ~get_path network_constants

let check_network_constants ~api_s ~bid =
  let checks = List.map (fun api -> check_network_constants ~api ~bid) api_s in
  check_if_one_true checks

let check_node_version ~api =
  let get_path () =
    EzAPI.forge0 (EzAPI.BASE api) Tezos.RPC.Services.node_version [] in
  let%lwt node_version =
    EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.node_version in
  Lwt.return @@ is_ok ~get_path node_version

let check_node_version ~api_s =
  let checks = List.map (fun api -> check_node_version ~api) api_s in
  check_if_one_true checks

module Soru = struct
  let check_soru_addresses ~api =
    let get_path () =
      EzAPI.forge0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.soru_addresses []
    in
    let%lwt soru_addresses =
      EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.soru_addresses
    in
    Lwt.return @@ is_ok ~get_path soru_addresses

  let check_soru_addresses ~api_s =
    let checks = List.map (fun api -> check_soru_addresses ~api) api_s in
    check_if_one_true checks

  let check_inbox ~api =
    let get_path () =
      EzAPI.forge0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.inbox [] in
    let%lwt inbox =
      EzReq_lwt.get0 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.inbox in
    Lwt.return @@ is_ok ~get_path inbox

  let check_inbox ~api_s =
    let checks = List.map (fun api -> check_inbox ~api) api_s in
    check_if_one_true checks

  let check_genesis_info ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let get_path () =
        EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.genesis_info
          soru_address [] in
      let%lwt genesis_info =
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.genesis_info
          soru_address in
      Lwt.return @@ is_ok ~get_path genesis_info
    with SORU_HELPER.Empty_address_book -> Lwt.return_true

  let check_genesis_info ~api_s =
    let checks = List.map (fun api -> check_genesis_info ~api) api_s in
    check_if_one_true checks

  let check_last_cemented_commitment_hash_with_level ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let get_path () =
        EzAPI.forge1 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
          soru_address [] in
      let%lwt last_cemented_commitment_hash_with_level =
        EzReq_lwt.get1 (EzAPI.BASE api)
          Tezos.RPC.Services.SORU.L1.last_cemented_commitment_hash_with_level
          soru_address in
      Lwt.return @@ is_ok ~get_path last_cemented_commitment_hash_with_level
    with SORU_HELPER.Empty_address_book -> Lwt.return_true

  let check_last_cemented_commitment_hash_with_level ~api_s =
    let checks =
      List.map
        (fun api -> check_last_cemented_commitment_hash_with_level ~api)
        api_s in
    check_if_one_true checks

  let check_commitments ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let%lwt inbox_level = SORU_HELPER.get_inbox_level ~api ~soru_address in
      let get_path () =
        EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.commitments
          soru_address inbox_level [] in
      let%lwt commitments =
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.commitments
          soru_address inbox_level in
      Lwt.return @@ is_ok ~get_path commitments
    with SORU_HELPER.Empty_address_book -> Lwt.return_true

  let check_commitments ~api_s =
    let checks = List.map (fun api -> check_commitments ~api) api_s in
    check_if_one_true checks

  let check_stakers_ids ~api =
    try%lwt
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let%lwt Tezos.RPC.Types.SORU.L1.{ lp_hash; _ } =
        SORU_HELPER.get_lcc ~api ~soru_address in
      let get_path () =
        EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers_ids
          soru_address lp_hash [] in
      let%lwt stakers_ids =
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers_ids
          soru_address lp_hash in
      Lwt.return @@ is_ok ~get_path stakers_ids
    with Tezos.RPC.Error.Storage_error | SORU_HELPER.Empty_address_book ->
      Lwt.return_true

  let check_stakers_ids ~api_s =
    let checks = List.map (fun api -> check_stakers_ids ~api) api_s in
    check_if_one_true checks

  let check_staker_id ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let%lwt staker = SORU_HELPER.get_one_staker ~api ~soru_address in
      let get_path () =
        EzAPI.forge2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.staker_id
          soru_address staker [] in
      let%lwt staker_id =
        EzReq_lwt.get2 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.staker_id
          soru_address staker in
      Lwt.return @@ is_ok ~get_path staker_id
    with SORU_HELPER.Empty_address_book | SORU_HELPER.No_stakers_available ->
      Lwt.return_true

  let check_staker_id ~api_s =
    let checks = List.map (fun api -> check_staker_id ~api) api_s in
    check_if_one_true checks

  let check_stakers ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let get_path () =
        EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers
          soru_address [] in
      let%lwt stakers =
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.stakers
          soru_address in
      Lwt.return @@ is_ok ~get_path stakers
    with SORU_HELPER.Empty_address_book -> Lwt.return_true

  let check_stakers ~api_s =
    let checks = List.map (fun api -> check_stakers ~api) api_s in
    check_if_one_true checks

  let check_kind ~api =
    try
      let%lwt soru_address = SORU_HELPER.get_one_soru_address ~api in
      let get_path () =
        EzAPI.forge1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.kind
          soru_address [] in
      let%lwt kind =
        EzReq_lwt.get1 (EzAPI.BASE api) Tezos.RPC.Services.SORU.L1.kind
          soru_address in
      Lwt.return @@ is_ok ~get_path kind
    with SORU_HELPER.Empty_address_book -> Lwt.return_true

  let check_kind ~api_s =
    let checks = List.map (fun api -> check_kind ~api) api_s in
    check_if_one_true checks
end

(* =============================== TEST CASES =============================== *)

module Naming = struct
  let replace_space =
    String.map @@ function
    | ' ' -> '_'
    | c -> c

  let make_test_category_name =
    List.map @@ fun (category, value) -> category ^ ":" ^ value

  let make_test_name ~name elts =
    String.concat " | "
    @@ List.map replace_space (name :: make_test_category_name elts)
end

let make_test_name ~network ?bid ?api_name () =
  let network = ("Network", network) in
  let bid = Option.map (fun bid -> ("Bid", bid)) bid in
  let api_name = Option.map (fun api_name -> ("Api", api_name)) api_name in
  Naming.make_test_name ~name:"services/encodings"
    (network :: List.filter_map Fun.id [bid; api_name])

let make_api_bid_test_cases ~network ~api_name ~api_addresses ~bid =
  let api_s = api_addresses in
  let block _ () = check_block ~api_s ~bid in
  let validators _ () = check_validators_with_max_level ~api_s ~bid in
  let network_constants _ () = check_network_constants ~api_s ~bid in
  let delegates _ () = check_delegates ~api_s ~bid in
  let participation _ () =
    check_participation_with_random_delegate ~api_s ~bid in
  let delegate_info _ () =
    check_delegate_info_with_random_delegate ~api_s ~bid in
  let test_cases =
    let open Alcotest_lwt in
    [
      test_case "block" `Quick block;
      test_case "validators" `Quick validators;
      test_case "network_constants" `Quick network_constants;
      test_case "delegates" `Quick delegates;
      test_case "participation" `Quick participation;
      test_case "delegate_info" `Quick delegate_info;
    ] in
  [(make_test_name ~network ~bid ~api_name (), test_cases)]

let make_soru_L1_test_cases ~network ~api_addresses =
  let api_s = api_addresses in
  let soru_addresses _ () = Soru.check_soru_addresses ~api_s in
  let inbox _ () = Soru.check_inbox ~api_s in
  let genesis_info _ () = Soru.check_genesis_info ~api_s in
  let last_cemented_commitment_hash_with_level _ () =
    Soru.check_last_cemented_commitment_hash_with_level ~api_s in
  let commitments _ () = Soru.check_commitments ~api_s in
  let stakers_ids _ () = Soru.check_stakers_ids ~api_s in
  let staker_id _ () = Soru.check_staker_id ~api_s in
  let stakers _ () = Soru.check_stakers ~api_s in
  let kind _ () = Soru.check_kind ~api_s in
  let test_cases =
    let open Alcotest_lwt in
    [
      test_case "soru addresses" `Quick soru_addresses;
      test_case "soru inbox" `Quick inbox;
      test_case "soru genesis_info" `Quick genesis_info;
      test_case "soru last cemented commitment hash with level" `Quick
        last_cemented_commitment_hash_with_level;
      test_case "soru commitments" `Quick commitments;
      test_case "soru stakers ids" `Quick stakers_ids;
      test_case "soru staker id" `Quick staker_id;
      test_case "soru stakers" `Quick stakers;
      test_case "soru kind" `Quick kind;
    ] in
  [(make_test_name ~network (), test_cases)]

let make_api_test_cases ~network ~api_name ~api_addresses =
  let api_s = api_addresses in
  let monitor_operations _ () = check_monitor_operations ~api_s in
  let node_version _ () = check_node_version ~api_s in
  let test_cases =
    let open Alcotest_lwt in
    [
      test_case "monitor_operations" `Slow monitor_operations;
      test_case "node_version" `Quick node_version;
    ] in
  [(make_test_name ~network ~api_name (), test_cases)]

module Test_filter = struct
  type 'a assoc_bid =
    | Head of 'a
    | Genesis of 'a
    | Transition_bids of 'a

  type all_adresses =
    | All_at_once
    | One_by_one

  type 'a assoc_network =
    | Mainnet of 'a
    | Ghostnet of 'a
    | Weeklynet of 'a
    | Genericnet_one of 'a
    | Genericnet_two of 'a
    | Genericnet_three of 'a
    | Genericnet_four of 'a

  type info = (unit assoc_bid list * all_adresses) assoc_network list

  let all_bids x = [Head x; Genesis x; Transition_bids x]

  let all_network x =
    [
      Mainnet x;
      Ghostnet x;
      Weeklynet x;
      Genericnet_one x;
      Genericnet_two x;
      Genericnet_three x;
      Genericnet_four x;
    ]
end

let list_bind_list l f = List.concat_map f l

let opt_bind_list opt f =
  match opt with
  | None -> []
  | Some x -> f x

let bool_bind_list b f =
  if b then
    []
  else
    f ()

let api_addresses_bind_list ~all_adresses ~api:Json_types.{ addresses; _ } f =
  let open Test_filter in
  match all_adresses with
  | All_at_once -> f ~api_name:"All" ~api_addresses:addresses
  | One_by_one ->
    list_bind_list addresses @@ fun address ->
    f ~api_name:address ~api_addresses:[address]

let bids_bind_list ~test_params bids f =
  let open Test_filter in
  list_bind_list bids @@ function
  | Head arg -> f ~bid:"head" arg
  | Genesis arg -> f ~bid:"0" arg
  | Transition_bids arg ->
    opt_bind_list test_params.Test_json_types.transition_bids @@ fun bids ->
    list_bind_list bids @@ fun bid -> f ~bid arg

let networks_bind_list ~parameters ~test_parameters info f =
  let Json_types.
        {
          mainnet;
          ghostnet;
          weeklynet;
          genericnet_one;
          genericnet_two;
          genericnet_three;
          genericnet_four;
        } =
    parameters in
  let Json_types.
        {
          mainnet = mainnet_params;
          ghostnet = ghostnet_params;
          weeklynet = weeklynet_params;
          genericnet_one = genericnet_one_params;
          genericnet_two = genericnet_two_params;
          genericnet_three = genericnet_three_params;
          genericnet_four = genericnet_four_params;
        } =
    test_parameters in
  let f_with_network_opt ~network ~api ~test_params arg =
    opt_bind_list api @@ fun api ->
    opt_bind_list test_params @@ fun test_params ->
    f ~network ~api ~test_params arg in
  let open Test_filter in
  list_bind_list info @@ function
  | Mainnet arg ->
    f ~network:"Mainnet" ~api:mainnet ~test_params:mainnet_params arg
  | Ghostnet arg ->
    f_with_network_opt ~network:"Ghostnet" ~api:ghostnet
      ~test_params:ghostnet_params arg
  | Weeklynet arg ->
    f_with_network_opt ~network:"Weeklynet" ~api:weeklynet
      ~test_params:weeklynet_params arg
  | Genericnet_one arg ->
    f_with_network_opt ~network:"Genericnet_one" ~api:genericnet_one
      ~test_params:genericnet_one_params arg
  | Genericnet_two arg ->
    f_with_network_opt ~network:"Genericnet_two" ~api:genericnet_two
      ~test_params:genericnet_two_params arg
  | Genericnet_three arg ->
    f_with_network_opt ~network:"Genericnet_three" ~api:genericnet_three
      ~test_params:genericnet_three_params arg
  | Genericnet_four arg ->
    f_with_network_opt ~network:"Genericnet_four" ~api:genericnet_four
      ~test_params:genericnet_four_params arg

let tests
    ?(which_test : Test_filter.info =
      Test_filter.[Mainnet ([Head ()], All_at_once)]) () =
  networks_bind_list ~parameters:Parameters.parameters
    ~test_parameters:Parameters.test_parameters which_test
  @@ fun ~network ~api ~test_params (bids, all_adresses) ->
  let bid_test_cases =
    bids_bind_list ~test_params bids @@ fun ~bid () ->
    api_addresses_bind_list ~all_adresses ~api
    @@ fun ~api_name ~api_addresses ->
    make_api_bid_test_cases ~network ~api_name ~api_addresses ~bid in
  let api_test_cases =
    api_addresses_bind_list ~all_adresses ~api
    @@ fun ~api_name ~api_addresses ->
    let test_cases = make_api_test_cases ~network ~api_name ~api_addresses in
    let soru_test_cases = make_soru_L1_test_cases ~network ~api_addresses in
    test_cases @ soru_test_cases in
  bid_test_cases @ api_test_cases
