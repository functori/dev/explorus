(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type data = {
  network_constants : network_constants; [@mutable]
  path_to_network_constants : string;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-network_constants"

  let element =
    Vue_component.CRender
      (Render.network_constants_render, Render.network_constants_static_renders)

  let props = None
end)

let update app ~protocol_transition_level heads =
  if
    List.exists
      (fun Tezos_process.TS.{ block; _ } ->
        let level = block.bl_header.hd_level in
        level = Konstant.genesis_level + 1 || level = protocol_transition_level)
      heads
  then begin
    app##.network_constants_ :=
      network_constants_to_jsoo
      @@ Central_state.Network.get_network_constants ()
  end

let init ~protocol_transition_level ~path_to_network_constants () =
  let data _ =
    object%js
      val mutable network_constants_ =
        network_constants_to_jsoo
        @@ Central_state.Network.get_network_constants ()

      val path_to_network_constants_ = string path_to_network_constants
    end in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            Listener.connect ~use_storage:true Tezos_process.heads_listener
              (update app ~protocol_transition_level)
            |> ignore );
      ]
    ()
