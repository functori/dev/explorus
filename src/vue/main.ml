(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2024, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type enabled_networks = string list [@@deriving jsoo]

type 'a components = 'a Json_types.components = {
  cp_baking_rights : 'a;
  cp_round_clock : 'a;
  cp_latest_blocks : 'a;
  cp_consensus_ops : 'a;
  cp_bakers_activity : 'a;
  cp_explorer : 'a;
  cp_soru : 'a;
  cp_dal : 'a;
  cp_info_bar : 'a;
  cp_consensus_progression : 'a;
  cp_network_constants : 'a;
  cp_share_url : 'a;
  cp_pyrometer : 'a;
  cp_protocol_injection : 'a;
  cp_logs : 'a;
  cp_settings : 'a;
}
[@@deriving jsoo]

type components_states = bool components [@@deriving jsoo]

type component_keys = {
  key_baking_rights : int; [@mutable]
  key_latest_blocks : int; [@mutable]
  key_consensus_ops : int; [@mutable]
  key_bakers_activity : int; [@mutable]
  key_explorer : int; [@mutable]
  key_soru : int; [@mutable]
  key_dal : int; [@mutable]
  key_logs : int; [@mutable]
  key_protocol_injection : int; [@mutable]
  key_settings : int; [@mutable]
}
[@@deriving jsoo]

type data = {
  all_keys : component_keys;
  selected : string; [@mutable]
  enabled_networks : enabled_networks;
  components_states : components_states;
}
[@@deriving jsoo]

module V = Vue_js.Make (struct
  type data = data_jsoo

  type all = data

  let id = "app"
end)

let get_pathname () = Custom_router.get_path ()

let unknown_pathname _app =
  let path = Custom_router.get_clean_path () in
  not (List.mem path Paths.routes || path = "")

let set_network_constants () =
  let%lwt block = Rpc.get_block () in
  if block.bl_header.hd_level = Konstant.genesis_level then
    Lwt.return_unit
  else
    let%lwt network_constants = Rpc.get_network_constants () in
    Lwt.return @@ Central_state.Network.set_network_constants network_constants

let set_and_monitor_protocol_transitions_blocks () =
  let%lwt protocol_transitions = Central_state.DJson.protocol_transitions in
  Tezos_process.Protocol_transition.set_transitions protocol_transitions ;
  Lwt.return_unit

let set_network_from_query () =
  let open Central_state in
  let reload ~set_net destination_state =
    let state = Central_state.Network.get_network_state () in
    let%lwt () = set_net () in
    let%lwt () = Cache.empty_all () in
    if state != destination_state then
      Js_of_ocaml.Dom_html.window##.location##reload ;
    Lwt.return_unit in
  match Custom_router.(get_query () |> extract_network_query) with
  | None -> Lwt.return_unit
  | Some query -> (
    match Network.string_to_network query with
    | Network.Customnet as state ->
      if%lwt Rpc.get_address_validity_test query then (
        Central_state.API.set_custom_address_persistent_on_success query ;
        reload ~set_net:API.set_to_customnet state
      ) else
        Lwt.return_unit
    | Network.Genericnet_four as state ->
      reload ~set_net:API.set_to_genericnet_four state
    | Network.Genericnet_three as state ->
      reload ~set_net:API.set_to_genericnet_three state
    | Network.Genericnet_two as state ->
      reload ~set_net:API.set_to_genericnet_two state
    | Network.Genericnet_one as state ->
      reload ~set_net:API.set_to_genericnet_one state
    | Network.Weeklynet as state -> reload ~set_net:API.set_to_weeklynet state
    | Network.Ghostnet as state -> reload ~set_net:API.set_to_ghostnet state
    | Network.Mainnet as state -> reload ~set_net:API.set_to_mainnet state)

let set_net app =
  let open Central_state in
  let net = Js_of_ocaml.Js.to_string app##.selected in
  match Network.string_to_network net with
  | Network.Mainnet -> API.set_to_mainnet ()
  | Network.Ghostnet -> API.set_to_ghostnet ()
  | Network.Weeklynet -> API.set_to_weeklynet ()
  | Network.Genericnet_one -> API.set_to_genericnet_one ()
  | Network.Genericnet_two -> API.set_to_genericnet_two ()
  | Network.Genericnet_three -> API.set_to_genericnet_three ()
  | Network.Genericnet_four -> API.set_to_genericnet_four ()
  | Network.Customnet -> API.set_to_customnet ()

let get_enabled_networks_as_strings () =
  let%lwt enabled_networks = Central_state.Network.enabled_networks in
  Lwt.return
  @@ List.map
       (fun network -> Central_state.Network.network_to_string network)
       enabled_networks

let refresh_components components app =
  List.iter
    (function
      (* LC := Logical_Component *)
      | Page.LC.BR ->
        app##.all_keys_##.baking_rights_ := app##.all_keys_##.baking_rights_ + 1
      | Page.LC.LB ->
        app##.all_keys_##.latest_blocks_ := app##.all_keys_##.latest_blocks_ + 1
      | Page.LC.CO ->
        app##.all_keys_##.consensus_ops_ := app##.all_keys_##.consensus_ops_ + 1
      | Page.LC.EA ->
        app##.all_keys_##.bakers_activity_
        := app##.all_keys_##.bakers_activity_ + 1
      | Page.LC.OE ->
        app##.all_keys_##.explorer := app##.all_keys_##.explorer + 1
      | Page.LC.SC -> app##.all_keys_##.soru := app##.all_keys_##.soru + 1
      | Page.LC.DL -> app##.all_keys_##.dal := app##.all_keys_##.dal + 1
      | Page.LC.LG -> app##.all_keys_##.logs := app##.all_keys_##.logs + 1
      | Page.LC.ST ->
        app##.all_keys_##.settings := app##.all_keys_##.settings + 1
      | Page.LC.IP ->
        app##.all_keys_##.protocol_injection_
        := app##.all_keys_##.protocol_injection_ + 1
      | Page.LC.RC
      | Page.LC.SL
      | Page.LC.PR
      | Page.LC.IB
      | Page.LC.NC
      | Page.LC.CP
        (* @warning : do not use a wildcard `_` here, we need the pattern
           matching to be exhaustive in order for the compiler to trigger a
           warning here whenever we add a new logical component *) -> ())
    components

let add_all_methods () =
  V.add_method0 "get_pathname" (fun _app -> get_pathname ()) ;
  V.add_method0 "unknown_pathname" unknown_pathname ;
  V.add_method0 "open_functori" Custom_router.open_functori ;
  V.add_method0 "set_net" set_net ;
  Custom_router.load_changing_path_methods refresh_components V.add_method0
  @@ Paths.routes

let stress_potential_persistent_address () =
  match Central_state.Persistency.Address.get () with
  | "" -> Lwt.return_unit
  | address ->
    if%lwt Rpc.get_address_validity_test address then
      Lwt.return_unit
    else begin
      Central_state.API.State.abort ~force:true () ;
      Lwt.return_unit
    end

let get_data () =
  let%lwt components_states = Central_state.DJson.components_states in
  let%lwt enabled_networks = get_enabled_networks_as_strings () in
  let get_selected_network () =
    let selected = Central_state.Persistency.Network.get () in
    if selected = "" then
      Central_state.Network.(network_to_string Mainnet)
    else
      selected in
  Lwt.return
  @@ object%js
       val all_keys_ =
         component_keys_to_jsoo
           {
             key_baking_rights = 0;
             key_latest_blocks = 0;
             key_consensus_ops = 0;
             key_bakers_activity = 0;
             key_explorer = 0;
             key_soru = 0;
             key_dal = 0;
             key_logs = 0;
             key_protocol_injection = 0;
             key_settings = 0;
           }

       val mutable selected = Js_of_ocaml.Js.string @@ get_selected_network ()

       val enabled_networks_ = enabled_networks_to_jsoo enabled_networks

       val components_states_ = components_states_to_jsoo components_states
     end

let component_initialization
    {
      cp_baking_rights;
      cp_round_clock;
      cp_latest_blocks;
      cp_consensus_ops;
      cp_bakers_activity;
      cp_explorer;
      cp_soru;
      cp_dal;
      cp_logs;
      cp_settings;
      cp_share_url;
      cp_pyrometer;
      cp_info_bar;
      cp_protocol_injection;
      cp_network_constants;
      cp_consensus_progression
      (* @warning : do not use a wildcard `_` here, we need the record to be
         exhaustive in order for the compiler to trigger a warning or an error
         here whenever we add a new component to deactivate *);
    } = function
  | Page.LC.BR -> cp_baking_rights
  | Page.LC.RC -> cp_round_clock
  | Page.LC.LB -> cp_latest_blocks
  | Page.LC.CO -> cp_consensus_ops
  | Page.LC.EA -> cp_bakers_activity
  | Page.LC.OE -> cp_explorer
  | Page.LC.SC -> cp_soru
  | Page.LC.DL -> cp_dal
  | Page.LC.LG -> cp_logs
  | Page.LC.ST -> cp_settings
  | Page.LC.SL -> cp_share_url
  | Page.LC.PR -> cp_pyrometer
  | Page.LC.IB -> cp_info_bar
  | Page.LC.IP -> cp_protocol_injection
  | Page.LC.NC -> cp_network_constants
  | Page.LC.CP -> cp_consensus_progression

let open_streams () =
  let _hd_stream = Rpc.HDStream.open_stream () in
  let _applied_mo_stream = Rpc.MOStream.Applied.open_stream () in
  let _outdated_mo_stream = Rpc.MOStream.Outdated.open_stream () in
  ()

let initialize_process () =
  let%lwt () = Tezos_process.Clock.initialize () in
  let%lwt () = Tezos_process.Dal.initialize () in
  open_streams () ;
  Lwt.return_unit

let initialize_components () =
  let%lwt protocol_transition_level =
    Tezos_process.Protocol_transition.get_transition () in
  let () = Central_state.Protocol.set_transition protocol_transition_level in
  let%lwt path_to_network_constants = Rpc.get_network_constants_path () in
  let%lwt components_states = Central_state.DJson.components_states in
  let () = Tezos_process.Clock.run ~protocol_transition_level () in
  let _baking_rights =
    if component_initialization components_states Page.LC.BR then
      Baking_rights.init ~protocol_transition_level () |> ignore in
  let _round_clock =
    if component_initialization components_states Page.LC.RC then
      Round_clock.init () |> ignore in
  let _latest_blocks =
    if component_initialization components_states Page.LC.LB then
      Latest_blocks.init ~protocol_transition_level () |> ignore in
  let _bakers_activity =
    if component_initialization components_states Page.LC.EA then
      Bakers_activity.init () |> ignore in
  let _consensus_ops =
    if component_initialization components_states Page.LC.CO then
      Consensus_ops.init ~protocol_transition_level () |> ignore in
  let _explorer =
    if component_initialization components_states Page.LC.OE then
      Explorer.init () |> ignore in
  let _soru =
    if component_initialization components_states Page.LC.SC then
      Soru.init () |> ignore in
  let _soru_commitment =
    if
      List.exists
        (component_initialization components_states)
        Page.LSC.(constraints SCC)
    then
      Soru_commitment.init () |> ignore in
  let _dal =
    if component_initialization components_states Page.LC.DL then
      Dal_.init () |> ignore in
  let _commitment_tree =
    if
      List.exists
        (component_initialization components_states)
        Page.LSC.(constraints CT)
    then
      Commitment_tree.init () |> ignore in
  let _logs =
    if component_initialization components_states Page.LC.LG then
      Logs.init () |> ignore in
  let _network_constants =
    if component_initialization components_states Page.LC.NC then
      Network_constants.init ~protocol_transition_level
        ~path_to_network_constants ()
      |> ignore in
  let _consensus_progression =
    if component_initialization components_states Page.LC.CP then
      Consensus_progression.init ~protocol_transition_level () |> ignore in
  let%lwt _hash_displayer =
    if
      List.exists
        (component_initialization components_states)
        Page.LSC.(constraints HD)
    then
      let%lwt _hash_displayer = Hash_displayer.init () in
      Lwt.return_unit
    else
      Lwt.return_unit in
  let _info_bar =
    if component_initialization components_states Page.LC.IB then
      Info_bar.init () |> ignore in
  let _share_url =
    if component_initialization components_states Page.LC.SL then
      Share_url.init () |> ignore in
  let _pyrometer =
    if component_initialization components_states Page.LC.PR then
      Pyrometer.init () |> ignore in
  let _protocol_injection =
    if component_initialization components_states Page.LC.IP then
      Protocol_injection.init ~protocol_transition_level () |> ignore in
  let _settings =
    if component_initialization components_states Page.LC.ST then
      Settings.init () |> ignore in
  Lwt.return_unit

let initialize_app () =
  let%lwt data = get_data () in
  let _set_title_by_path = Custom_router.set_title @@ get_pathname () in
  let _app =
    V.init ~data ~export:true ~render:Render.app_render
      ~static_renders:Render.app_static_renders () in
  Lwt.return_unit

let launch_app () =
  Log.log "→ explorus ► start ←" ;
  (* /!\ Order matters here, be careful when you add a step in the launching app
     process /!\ *)
  let%lwt () (* try to -> *) = set_network_from_query () in
  let%lwt () = stress_potential_persistent_address () in
  let%lwt () = Tezos_process.Protocol_transition.set_protocol () in
  let%lwt () = set_network_constants () in
  let%lwt () = set_and_monitor_protocol_transitions_blocks () in
  let () = add_all_methods () in
  let%lwt () = initialize_process () in
  let%lwt () = initialize_components () in
  initialize_app ()

let () =
  Lwt.async_exn ~__FUNCTION__ @@ fun () ->
  Lwt.catch launch_app @@ function
  | Central_state.Network.Reload ->
    (* Just waiting for the reload *)
    Log.log "Reloading app..." ;
    Lwt.return_unit
  | exn ->
    let open Central_state in
    Log.log_exn ~__FUNCTION__ ~exn () ;
    Log.log "Restart on Mainnet" ;
    let%lwt () = Network.set_network_state ~force_reload:false Network.Mainnet in
    launch_app ()
