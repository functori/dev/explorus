(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Json_types

let known_bakers_file = Lib_tst.wrap_path_json ~dir:"static" Paths.known_bakers

let test_known_bakers_file =
  Lib_tst.wrap_path_json ~dir:"tests" Paths.known_bakers

let known_bakers_path_ok () =
  Lib_tst.destruct_json_path known_bakers_file known_bakers_enc

let known_bakers_encoding () =
  Lib_tst.destruct_json_encoding known_bakers_file known_bakers_enc

let functori_existence () =
  let known_bakers =
    Lib_tst.destruct_json test_known_bakers_file known_bakers_enc in
  match
    List.find_opt
      (fun kb ->
        kb.name = "Functori" && kb.pkh = "tz1funU3PjPsuXvmtXMgnAckY1s4pNT6V7WJ")
      known_bakers
  with
  | None -> false
  | Some _ -> true

let yellow_existence () =
  let known_bakers =
    Lib_tst.destruct_json test_known_bakers_file known_bakers_enc in
  match
    List.find_opt
      (fun kb ->
        kb.name = "Yellow" && kb.pkh = "tz2F1TZXc456gsfoFQLYW7E74SQVjzZtEqXJ")
      known_bakers
  with
  | None -> false
  | Some _ -> true

let test_path _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ known_bakers_path_ok ()

let test_encoding _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ known_bakers_encoding ()

let test_functori _ () =
  Lwt.return
  @@ Alcotest.(check bool) "return true" true
  @@ functori_existence ()

let test_yellow _ () =
  Lwt.return @@ Alcotest.(check bool) "return true" true @@ yellow_existence ()
