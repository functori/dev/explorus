Explorus changelog
==================

Version DEV
===========

Added
-----

-  Activate Nextnet.
   `MR!451 <https://gitlab.com/functori/dev/explorus/-/merge_requests/451>`__

Fixed
-----

Changed
-------

Deprecated
----------

-  Deactivate Parisnet.
   `MR!451 <https://gitlab.com/functori/dev/explorus/-/merge_requests/451>`__

Version 6.0
===========

Added
-----

-  Add more details on DAL operations in consensus operations' page.
   `MR!433 <https://gitlab.com/functori/dev/explorus/-/merge_requests/433>`__

-  QuebecAnet & QuebecBnet activation
   `MR!436 <https://gitlab.com/functori/dev/explorus/-/merge_requests/436>`__

-  Qenanet activation
   `MR!443 <https://gitlab.com/functori/dev/explorus/-/merge_requests/443>`__

-  Quebecnet activation & QuebecAnet & QuebecBnet deactivation
   `MR!444 <https://gitlab.com/functori/dev/explorus/-/merge_requests/444>`__

-  Add a freeze button to consensus operations page.
   `MR!445 <https://gitlab.com/functori/dev/explorus/-/merge_requests/445>`__

-  Remove Qenanet.
   `MR!446 <https://gitlab.com/functori/dev/explorus/-/merge_requests/446>`__

-  Rionet activation
   `MR!453 <https://gitlab.com/functori/dev/explorus/-/merge_requests/453>`__

Fixed
-----

-  Fix DAL attested/published computation in consensus operations. 
   `MR!435 <https://gitlab.com/functori/dev/explorus/-/merge_requests/435>`__

-  RPC: prevent validators spamming when network hasn't started.
   `MR!437 <https://gitlab.com/functori/dev/explorus/-/merge_requests/437>`__

-  Fix DAL attestation lag offset.
   `MR!442 <https://gitlab.com/functori/dev/explorus/-/merge_requests/442>`__

Changed
-------

-  Introducing generic networks 
   `MR!436 <https://gitlab.com/functori/dev/explorus/-/merge_requests/436>`__

-  Update network constants 
   `MR!441 <https://gitlab.com/functori/dev/explorus/-/merge_requests/441>`__

Version 5.0
===========

Added
-----

-  Betanet activation
   `MR!430 <https://gitlab.com/functori/dev/explorus/-/merge_requests/430>`__

Fixed
-----

-  Fix Parisnet's chain name
   `MR!424 <https://gitlab.com/functori/dev/explorus/-/merge_requests/424>`__
-  Remove version 0 from RPC parameters
   `MR!429 <https://gitlab.com/functori/dev/explorus/-/merge_requests/429>`__ 
-  Fix `delegate_info` RPC for Betanet
   `MR!431 <https://gitlab.com/functori/dev/explorus/-/merge_requests/431>`__ 

Changed
-------

-  Move chain name to parameter for consistency
   `MR!427 <https://gitlab.com/functori/dev/explorus/-/merge_requests/427>`__ 

Deprecated
----------

-  Oxfordnet depreciation
   `MR!425 <https://gitlab.com/functori/dev/explorus/-/merge_requests/425>`__

Version 4.0
===========

Added
-----

-  Predalnet activation
   `MR!412 <https://gitlab.com/functori/dev/explorus/-/merge_requests/412>`__
-  Parisnet A & B activation
   `MR!417 <https://gitlab.com/functori/dev/explorus/-/merge_requests/417>`__
-  Parisnet activation
   `MR!420 <https://gitlab.com/functori/dev/explorus/-/merge_requests/420>`__
-  Url: allow to set custom network in url parameters
   `MR!409 <https://gitlab.com/functori/dev/explorus/-/merge_requests/409>`__
-  Encoding: add all operations
   `MR!406 <https://gitlab.com/functori/dev/explorus/-/merge_requests/406>`__
-  DAL: attestations per declared attestors
   `MR!400 <https://gitlab.com/functori/dev/explorus/-/merge_requests/400>`__
-  Add the attestation_with_dal operation
   `MR!399 <https://gitlab.com/functori/dev/explorus/-/merge_requests/399>`__

Fixed
-----

-  Refacto: teztnet {xyz -> com}
   `MR!419 <https://gitlab.com/functori/dev/explorus/-/merge_requests/419>`__
-  Pyrometer: update address
   `MR!418 <https://gitlab.com/functori/dev/explorus/-/merge_requests/418>`__
-  Bakers/Typo: loosing -> losing
   `MR!416 <https://gitlab.com/functori/dev/explorus/-/merge_requests/416>`__
-  Encoding: ignore unused fields in refutation-games
   `MR!411 <https://gitlab.com/functori/dev/explorus/-/merge_requests/411>`__
-  Encoding: contract transaction entrypoint is "default" if none
   `MR!410 <https://gitlab.com/functori/dev/explorus/-/merge_requests/410>`__
-  Encoding: Fix metadata_slot_header encoding
   `MR!405 <https://gitlab.com/functori/dev/explorus/-/merge_requests/405>`__
-  Rpc: fix adaptive-issuance liquidity_baking_subsidy error
   `MR!404 <https://gitlab.com/functori/dev/explorus/-/merge_requests/404>`__
-  Encoding: rename dal publish_slot_header to dal publish_commitment
   `MR!403 <https://gitlab.com/functori/dev/explorus/-/merge_requests/403>`__
-  RPC: take version 0 for rpc
   `MR!402 <https://gitlab.com/functori/dev/explorus/-/merge_requests/402>`__
-  Fix issuance ratio min missing field rpc error
   `MR!398 <https://gitlab.com/functori/dev/explorus/-/merge_requests/398>`__

Deprecated
----------

-  Dailynet depreciation
   `MR!413 <https://gitlab.com/functori/dev/explorus/-/merge_requests/413>`__
-  Parisnet A & B depreciation
   `MR!421 <https://gitlab.com/functori/dev/explorus/-/merge_requests/421>`__
-  Predalnet depreciation
   `MR!422 <https://gitlab.com/functori/dev/explorus/-/merge_requests/422>`__

Version 3.0
===========

Added
-----

-  Oxfordnet activation
   `MR!380 <https://gitlab.com/functori/dev/explorus/-/merge_requests/380>`__
-  New computation regarding adaptive issuance and rewards 
   `MR!385 <https://gitlab.com/functori/dev/explorus/-/merge_requests/385>`__
-  DAL column on latest block details added
   `MR!388 <https://gitlab.com/functori/dev/explorus/-/merge_requests/388>`__
-  DAL minimal page: slot status per level
   `MR!390 <https://gitlab.com/functori/dev/explorus/-/merge_requests/390>`__   

Fixed
-----

Changed
-------

-  Remove all iterations of 'endorsement' in favor of 'attestation' 
   `MR!382 <https://gitlab.com/functori/dev/explorus/-/merge_requests/382>`__

Deprecated
----------

-  Nairobinet depreciation
   `MR!381 <https://gitlab.com/functori/dev/explorus/-/merge_requests/381>`__

Version 2.0
===========

Added
-----

-  Nairobinet activation
   `MR!354 <https://gitlab.com/functori/dev/explorus/-/merge_requests/354>`__

-  Title on pages
   `MR!350 <https://gitlab.com/functori/dev/explorus/-/merge_requests/350>`__
   `MR!351 <https://gitlab.com/functori/dev/explorus/-/merge_requests/351>`__

-  SORU’s refutation game
   `MR!346 <https://gitlab.com/functori/dev/explorus/-/merge_requests/346>`__

-  SORU’s conflicts
   `MR!345 <https://gitlab.com/functori/dev/explorus/-/merge_requests/345>`__

-  SORU’s stakers info
   `MR!343 <https://gitlab.com/functori/dev/explorus/-/merge_requests/343>`__

-  Origination to operation explorer
   `MR!338 <https://gitlab.com/functori/dev/explorus/-/merge_requests/338>`__

-  Link to all network constants
   `MR!336 <https://gitlab.com/functori/dev/explorus/-/merge_requests/336>`__

-  Documentation
   `MR!335 <https://gitlab.com/functori/dev/explorus/-/merge_requests/335>`__
   `MR!334 <https://gitlab.com/functori/dev/explorus/-/merge_requests/334>`__

-  Soru for mumbainet
   `MR!328 <https://gitlab.com/functori/dev/explorus/-/merge_requests/328>`__
   `MR!329 <https://gitlab.com/functori/dev/explorus/-/merge_requests/329>`__

-  SORU: make the smart rollup ID select bar searchable
   `MR!372 <https://gitlab.com/functori/dev/explorus/-/merge_requests/372>`__

-  CHANGELOGS pages for explorus and tezos repository
   `MR!374 <https://gitlab.com/functori/dev/explorus/-/merge_requests/374>`__

Fixed
-----

-  SORU’s commitment hover erasing (Chrome)
   `commit!572ba1 <https://gitlab.com/functori/dev/explorus/-/commit/572ba167a2f510d77d1a57909cad1283b610fa81>`__

-  SORU’s general board odd overflow’s fix (Chrome)
   `commit!d23792 <https://gitlab.com/functori/dev/explorus/-/commit/d23792ccfb730c4ebef679af8f7c0a88e4f6fa3e>`__

-  Unavailable network constants
   `MR!347 <https://gitlab.com/functori/dev/explorus/-/merge_requests/347>`__

-  Better baking rights computation
   `MR!341 <https://gitlab.com/functori/dev/explorus/-/merge_requests/341>`__

-  Clock computation
   `MR!333 <https://gitlab.com/functori/dev/explorus/-/merge_requests/333>`__

-  Stress_potential_persistent_address test
   `MR!327 <https://gitlab.com/functori/dev/explorus/-/merge_requests/327>`__

-  Handle bakers activity on genesis block
   `MR!326 <https://gitlab.com/functori/dev/explorus/-/merge_requests/326>`__

-  Fix network constants reward encoding
   `MR!371 <https://gitlab.com/functori/dev/explorus/-/merge_requests/371>`__

-  Fix participation encoding
   `MR!376 <https://gitlab.com/functori/dev/explorus/-/merge_requests/376>`__

Changed
-------

-  Rename purely interface related iteration of endorsement to
   attestation
   `MR!355 <https://gitlab.com/functori/dev/explorus/-/merge_requests/355>`__
   `MR!373 <https://gitlab.com/functori/dev/explorus/-/merge_requests/373>`__

-  Improve the overall display of the SORU page
   `MR!344 <https://gitlab.com/functori/dev/explorus/-/merge_requests/344>`__

-  Improve jsoo data structure usage
   `MR!342 <https://gitlab.com/functori/dev/explorus/-/merge_requests/342>`__

-  Ask the user to lauch the commitment_tree
   `MR!339 <https://gitlab.com/functori/dev/explorus/-/merge_requests/339>`__

-  Activate features by protocol and not network
   `MR!331 <https://gitlab.com/functori/dev/explorus/-/merge_requests/331>`__

Deprecated
----------

-  Mumbainet depreciation
   `MR!354 <https://gitlab.com/functori/dev/explorus/-/merge_requests/354>`__

-  Limanet depreciation
   `MR!352 <https://gitlab.com/functori/dev/explorus/-/merge_requests/352>`__
