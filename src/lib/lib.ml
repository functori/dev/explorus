(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ==========================================================================
   ============================ INDEPENDANT LIBS ============================
   ========================================================================== *)

module Log = struct
  (** [Log] is a convenient module to print either usefull debug or information
      messages on the browsers console. *)

  let log = print_endline

  let log_f = Printf.printf

  let log_sf = Printf.sprintf

  let log_exn ?msg ~__FUNCTION__ ~exn () =
    let msg =
      match msg with
      | None -> ""
      | Some msg -> "\n" ^ msg in
    log (log_sf "%s: %s%s" __FUNCTION__ (Printexc.to_string exn) msg)
end

module Pretty = struct
  (** [Pretty] interpretations for string/int/bool and preprocessing for
      hash/address. *)

  let none_str = "[none]"

  let none_int = -1

  let bool_to_interjection b =
    if b then
      "yes"
    else
      "no"

  (** [shorten_end split_size str] returns the first [split_size] characters of
      [str]. If [split_size] is greater than the size of [str], it returns [str]
      unchanged. Examples:

      - [shorten_end 3 "abcdef"] = "abc"

      - [shorten_end 5 "abc" ] = "abc" *)
  let shorten_end split_size str =
    try String.sub str 0 split_size with Invalid_argument _ -> str

  (** [shorten_middle size_from_start size_from_end str] returns a string with
      the same [size_from_start] first characters and the same [size_from_end]
      last characters as [str] concatenate next to ".." as long as the result is
      smaller than [str]. Examples:

      - [shorten_middle 2 3 "abcdefghij"] = "ab..hij"

      - [shorten_middle 2 1 "abcd"] = "abcd" *)
  let shorten_middle size_from_start size_from_end str =
    try
      let middle = ".." in
      let len = String.length str in
      if len <= size_from_start + String.length middle + size_from_end then
        str
      else
        let start = String.sub str 0 size_from_start in
        let endding = String.sub str (len - size_from_end) size_from_end in
        start ^ middle ^ endding
    with _ -> str

  let preprocess_local_address address =
    try
      let address_prefix = String.sub address 0 3 in
      if address_prefix = "127" || address_prefix = "loc" then
        "http://" ^ address
      else
        address
    with _ -> address
end

module Hex = struct
  (** [Hex] is a module that adds functions to handle hexadecimal values. *)

  type t = string

  exception NonHexString of string

  (* See: https://discuss.ocaml.org/t/convert-hexadecimal-to-decimal/396 *)
  let to_int hex =
    try int_of_string ("0x" ^ hex) with _ -> raise (NonHexString hex)

  let of_int = Printf.sprintf "%0x"

  let to_int64 hex =
    try Int64.of_string ("0x" ^ hex) with _ -> raise (NonHexString hex)

  let of_int64 = Printf.sprintf "%0Lx"
end

module Compute = struct
  (** [Compute] is a convenient module that makes base computation easier and/or
      more readable. *)

  let round_by_2 n = Float.round (n *. 100.) /. 100.

  (** [ordered compare x y] returns [(x, y)] if [compare x y < 0], [(y, x)]
      otherwise. *)
  let ordered compare x y =
    if compare x y < 0 then
      (x, y)
    else
      (y, x)

  let lexico_compare compares =
    Option.value ~default:0 @@ List.find_opt (( != ) 0) compares
end

module Time = struct
  (** [Time] is a module that sets bases to either manipulate or get time. *)

  let seconds_to_ms s = s *. 1000.

  let universal () =
    (* get system time |>

       assumes UTC (Coordinated Universal Time), also known as GMT |>

       convert it back to Unix.tm |>

       get float value of it *)
    Unix.time () |> Unix.gmtime |> Unix.mktime |> fst
end

module ConcurrentValue : sig
  (** [ConcurrentValue] implements a way to allow safe concurrent access to a
      value.

      When an access request [r] to a [ConcurrentValue.t] [c] is triggered to
      execute the function [f] , all new access requests to [c] are suspended
      until the execution of [f] is completed. Then, the next access request to
      [c] that was triggered will be the next one to get access to [c]. The
      others will wait their turn.

      By default, access requests to a [ConcurrentValue.t][c] give access to [c]
      in the order in which the access requests were triggered.

      This module also offers the possibility of deferring access to a
      [ConcurrentValue.t] to allow priority access (see [unpriority]).

      A request for access to a [ConcurrentValue.t][c], when [c] is deferred
      with respect to another [ConcurrentValue.t][c'], will request access to
      the value of [c] via an access request to [c'].

      For example: Let there be 2 concurrent values [v] and [v'] and 3 processes
      [a], [b] and [c] such that :

      - [v'] = [unpriority v].
      - Process [a] requests access to [v'] after 0s for an execution [ea] of
        10s.
      - Process [b] requests access to [v'] after 5s for an execution [eb] of
        10s.
      - Process [c] requests access to [v] after 8s for an execution [ec] of
        10s.

      At second 0:

      [a] requests access to [v']. As no process has yet requested it, [a]
      prevents the other processes from accessing [v'] and requests access to
      [v]. And since no process has requested it either, it prevents the other
      processes from accessing [v] and executes [ea] with the value of [v].

      At second 5:

      [b] requests access to [v']. Since [a] prevents access to [v'], [b] waits.

      At second 8:

      [c] requests access to [v]. Since [a] prevents access to [v], [c] waits.

      At second 10:

      [ea] terminates and allows access to [v] and [v']. Since only [b] requests
      access to [v'], [b] prevents other processes from accessing [v'] and
      requests access to [v]. Since [b] and [c] request access to [v] and [c]
      requested it before, [c] prevents the other processes from accessing [v]
      and executes [ec] with the value of [v].

      At second 20:

      [ec] terminates and allows access to [v]. Since only [b] requests access
      to [v], [b] prevents other processes from accessing [v] and executes [eb]
      with the value of [v].

      At second 30:

      [ed] terminates and allows access to [v] and [v'].

      At the end, the execution order will have been [ea] then [ec] then [eb].

      Be careful, if [a] requests for access to [v] instead of [v']:

      At second 0:

      [a] only prevents access to [v] and not to [v'].

      At second 5:

      [b] prevents the other processes from accessing [v'] and requests access
      to [v]. And as [a] prevents access to [v], [b] still waits. ...

      At second 10:

      [ea] terminates and allows access to [v] only. Since [b] and [c] request
      access to [v] and [b] requested it before, [b] prevents the other
      processes from accessing [v] and executes [eb] with the value of [v]. ...

      At the end the execution order will have been [ea] then [eb] then [ec].

      NB:

      - Do not use a reference in a [ConcurrentValue.t], the behaviour of the
        program might be odd
      - A [ConcurrentValue.t] can safely be used in another [ConcurrentValue.t] *)

  type 'a t

  val create : 'a -> 'a t

  val apply : 'a t -> ('a -> 'b Lwt.t) -> 'b Lwt.t

  val apply_with_update :
    'a t -> (update:('a -> unit) -> 'a -> 'b Lwt.t) -> 'b Lwt.t

  val update : 'a t -> ('a -> 'a Lwt.t) -> unit Lwt.t

  val simple_update : 'a t -> ('a -> 'a) -> unit Lwt.t

  val update0 : 'a t -> (unit -> 'a Lwt.t) -> unit Lwt.t

  val update1 : 'a t -> 'b -> ('b -> 'a Lwt.t) -> unit Lwt.t

  val unpriority : 'a t -> 'a t

  val get : 'a t -> 'a
end = struct
  type 'a t = {
    locks : Lwt_mutex.t list;
    value : 'a ref;
  }

  (** [create v] returns a new ConcurrentValue.t with the value [v]. *)
  let create v = { locks = [Lwt_mutex.create ()]; value = ref v }

  (** [unpriority c] returns a new ConcurrentValue.t [c'] with the same value
      than [c] but updates on [c] will have priority over updates on [c'] *)
  let unpriority c = { c with locks = Lwt_mutex.create () :: c.locks }

  (** [apply c f] will trigger a request to access [c] in order to execute
      [f v], [v] being the value of [c]. *)
  let apply { value; locks } f =
    let rec aux = function
      | [] -> f !value
      | lock :: locks -> Lwt_mutex.with_lock lock @@ fun () -> aux locks in
    aux locks

  (** [apply_with_update c f] will have the same behavior as [apply] but will
      provide a function to update [c]. *)
  let apply_with_update c f =
    let update new_value = c.value := new_value in
    apply c @@ f ~update

  (** [update c f] will trigger a request to access [c] in order to update it
      with the result of [f v], [v] being the value of [c]. *)
  let update c f =
    apply_with_update c @@ fun ~update value ->
    let%lwt new_value = f value in
    update new_value ;
    Lwt.return_unit

  (** Same as [update] but the update function does not need to return a Lwt.t
      value. *)
  let simple_update c f = update c @@ fun r -> Lwt.return (f r)

  (** Same as [update] but the update function does need the value of the
      [ConcurrentValue.t]. *)
  let update0 c f = update c @@ fun _ -> f ()

  (** Same as [update0] but the update function use an additional argument. *)
  let update1 c a f = update c @@ fun _ -> f a

  (** [get c] returns the value of [c] without preventing access to [c].
      Warning: even just after getting the value of [c] with [get c], the value
      of [c] may have changed. *)
  let get c = !(c.value)
end

module Incomplete_JSON_Parser = struct
  (** [Incomplete_JSON_Parser] is a module that allows you, as its name
      indicate, manipulate incomplete JSON while parsing it. *)

  type decoder = {
    current_stream : string;
    recognizable_json : string list;
  }

  let create_bracket_counter () = ref None

  let incr_bracket_counter i =
    match !i with
    | None -> i := Some 1
    | Some c -> i := Some (c + 1)

  let decr_bracket_counter i =
    match !i with
    | None -> i := Some 0
    | Some c -> i := Some (c - 1)

  let reset_bracket_counter i = i := None

  let handle_char i str = function
    | '{' as c ->
      incr_bracket_counter i ;
      str ^ Char.escaped c
    | '}' as c ->
      decr_bracket_counter i ;
      str ^ Char.escaped c
    | ']' when !i = None -> str
    | '[' when !i = None -> str
    | ',' when !i = None -> str
    | any_other -> str ^ Char.escaped any_other

  let handle_parse_cases str i decoder =
    match !i with
    | None
    (* ignoring :

       - ',' after a '}'

       - '[' at the start of JSON

       - ']' at the end of a valid JSON *) -> decoder
    | Some c when c = 0 (* a complete JSON was seen *) ->
      reset_bracket_counter i ;
      {
        current_stream = "";
        recognizable_json = str :: decoder.recognizable_json;
      }
    | Some _ (* a partial JSON is being parsed *) ->
      { current_stream = str; recognizable_json = decoder.recognizable_json }

  let parse_monitor_operation stream =
    let i = create_bracket_counter () in
    let neutral_decoder = { current_stream = ""; recognizable_json = [] } in
    String.fold_left
      (fun decoder c ->
        let str = handle_char i decoder.current_stream c in
        handle_parse_cases str i decoder)
      neutral_decoder stream
end

module IJ_Parser = Incomplete_JSON_Parser

module CircularList : sig
  (** [CircularList] is a convenient module that behaves like a regular circular
    list while keeping track of the position of the list with a pointer.

    [invariant] :

    - a [CircularList.t] is never empty

    [example] :

    1) pl = { position = 1; list = ["a";"b"] }

    [next pl] = {position = 0; list = ["a";"b"]}

    2) pl = {position = 0; list = ["a"; "b"]}

    [prev pl] = {position = 1; list = ["a";"b"]} *)

  type 'a t

  val init_single : 'a -> 'a t

  val init : 'a list -> 'a t

  val length : 'a t -> int

  val position : 'a t -> int

  val add : 'a t -> 'a -> 'a t

  val get : 'a t -> 'a

  val next : 'a t -> 'a t

  val prev : 'a t -> 'a t
end = struct
  type 'a t = {
    position : int;
    list : 'a list;
  }

  let init_single x = { position = 0; list = [x] }

  let init xl =
    if List.length xl = 0 then
      (* 'failwith' is allowed here because the [invariant] should not be
         breached *)
      failwith @@ __FUNCTION__
      ^ " : you can't initialize a CircularList with an empty list"
    else
      { position = 0; list = xl }

  let length { list; _ } = List.length list

  let position { position; _ } = position

  let add { position; list } x = { position; list = list @ [x] }

  let get pl =
    try List.nth pl.list pl.position
    with _ ->
      (* 'failwith' is allowed here because the [invariant] is breached *)
      failwith @@ __FUNCTION__ ^ " : can't get position "
      ^ Int.to_string pl.position

  let incr_pos { position; list } =
    if position + 1 < List.length list then
      Some { position = position + 1; list }
    else
      None

  let decr_pos { position; list } =
    if position - 1 >= 0 then
      Some { position = position - 1; list }
    else
      None

  let next pl =
    match incr_pos pl with
    | Some pl -> pl
    | None -> { pl with position = 0 }

  let prev pl =
    match decr_pos pl with
    | Some pl -> pl
    | None -> { pl with position = List.length pl.list - 1 }
end

module GenId : sig
  (** [GenId] is a module that implements a unique identifier generator. *)

  type t

  val create : unit -> t

  val gen : t -> int
end = struct
  type t = int ref

  let create () = ref 0

  let gen t =
    let x = !t in
    t := x + 1 ;
    x
end

module SafeTrigger : sig
  (** [SafeTrigger] is a simple module with an easy interface that create a
      [false] mutable bool in order for it to be (really) triggered only one
      time and thus be setted to [true]. It can be indefinitly access in
      reading-mode but can only be triggered once, it is a « one-way boolean ». *)

  type t

  val create : unit -> t

  val access : t -> bool

  val trigger : t -> unit
end = struct
  type t = bool ref

  let create () : t = ref false

  let access st = !st

  let trigger st = if not !st then st := true
end

module PendingFunctions : sig
  type 'a t

  val create : ?functions:('a -> unit) list -> unit -> 'a t

  val add : 'a t -> ('a -> unit) -> unit

  val run : 'a t -> 'a -> unit
end = struct
  type 'a t = ('a -> unit) list ref

  let create ?(functions = []) () = ref functions

  let add p f = p := f :: !p

  let run p x =
    List.iter (fun f -> f x) !p ;
    p := []
end

(* ==========================================================================
   ============================= EXTENDED LIBS ==============================
   ========================================================================== *)

module Lwt = struct
  include Lwt

  (** [async_exn f] has the same behaviour as [Lwt.dont_wait] but it will by
      default log the exception in case there is one. *)
  let async_exn ~__FUNCTION__ f =
    let handler e =
      let e = Printexc.to_string e in
      Log.log_f "%s : %s\n" __FUNCTION__ e in
    Lwt.dont_wait f handler
end

module Int = struct
  include Int

  (** [iter ~start ~stop f] applies function [f] with an integer that goes from
      [~start] to [~stop] *)
  let iter ~start ~stop f =
    let rec aux i =
      if i < stop then begin
        f i ;
        aux @@ succ i
      end in
    aux start

  (** [iter_lwt ~start ~stop f] applies promise [f] with an integer that goes
      from [~start] to [~stop] *)
  let iter_lwt ~start ~stop f =
    let rec aux i =
      if i < stop then
        let%lwt _ = f i in
        aux (succ i)
      else
        Lwt.return_unit in
    aux start

  (** [of_string] has the same behaviour as [int_of_string] *)
  let of_string = int_of_string
end

module Option = struct
  include Option

  (** [perform x_opt ~default ~perform] returns [~default] if [x_opt] is [None],
      otherwise if it's [Some x] it will return [~perform x]. *)
  let perform x_opt ~default ~perform =
    match x_opt with
    | None -> default
    | Some x -> perform x

  (** [perform_delayed x_opt ~default ~perform] returns the evaluation of
      [~default ()] if [x_opt] is [None], otherwise if it's [Some x] it will
      return [~perform x]. *)
  let perform_delayed x_opt ~default ~perform =
    match x_opt with
    | None -> default ()
    | Some x -> perform x

  (** [merge f o1 o2] tries to return [f v1 v2] if [o1] & [o2] are different
      from [None], return [None] otherwise. *)
  let merge f o1 o2 =
    match (o1, o2) with
    | Some v1, Some v2 -> Some (f v1 v2)
    | _ -> None

  (** [union f o1 o2] tries to return:

      - [f v1 v2] if [o1] & [o2] are equal to [Some v1] & [Some v2]
      - return [v] if at least one of [o1] & [o2] is equal to [Some v]
      - return [None] if both of them are [None] *)
  let union f o1 o2 =
    match (o1, o2) with
    | Some v1, Some v2 -> Some (f v1 v2)
    | Some v, None | None, Some v -> Some v
    | None, None -> None

  (** [bind_none None f] returns [f ()] and [bind_none (Some x) f] returns
      [Some x]. *)
  let bind_none o f =
    match o with
    | None -> f ()
    | _ -> o

  (** [value_delayed ~default v] behaves like [Option.value], but the [~default]
      value is passed as a thunk that is only computed if needed. *)
  let value_delayed ~default = function
    | None -> default ()
    | Some v -> v
end

module List = struct
  include List

  (** [take_rev n l] returns up to the [n] first elements from list [l] in
      reverse order, if available. *)
  let take_rev n l =
    let rec aux n acc = function
      | h :: t when n > 0 -> aux (n - 1) (h :: acc) t
      | _ -> acc in
    aux n [] l

  (** [take n l] returns up to the [n] first elements from list [l], if
      available. *)
  let take n l = rev (take_rev n l)

  (** [take_last n l] returns up to the [n] last elements from list [l], if
      available. *)
  let take_last n l = take_rev n (rev l)

  (** [hd_opt l] returns the first element of the given list as an option,
      [None] if the list is empty. *)
  let hd_opt = function
    | [] -> None
    | hd :: _tl -> Some hd

  (** [tl_opt l] returns the given list without its first element as an option,
      [None] if the list is empty. *)
  let tl_opt = function
    | [] -> None
    | _hd :: tl -> Some tl

  (** [is_empty l] checks if [l] is empty *)
  let is_empty l = l = []

  (** [last l] returns the last element of the list, or raises
      [Invalid_argument] if the list is empty. *)
  let rec last = function
    | [] -> invalid_arg @@ __FUNCTION__ ^ ": empty list"
    | [h] -> h
    | _ :: t -> last t

  (** [last_opt l] returns the last element of the list as an option, [None] if
      the list is empty. *)
  let rec last_opt = function
    | [] -> None
    | [h] -> Some h
    | _ :: t -> last_opt t

  (** [fold_left_while p f init l], accumulates elements [x] of list [l] using
      function [f], as long as predicate [p acc x] holds. *)
  let fold_left_while p f init l =
    let rec aux acc = function
      | [] -> acc
      | elt :: elts when p acc elt -> aux (f acc elt) elts
      | _ -> acc in
    aux init l

  (** [fold_right_while p f l init], accumulates elements [x] of list [l] in
      reverse order using function [f], as long as predicate [p acc x] holds.
      This function is tail recursive. *)
  let fold_right_while p f l init =
    let rec aux elts continue =
      match elts with
      | [] -> continue init
      | elt :: elts ->
        aux elts @@ fun acc ->
        if p elt acc then
          continue (f elt acc)
        else
          acc in
    aux l @@ fun acc -> acc
end

module String = struct
  include String

  (** [last_is c s] return [true] if last char of [s] is [c], [false] otherwise *)
  let last_is c s =
    let length = String.length s in
    try String.get s (length - 1) = c with _ -> false

  (** [without_last s] return [s] without the last character *)
  let without_last s =
    let length = String.length s in
    try String.sub s 0 (length - 1) with _ -> s

  (** [if_empty ~default s] return [~default] if [s] is empty, [s] otherwise *)
  let if_empty ~default s =
    match s with
    | "" -> default
    | s -> s

  (** [find_from s pos x] behaves as [find s x] but starts searching at position
      [pos]. [find s x] is equivalent to [find_from s 0 x].

      Raises:

      - Not_found if no substring is found

      - Invalid_argument if [pos] is not a valid position in the string.
        Example: String.find_from "foobarbaz" 4 "ba" = 6

      source :
      https://ocaml-batteries-team.github.io/batteries-included/hdoc2/BatString.html *)
  let find_from str pos sub =
    let len = length str in
    let sublen = length sub in
    if pos < 0 || pos > len then invalid_arg __FUNCTION__ ;
    if sublen = 0 then
      pos
    else
      let rec find ~str ~sub i =
        if i > len - sublen then
          raise Not_found
        else
          (* 0 <= i <= length str - length sub *)
          let rec loop ~str ~sub i j =
            if j = sublen then
              i
            else if
              (* 0 <= j < length sub *)
              (* ==>  0 <= i + j < length str *)
              unsafe_get str (i + j) <> unsafe_get sub j
            then
              find ~str ~sub (i + 1)
            else
              loop ~str ~sub i (j + 1) in
          loop ~str ~sub i 0 in
      find ~str ~sub pos

  (** [find s x] returns the starting index of the first occurrence of string
      [x] within string [s].

      Note that this implementation is optimized for short strings.

      Raises:

      - Not_found if [x] is not a substring of [s]. Example: String.find
        "foobarbaz" "bar" = 3

      source :
      https://ocaml-batteries-team.github.io/batteries-included/hdoc2/BatString.html *)
  let find str sub = find_from str 0 sub

  (** [exists str sub] returns [true] if [sub] is a substring of [str], [false]
      otherwise.

      Example: String.exists "foobarbaz" "obar" = true

      source :
      https://ocaml-batteries-team.github.io/batteries-included/hdoc2/BatString.html *)
  let exists str sub =
    try
      ignore (find str sub) ;
      true
    with Not_found -> false
end

module Lwt_list = struct
  include Lwt_list
  open Lwt.Infix

  let filter_mapi_s f l =
    let rec inner acc i = function
      | [] -> List.rev acc |> Lwt.return
      | hd :: tl -> (
        Lwt.apply (f i) hd >>= function
        | Some v -> (inner [@ocaml.tailcall]) (v :: acc) (i + 1) tl
        | None -> (inner [@ocaml.tailcall]) acc (i + 1) tl) in
    inner [] 0 l
end

module Map = struct
  include Map

  module type S = sig
    include S

    val key_compare : key -> key -> int

    val of_list : (key * 'a) list -> 'a t

    val take : int -> 'a t -> 'a t

    val take_last : int -> 'a t -> 'a t

    val trunc : int -> 'a t -> 'a t

    val trunc_last : int -> 'a t -> 'a t

    val add_if_not_bind : key -> 'a -> 'a t -> 'a t

    val add_if_not_bind_delayed : key -> (unit -> 'a) -> 'a t -> 'a t

    val update_default : key -> default:'a -> ('a -> 'a) -> 'a t -> 'a t

    val update_default_delayed :
      key -> get_default:(unit -> 'a) -> ('a -> 'a) -> 'a t -> 'a t

    val update_with_default : key -> default:'a -> ('a -> 'a) -> 'a t -> 'a t

    val update_with_default_delayed :
      key -> get_default:(unit -> 'a) -> ('a -> 'a) -> 'a t -> 'a t

    val simple_union : (key -> 'a -> 'a -> 'a) -> 'a t -> 'a t -> 'a t

    val bindings_without_keys : 'a t -> 'a list

    val rev_bindings : 'a t -> (key * 'a) list

    module Lwt : sig
      val fold_s : (key -> 'a -> 'b -> 'b Lwt.t) -> 'a t -> 'b -> 'b Lwt.t

      val map_s : ('a -> 'b Lwt.t) -> 'a t -> 'b t Lwt.t

      val update : key -> ('a option -> 'a option Lwt.t) -> 'a t -> 'a t Lwt.t

      val add_if_not_bind_delayed :
        key -> (unit -> 'a Lwt.t) -> 'a t -> 'a t Lwt.t

      val update_default :
        key -> default:'a -> ('a -> 'a Lwt.t) -> 'a t -> 'a t Lwt.t

      val update_default_delayed :
        key ->
        get_default:(unit -> 'a Lwt.t) ->
        ('a -> 'a Lwt.t) ->
        'a t ->
        'a t Lwt.t

      val update_with_default :
        key -> default:'a -> ('a -> 'a Lwt.t) -> 'a t -> 'a t Lwt.t

      val update_with_default_delayed :
        key ->
        get_default:(unit -> 'a Lwt.t) ->
        ('a -> 'a Lwt.t) ->
        'a t ->
        'a t Lwt.t
    end
  end

  module Make (Ord : OrderedType) : S with type key = Ord.t = struct
    include Make (Ord)

    let key_compare = Ord.compare

    let of_list l = List.fold_left (fun m (key, v) -> add key v m) empty l

    (** [take n m] returns up a map with the the [n] first elements from map
        [m], if available. *)
    let take n m =
      let aux key v (i, acc) =
        if i <= 0 then
          (i, acc)
        else
          (i - 1, add key v acc) in
      snd @@ fold aux m (n, empty)

    (** [take_last n m] returns up a map with the [n] last elements from map
        [m], if available. *)
    let take_last n m =
      let aux key v (i, acc) =
        if i <= 0 then
          (i, add key v acc)
        else
          (i - 1, acc) in
      snd @@ fold aux m (cardinal m - n, empty)

    (** [trunc size map] returns the same result as [take size map] but remove
        the overflow elements rather than taking the desired elements [m], if
        available. *)
    let trunc n m =
      let rec aux i m =
        if i > 0 then
          match max_binding_opt m with
          | None -> empty
          | Some (max_key, _) -> aux (i - 1) (remove max_key m)
        else
          m in
      aux (cardinal m - n) m

    (** [trunc_last size map] returns the same result as [take_last size map]
        but remove the overflow elements rather than taking the desired elements
        [m], if available. *)
    let trunc_last n m =
      let rec aux i m =
        if i > 0 then
          match min_binding_opt m with
          | None -> empty
          | Some (min_key, _) -> aux (i - 1) (remove min_key m)
        else
          m in
      aux (cardinal m - n) m

    (** [update_default key ~default f m] returns a map containing the same
        bindings as [m], except for the binding of [key]. Depending on the value
        of [y] where [y] is [find_opt key m], the binding of [key] is [f z] if
        [y] is [Some z], [default] otherwise. If [f z] is physically equal to
        [z], [m] is returned unchanged *)
    let update_default key ~default f map =
      update key
        (function
          | None -> Some default
          | Some v -> Some (f v))
        map

    (** Same as [update_default] but with a delayed value *)
    let update_default_delayed key ~get_default f map =
      update key
        (function
          | None -> Some (get_default ())
          | Some v -> Some (f v))
        map

    (** [add_if_not_bind key value map] returns the same result as
        [add key value map] if [key] is not bind in [map], returns [map]
        otherwise *)
    let add_if_not_bind key value map =
      update_default key ~default:value Fun.id map

    (** Same as [add_if_not_bind] but with a delayed value *)
    let add_if_not_bind_delayed key get_value map =
      update_default_delayed key ~get_default:get_value Fun.id map

    (** [update_with_default key ~default f m] gives the same result as
        [update_default key ~default f m], but if [find_opt key m] is [None],
        the binding of [key] is [f default] *)
    let update_with_default key ~default f map =
      update key
        (fun v_opt ->
          let v = Option.value ~default v_opt in
          Some (f v))
        map

    (** Same as [update_with_default] but with a delayed value *)
    let update_with_default_delayed key ~get_default f map =
      let v =
        match find_opt key map with
        | None -> get_default ()
        | Some v -> v in
      add key (f v) map

    (** [simple_union f m1 m2] gives the same result as
        [Map.union (fun key x y -> Some (f key x y) m1 m2)] *)
    let simple_union f map1 map2 =
      union (fun key x y -> Some (f key x y)) map1 map2

    (** [bindings_without_keys map] gives the same result as [bindings map] but
        without keys (this function is less efficient, it adds a + O(n)
        complexity). Same behaviour as :
        [List.filter_map (fun (_, x) -> Some x) @@ bindings map] but more
        efficient. *)
    let bindings_without_keys map = fold (fun _key x l -> x :: l) map []

    (** [rev_bindings map] gives the same result as [rev (bindings map)] but
        more efficiently. *)
    let rev_bindings map =
      fold (fun key v bindings -> (key, v) :: bindings) map []

    module Lwt = struct
      let fold_s f m acc =
        fold (fun key v acc -> Lwt.bind acc (f key v)) m (Lwt.return acc)

      let map_s f m =
        fold_s
          (fun key x acc ->
            let%lwt x' = f x in
            Lwt.return (add key x' acc))
          m empty

      let update key f m =
        let v_opt = find_opt key m in
        let%lwt v_opt = f v_opt in
        Lwt.return (update key (fun _ -> v_opt) m)

      let update_default key ~default f m =
        let%lwt v =
          match find_opt key m with
          | None -> Lwt.return default
          | Some v -> f v in
        Lwt.return (add key v m)

      let update_default_delayed key ~get_default f m =
        let%lwt v =
          match find_opt key m with
          | None -> get_default ()
          | Some v -> f v in
        Lwt.return (add key v m)

      let update_with_default key ~default f m =
        let v = Option.value ~default (find_opt key m) in
        let%lwt v = f v in
        Lwt.return (add key v m)

      let update_with_default_delayed key ~get_default f m =
        let%lwt v =
          match find_opt key m with
          | None -> get_default ()
          | Some v -> Lwt.return v in
        let%lwt v = f v in
        Lwt.return (add key v m)

      let add_if_not_bind_delayed key get_value m =
        update_default_delayed key ~get_default:get_value Lwt.return m
    end
  end
end

module Set = struct
  include Set

  module type S = sig
    include S

    val disjoint_union : t -> t -> t

    module Lwt : sig
      val fold_s : (elt -> 'a -> 'a Lwt.t) -> t -> 'a -> 'a Lwt.t
    end
  end

  module Make (Ord : OrderedType) : S with type elt = Ord.t = struct
    include Make (Ord)

    let disjoint_union x y = diff (union x y) (inter x y)

    module Lwt = struct
      let fold_s f s acc =
        fold (fun elt acc -> Lwt.bind acc (f elt)) s (Lwt.return acc)
    end
  end
end

(* ==========================================================================
   ============================= DEPENDANT LIBS =============================
   ========================================================================== *)

module IdMap : sig
  (** [IdMap] is a module that implements an auto-incrementing key map.

      @deps: [GenId] *)

  type 'a t

  type id

  val create : unit -> 'a t

  val add : 'a t -> 'a -> id

  val remove : 'a t -> id -> unit

  val iter : (id -> 'a -> unit) -> 'a t -> unit
end = struct
  module IdMap = Map.Make (Int)

  type id = int

  type 'a t = {
    gen_id : GenId.t;
    mutable map : 'a IdMap.t;
  }

  let create () = { gen_id = GenId.create (); map = IdMap.empty }

  let add t x =
    let id = GenId.gen t.gen_id in
    t.map <- IdMap.add id x t.map ;
    id

  let remove t id = t.map <- IdMap.remove id t.map

  let iter f t = IdMap.iter f t.map
end

(* @extension : Change the capacity *)
module BoundedMap = struct
  (** [BoundedMap] is a module that implements a functor on maps that prevents
      the map from exceeding a capacity by removing the smallest binding,
      defined by the function of [compare] on keys, each time a new binding is
      added.

      @deps: extended-[Map] *)

  module type Capacity = sig
    val capacity : int
  end

  module type S = Map.S

  module Make (S : Map.S) (C : Capacity) :
    S with type key = S.key and type 'a t = 'a S.t = struct
    module Std_Lwt = Lwt
    include S

    let resize map = trunc_last C.capacity map

    let add key value map = add key value map |> resize

    let update key f map = update key f map |> resize

    let singleton key value =
      if C.capacity > 0 then
        singleton key value
      else
        empty

    let merge f map1 map2 = merge f map1 map2 |> resize

    let union f map1 map2 = union f map1 map2 |> resize

    let add_seq seq map = add_seq seq map |> resize

    let of_seq seq = of_seq seq |> resize

    let take n m = take n m |> resize

    let take_last n m = take_last n m |> resize

    let add_if_not_bind key value map = add_if_not_bind key value map |> resize

    let add_if_not_bind_delayed key get_value map =
      add_if_not_bind_delayed key get_value map |> resize

    let update_default key ~default f map =
      update_default key ~default f map |> resize

    let update_default_delayed key ~get_default f m =
      update_default_delayed key ~get_default f m |> resize

    let update_with_default key ~default f map =
      update_with_default key ~default f map |> resize

    let update_with_default_delayed key ~get_default f m =
      update_with_default_delayed key ~get_default f m |> resize

    let simple_union f map1 map2 = simple_union f map1 map2 |> resize

    module Lwt = struct
      module Lwt = Std_Lwt
      include S.Lwt

      let fold_s f m acc =
        Lwt_list.fold_left_s (fun acc (key, x) -> f key x acc) acc (bindings m)

      let map_s f m =
        fold_s
          (fun key x acc ->
            let%lwt x' = f x in
            Lwt.return (add key x' acc))
          m empty

      let update key f m = Lwt.map resize @@ update key f m

      let add_if_not_bind_delayed key get_value map =
        Lwt.map resize @@ add_if_not_bind_delayed key get_value map

      let update_default key ~default f m =
        Lwt.map resize @@ update_with_default key ~default f m

      let update_default_delayed key ~get_default f m =
        Lwt.map resize @@ update_default_delayed key ~get_default f m

      let update_with_default key ~default f m =
        Lwt.map resize @@ update_with_default key ~default f m

      let update_with_default_delayed key ~get_default f m =
        Lwt.map resize @@ update_with_default_delayed key ~get_default f m
    end
  end
end

module Listener : sig
  (** [Listener] is a convenient module to easily create and interact with
      listeners.

      Any functions can be connected to the listener at any time. Each time the
      listener is triggered, all connected functions will also be triggered.
      (See [connect])

      The listener can be triggered as many times as desired. All functions
      connected to the listener at the moment of triggering will also be
      triggered. (See [trigger]).

      A storage is linked to the listener so that when a function connects to
      the listener, it can be triggered immediately. When the listener is
      created, the storage is None. When the listener is triggered, the storage
      is updated with the storage_update function. (See [create]).
      
      @deps: [IdMap] *)

  type 'a t

  type 'a connection_id

  val create : ?storage:'a -> unit -> 'a t

  val connect : ?use_storage:bool -> 'a t -> ('a -> unit) -> 'a connection_id

  val disconnect : 'a connection_id -> unit

  val trigger : 'a t -> 'a -> unit

  val get : 'a t -> 'a Lwt.t

  val fold :
    ?use_storage:bool ->
    ('a -> 'b -> 'a) ->
    'a ->
    'b t ->
    'b connection_id * 'a t

  val map : ?use_storage:bool -> ('a -> 'b) -> 'a t -> 'a connection_id * 'b t

  val filter :
    ?use_storage:bool -> ('a -> bool) -> 'a t -> 'a connection_id * 'a t

  val filter_map :
    ?use_storage:bool -> ('a -> 'b option) -> 'a t -> 'a connection_id * 'b t
end = struct
  type 'a t = {
    connections : ('a -> unit) IdMap.t;
    mutable storage : 'a option;
  }

  type 'a connection_id = {
    id : IdMap.id;
    listener : 'a t;
  }

  (** [create ?storage ()] creates a listener [l] with empty storage. Each time
      the listener [l] will be triggered, the storage will store the new
      triggered value. Provide [storage] to initialize the storage. *)
  let create ?storage () = { connections = IdMap.create (); storage }

  (** [connect ?use_storage l f] connects the function [f] to the listener [l].
      If [use_storage] is true, [f] is executed with the storage of listener [l]
      if it exists. By default, [use_storage] is true. Each time the listener
      [l] is triggered with a value [v], [f] will be triggered with the same
      value [v]. Returns a connection identifier. *)
  let connect ?(use_storage = true) listener f =
    if use_storage then Option.iter f listener.storage ;
    let id = IdMap.add listener.connections f in
    { id; listener }

  (** [disconnect id] disconnects the function, associated with the connection
      identifier [id], from the concerned listener. *)
  let disconnect { id; listener } = IdMap.remove listener.connections id

  (** [trigger l v] triggers the listener [l] with the value [v]. All functions
      connected at the moment of triggering are triggered with the same value
      [v]. The storage is also updated. *)
  let trigger listener value =
    listener.storage <- Some value ;
    IdMap.iter (fun _ f -> f value) listener.connections

  (** [get listener] returns the [listener] storage value if it exists,
      otherwise it returns the next value. *)
  let get listener =
    let getter, trigger = Lwt.task () in
    let connection = ref None in
    let get v =
      let () = try Lwt.wakeup trigger v with Invalid_argument _ -> () in
      Option.iter disconnect !connection in
    connection := Some (connect listener get) ;
    match listener.storage with
    | Some storage ->
      Lwt.cancel getter ;
      Lwt.return storage
    | None -> getter

  (** [fold ?use_storage f init listener] creates a new listener, with a storage
      [storage], that is connected to [listener] and is triggered with the value
      [f storage value] when [listener] is triggered with [value]. *)
  let fold ?use_storage f acc listener =
    let new_listener = create ~storage:acc () in
    let connection =
      connect ?use_storage listener @@ fun value ->
      let acc = Option.value new_listener.storage ~default:acc in
      (trigger new_listener) (f acc value) in
    (connection, new_listener)

  (** [filter_map ?use_storage f listener] creates a new listener that is
      connected to [listener] and is triggered with the value [value'] when
      [listener] is triggered with a value [value] such that
      [f value = Some value']. *)
  let filter_map ?use_storage f listener =
    let storage = Option.bind listener.storage f in
    let new_listener = create ?storage () in
    let connection =
      connect ?use_storage listener @@ fun value ->
      Option.iter (trigger new_listener) (f value) in
    (connection, new_listener)

  (** [map ?use_storage f listener] creates a new listener that is connected to
      [listener] and is triggered with the value [f value] when [listener] is
      triggered with a value [value]. *)
  let map ?use_storage f listener =
    filter_map ?use_storage (fun value -> Some (f value)) listener

  (** [filter ?use_storage f listener] creates a new listener that is connected
      to [listener] and is triggered with the value [value] when [listener] is
      triggered with [value] and [f value] is true. *)
  let filter ?use_storage f listener =
    filter_map ?use_storage
      (fun value ->
        if f value then
          Some value
        else
          None)
      listener
end

module Tempotbl = struct
  (** [Tempotbl] is a wrapper around [Hashtbl] that allows to fill/update/clean
      the table with the wanted duration. It provides a convenient interface
      that can be used by providing a [Specification] module to [Tempotbl.Make]
      functor. A quick overview of each type/function that needs to be provided:

      [type key/data] : will replace the [('a,'b) t] from [Hashtbl] into
      [(key, data) t]

      [capacity] : expected number of elements that will be in the table

      [duration] : frequency at which elements will be removed/cleaned from the
      table

      [update_on_access] : allows to disable/enable the timestamp update of
      table's elements everytime they are accessed [sleep] : provides the method
      of your choice to pass time 
      
      @deps: [SafeTrigger], [Time] *)

  module type Specification = sig
    type key

    type data

    val capacity : int

    val duration : float

    val update_on_access : bool

    val sleep : float -> unit Lwt.t
  end

  module type S = sig
    type key

    type data

    type t

    val create : trigger_routine_from_start:bool -> t

    val clear : t -> unit

    val clean_routine : t -> unit

    val add : ?default_ts:float -> t -> key -> data -> unit

    val get :
      t -> key -> search_for:(unit -> (data * float) Lwt.t) -> data Lwt.t

    val find_opt : t -> key -> data option

    val length : t -> int
  end

  module Make (Spec : Specification) :
    S with type key = Spec.key and type data = Spec.data = struct
    include Spec

    type temporary_data = {
      data : Spec.data;
      timestamp : float;
    }

    type t = {
      tbl : (Spec.key, temporary_data) Hashtbl.t;
      safe_trigger : SafeTrigger.t;
    }

    (** [clean_routine tbl] cleans [tbl] every Spec.duration by looking for each bindings of
        [tbl] if it's outdated, i.e :
        {current_time - binding's_last_time_solicited >= Spec.duration} *)
    let clean_routine { tbl; safe_trigger } =
      if not @@ SafeTrigger.access safe_trigger then begin
        SafeTrigger.trigger safe_trigger ;
        let clean key { timestamp; _ } =
          let now = Time.universal () in
          if now -. timestamp >= Spec.duration then Hashtbl.remove tbl key in
        let rec routine () =
          Hashtbl.iter clean tbl ;
          let%lwt () = Spec.sleep Spec.duration in
          routine () in
        Lwt.async_exn ~__FUNCTION__ @@ fun () -> routine ()
      end

    (** [create ~trigger_routine_from_start] creates a new, empty tempotbl, with
        initial size the capacity specified in `Spec`, if
        [~trigger_routine_from_start] is true it will trigger the
        [clean_routine] function immediatly after its creation *)
    let create ~trigger_routine_from_start : t =
      let tbl = Hashtbl.create Spec.capacity in
      let safe_trigger = SafeTrigger.create () in
      let t = { tbl; safe_trigger } in
      if trigger_routine_from_start then clean_routine t ;
      t

    (** [clear tbl] empty a tempotbl and shrink the size of the bucket table to
        its initial size. *)
    let clear { tbl; _ } = Hashtbl.reset tbl

    (** [update tbl key data] behave as [Hashtbl.replace]
        [Hashtbl.replace tbl key data] replaces the current binding of [key] in
        [tbl] by a binding of [key] to [data]. If key is unbound in [tbl], a
        binding of [key] to [data] is added to [tbl]. This is functionally
        equivalent to [Hashtbl.remove tbl key] followed by
        [Hashtbl.add tbl key data]. *)
    let update tbl key data = Hashtbl.replace tbl key data

    (** [add ?default_ts tbl key data] adds a binding of [key] to [data] in
        table [tbl] with an additional timestamp information : if [?default_ts]
        is optional we use [Time.universal], [?default_ts] otherwise. *)
    let add ?default_ts { tbl; _ } key data =
      match default_ts with
      | None ->
        let timestamp = Time.universal () in
        update tbl key { data; timestamp }
      | Some timestamp -> update tbl key { data; timestamp }

    (** [get tbl key ~search_for] returns the current binding of [key] in [tbl]
        and update its timestamp, if it can't be found then [~search_for] is
        called and [tbl] is updated with a new [key]-binding pair returned by
        [~search_for] and the binding is returned *)
    let get { tbl; _ } key ~search_for =
      match Hashtbl.find_opt tbl key with
      | None ->
        let%lwt data, timestamp = search_for () in
        update tbl key { data; timestamp } ;
        Lwt.return data
      | Some tmp_data ->
        let timestamp = Time.universal () in
        if Spec.update_on_access then update tbl key { tmp_data with timestamp } ;
        Lwt.return tmp_data.data

    (** [find_opt tbl x] returns the current binding (only the data) of [x] in
        [tbl], or None if no such binding exists.*)
    let find_opt { tbl; _ } x =
      match Hashtbl.find_opt tbl x with
      | None -> None
      | Some temporary_data -> Some temporary_data.data

    (** [length tbl] returns the number of bindings in [tbl]. It takes constant
        time. Multiple bindings are counted once each, so [length] gives the
        number of times [Hashtbl.iter] calls its first argument.*)
    let length { tbl; _ } = Hashtbl.length tbl
  end
end

module Periodic_bundled_apply : sig
  (** [Periodic_bundled_apply] is a module that implements a function whose
      purpose is to force a pause time between calls to the same function, see
      [make].

      @deps: [ConcurrentValue] *)

  val make :
    sleep:(float -> unit Lwt.t) ->
    delay:float ->
    ('elt list -> unit) ->
    'elt ->
    unit
end = struct
  module Automaton = struct
    [@@@ocamlformat "wrap-comments=false"]
    (*
           ------
       Opening   |
          |      v
          [ Open ]
          ^      |
          |   Entering
       Opening   |
          |      v
          [ Close ]
          ^      |
          |   Entering
       Opening   |
          |      v
         [ Waiting ]
          ^      |
          |   Entering
           ------
     *)

    type 'elt state =
      | Open
      | Close
      | Waiting of 'elt list

    type 'elt transition =
      | Entering of 'elt
      | Opening

    let initial_state : 'elt state = Open

    let apply ~receive op state =
      match (state, op) with
      | Open, Entering elt ->
        receive [elt] ;
        Close
      | Close, Entering elt -> Waiting [elt]
      | Waiting elts, Entering elt -> Waiting (elt :: elts)
      | Waiting elts, Opening ->
        receive elts ;
        Close
      | Close, Opening -> Open
      | Open, Opening -> (* Unexpected case *) Open
  end

  (** [make ~sleep ~delay f] returns a function [apply].

      Calling [apply elt] calls [f \[elt\]].

      If [apply] has called [f] for less than [delay] second (whose computation
      is induced by [sleep]), calls to [apply elt], during that time, will not
      call [f] but will put [elt] on hold until the end of the [delay]. [f] will
      be called at the end with all [elt]s on hold.

      In this case, the order of the [elt]s in the call of [f] is the reverse
      order in which they were called with [apply]. *)
  let make ~sleep ~delay f =
    let state = ConcurrentValue.create Automaton.initial_state in
    let rec apply transition =
      ConcurrentValue.simple_update state @@ Automaton.apply ~receive transition
    and receive elts =
      Lwt.async_exn ~__FUNCTION__ @@ fun () ->
      let () = f elts in
      let%lwt () = sleep delay in
      apply Automaton.Opening in
    fun elt ->
      Lwt.async_exn ~__FUNCTION__ @@ fun () -> apply (Automaton.Entering elt)
end
