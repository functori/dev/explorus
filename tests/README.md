# TESTS

This folder contains all the tests that can be executed by `scripts/alcotest.sh`.

It highly relies on the files located in `src/common` and `src/lib`.
