(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ================================= TYPES ================================= *)

type warnings = {
  system_is_late : bool;
  node_is_late : bool;
}
[@@deriving jsoo]

type round_info = Tezos_process.Clock.round_info = {
  curr_round : int;
  curr_max_round : int;
  curr_pos_round : int;
}
[@@deriving jsoo]

type data = {
  round_info : round_info option; [@mutable]
  warnings : warnings; [@mutable]
}
[@@deriving jsoo { remove_prefix = false }]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-round_clock"

  let element =
    Vue_component.CRender
      (Render.round_clock_render, Render.round_clock_static_renders)

  let props = None
end)

(* =========================== REFRESH MECHANISMS =========================== *)

let check_warnings app { curr_max_round; curr_pos_round; _ } =
  let set_warnings ?(system_is_late = false) ?(node_is_late = false) () =
    app##.warnings := warnings_to_jsoo { system_is_late; node_is_late } in
  if curr_pos_round > curr_max_round then set_warnings ~node_is_late:true () ;
  if curr_pos_round < 0 then set_warnings ~system_is_late:true ()

let central_refresh app =
  let callback () =
    Lwt.async_exn ~__FUNCTION__ @@ fun () ->
    let round_info = Tezos_process.Clock.get_round_info () in
    check_warnings app round_info ;
    app##.round_info_ := def (round_info_to_jsoo round_info) ;
    Lwt.return_unit in
  let open Central_state.Intervals in
  add_intervals ~clearable_type:Unclearable ~callback ~loop_frequency:1.

let init () =
  let data _ =
    object%js
      val mutable round_info_ = undefined

      val mutable warnings =
        warnings_to_jsoo { system_is_late = false; node_is_late = false }
    end in
  C.make ~data ~lifecycle:[("beforeMount", central_refresh)] ()
