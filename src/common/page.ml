(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type t =
  | Blocks
  | Consensus_ops
  | Bakers_activity
  | Explorer
  | Soru
  | Dal
  | Logs
  | Settings

module Logical_Component = struct
  type t =
    | BR (* baking rights *)
    | RC (* round clock *)
    | LB (* latest blocks*)
    | CO (* consensus ops*)
    | EA (* attesting activity *)
    | OE (* operation explorer *)
    | SC (* soru *)
    | DL (* dal *)
    | LG (* logs *)
    | ST (* settings *)
    | SL (* share link *)
    | PR (* pyrometer *)
    | IB (* info bar *)
    | IP (* protocol_injection *)
    | NC (* network constants *)
    | CP (* consensus progression *)

  let compare = compare
end

module LC = Logical_Component

module Logical_Sub_Component = struct
  type t =
    | HD (* hash displayer *)
    | CT (* commitment tree *)
    | SCC (* soru commitment *)

  let constraints = function
    | HD -> LC.[BR; LB; CO; EA; OE; SC; DL; IB]
    | CT -> LC.[SC]
    | SCC -> LC.[SC]
end

module LSC = Logical_Sub_Component

module Logical_Component_Set = struct
  module Logical_Components = Set.Make (LC)
  include Logical_Components

  type t = Logical_Components.t

  let transition = Logical_Components.disjoint_union
end

module LCS = Logical_Component_Set

(* ========================== CONVERSION FUNCTIONS ========================== *)

let to_path = function
  | Blocks -> "blocks"
  | Consensus_ops -> "consensus_ops"
  | Bakers_activity -> "bakers_activity"
  | Explorer -> "explorer"
  | Soru -> "soru"
  | Dal -> "dal"
  | Logs -> "changelogs"
  | Settings -> "settings"

let of_path = function
  | "blocks" -> Blocks
  | "consensus_ops" -> Consensus_ops
  | "bakers_activity" -> Bakers_activity
  | "explorer" -> Explorer
  | "soru" -> Soru
  | "dal" -> Dal
  | "changelogs" -> Logs
  | "settings" -> Settings
  | _ (* in other cases go to blocks to avoid crashes *) -> Blocks

let to_lcs = function
  | Blocks -> LCS.of_list LC.[BR; LB]
  | Consensus_ops -> LCS.of_list LC.[BR; CO]
  | Bakers_activity -> LCS.of_list LC.[EA]
  | Explorer -> LCS.of_list LC.[OE]
  | Soru -> LCS.of_list LC.[SC]
  | Dal -> LCS.of_list LC.[DL]
  | Logs -> LCS.of_list LC.[LG]
  | Settings -> LCS.of_list LC.[ST]

(* ============================ TOP LEVEL VALUES ============================ *)

let pages =
  [Blocks; Consensus_ops; Bakers_activity; Explorer; Soru; Dal; Logs; Settings]
