(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* ================================= TYPES ================================= *)

type timestamp = Tezos_process.Blockchain.timestamp = {
  time : string;
  date : string;
}
[@@deriving jsoo]

type missing_attester = Tezos_process.Blockchain.missing_attester = {
  miss_attester : string;
  miss_slots : int;
}
[@@deriving jsoo]

type block_details = Tezos_process.Blockchain.block_details = {
  bd_level : int;
  bd_round : int;
  bd_hash : string;
  bd_pred_hash : string;
  bd_manager_ops : int; (* transaction, origination... *)
  bd_manager_ops_batches : int;
  bd_baker : string;
  bd_baker_rew : float;
  bd_proposer : string;
  bd_proposer_rew : float;
  bd_attestation_power : float;
  bd_missed_slots : int;
  bd_missing_attesters : missing_attester list;
  bd_timestamp : timestamp;
  bd_fitness : fitness;
  bd_dal_attestation : int option;
}
[@@deriving jsoo]

type blocks_details = block_details list [@@deriving jsoo]

type data = {
  blocks_details : blocks_details option; [@mutable]
  protocol_transition_level : int;
}
[@@deriving jsoo { remove_prefix = false }]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-latest_blocks"

  let element =
    Vue_component.CRender
      (Render.latest_blocks_render, Render.latest_blocks_static_renders)

  let props = None
end)

(* =========================== REFRESH MECHANISMS =========================== *)

let update_latest_blocks_vue app chain =
  app##.blocks_details_ := def (blocks_details_to_jsoo chain)

let init ~protocol_transition_level () =
  let data _ =
    object%js
      val mutable blocks_details_ = undefined

      val protocol_transition_level_ = protocol_transition_level
    end in
  let at_destroy = PendingFunctions.create () in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            let id =
              Listener.connect ~use_storage:true
                Tezos_process.Blockchain.chain_update_listener
                (update_latest_blocks_vue app) in
            PendingFunctions.add at_destroy @@ fun () -> Listener.disconnect id
        );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ()
