# SCRIPTS

This folder contains a compendium of useful scripts.

-   `alcotest.sh` execute all the tests related to OCaml code base.
-   `auto-hard-refresh.sh` allows the automated hard refresh of the browser on each deployment.
-   `check-static-explorus-pos.sh` checks if the automated hard refresh will fail.
-   `get-flextesa-for-linux.sh` downloads flextesa for users that use linux based OS.
-   `install-deps.sh` install all necessary dependencies to compile explorus.
-   `lint-html.sh` check if the `.html` files are well indented.
-   `lint.sh` check if the `.ml/.mli` files are well indented.
-   `sandbox_node.sh` allows you to launch a tezos node in a sandbox.
-   `track-dead-css-from.sh` tracks every dead css in the code.
-   `update_weeklynet_address.sh` update **Weeklynet**'s address automaticaly.
