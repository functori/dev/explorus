(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos.Entities
open Tezos_process.Consensus

(* ================================= TYPES ================================= *)

type ballots = Ballots.t = {
  counter : int;
  percent_progression : float;
  consensus : bool;
}
[@@deriving jsoo]

type voting_state = {
  ballots : ballots;
  reaching_quorum_operation_delta : float option;
  last_operation_delta : float option;
}
[@@deriving jsoo]

type round_consensus_state = {
  round : int;
  consensus_state : voting_state AttesPreattes.t;
}
[@@deriving jsoo]

type level_consensus_state = {
  level : int;
  rounds_consensus_state : round_consensus_state list;
}
[@@deriving jsoo]

type consensus_states = level_consensus_state list [@@deriving jsoo]

type data = {
  tab_id : string;
  current_tab_index : int; [@mutable]
  can_scroll_up : bool; [@mutable]
  can_scroll_down : bool; [@mutable]
  consensus_committee_size : int option; [@mutable]
  consensus_states : consensus_states; [@mutable]
  protocol_transition_level : int;
}
[@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-consensus_progression"

  let element =
    Vue_component.CRender
      ( Render.consensus_progression_render,
        Render.consensus_progression_static_renders )

  let props = None
end)

(* ============================ DYNAMIC DISPLAY ============================ *)

let sort_bindings_by_decreasing_order bindings =
  List.sort (fun (k1, _) (k2, _) -> compare k2 k1) bindings

let get_voting_state
    Votings.
      { ballots; reaching_quorum_operation_delta; last_operation_delta; _ } =
  { ballots; reaching_quorum_operation_delta; last_operation_delta }

let get_round_consensus_state (round, State.{ votings; _ }) =
  let consensus_state = AttesPreattes.map get_voting_state votings in
  { round; consensus_state }

let get_rounds_consensus_state rounds =
  rounds |> RoundMap.bindings |> sort_bindings_by_decreasing_order
  |> List.map get_round_consensus_state

let get_level_consensus_state (level, LevelInfo.{ rounds; _ }) =
  let rounds_consensus_state = get_rounds_consensus_state rounds in
  { level; rounds_consensus_state }

let filter_handled_consensus_states =
  LevelMap.filter_map @@ fun _ level_info ->
  let rounds =
    RoundMap.filter
      (fun _round State.{ already_processed; _ } -> already_processed)
      level_info.LevelInfo.rounds in
  if RoundMap.is_empty rounds then
    None
  else
    Some LevelInfo.{ level_info with rounds }

let get_consensus_states consensus_states =
  consensus_states |> filter_handled_consensus_states |> LevelMap.bindings
  |> sort_bindings_by_decreasing_order
  |> List.map get_level_consensus_state

let consensus_states_connection ~app =
  Listener.connect Tezos_process.ConsensusStates.applied_update_listener
  @@ fun consensus_states ->
  let consensus_states = get_consensus_states consensus_states in
  let network_constants = Central_state.Network.get_network_constants () in
  app##.consensus_states_ := consensus_states_to_jsoo consensus_states ;
  app##.consensus_committee_size_
  := def network_constants.nc_consensus_committee_size

(* ========================= SCROLL BUTTON HANDLING ========================= *)

let update_scroll_buttons app =
  let open Js_of_ocaml in
  Opt.iter (Dom_html.document##getElementById app##.tab_id_) @@ fun tab ->
  let rec get_current_tab_index i =
    match Lib_js.get_child tab i with
    | None -> 0
    | Some child ->
      let top =
        child##getBoundingClientRect##.top -. tab##getBoundingClientRect##.top
      in
      let height =
        Optdef.get child##getBoundingClientRect##.height (fun () -> 0.) in
      (* Check if the child is half visible *)
      if top +. (height /. 2.) > 0. then
        i
      else
        get_current_tab_index (i + 1) in
  let current_tab_index = get_current_tab_index 0 in
  app##.current_tab_index_ := current_tab_index ;
  app##.can_scroll_up_ := Js.bool (current_tab_index <> 0) ;
  app##.can_scroll_down_ :=
    Js.bool (current_tab_index <> tab##.childNodes##.length - 1)

let init_observer app at_destroy =
  let open Js_of_ocaml in
  let rec get_tab () =
    match Opt.to_option (Dom_html.document##getElementById app##.tab_id_) with
    | None -> Lwt.bind (EzLwtSys.sleep 0.5) get_tab
    | Some tab -> Lwt.return tab in
  Lwt.async_exn ~__FUNCTION__ @@ fun () ->
  let%lwt tab = get_tab () in
  let node = (tab :> Dom.node Js.t) in
  let f _records _observer = update_scroll_buttons app in
  let observer = MutationObserver.observe ~node ~f ~child_list:true () in
  PendingFunctions.add at_destroy (fun () -> observer##disconnect) ;
  Lwt.return_unit

let scroll_to app dir =
  let open Js_of_ocaml in
  let get_top_of_child tab i =
    match (Lib_js.get_child tab 0, Lib_js.get_child tab i) with
    | Some first_child, Some child ->
      Some
        (child##getBoundingClientRect##.top
        -. first_child##getBoundingClientRect##.top)
    | _ -> None in
  Opt.iter (Dom_html.document##getElementById app##.tab_id_) @@ fun tab ->
  match Js.to_string dir with
  | "up" -> Lib_js.scrollTo ~top:0. tab
  | "previous" ->
    get_top_of_child tab (app##.current_tab_index_ - 1)
    |> Option.iter @@ fun top -> Lib_js.scrollTo ~top tab
  | "next" ->
    get_top_of_child tab (app##.current_tab_index_ + 1)
    |> Option.iter @@ fun top -> Lib_js.scrollTo ~top tab
  | "down" ->
    let top = tab##.scrollHeight - tab##.clientHeight |> Float.of_int in
    Lib_js.scrollTo ~top tab
  | dir -> print_endline @@ Printf.sprintf "Unknown scroll direction: %s" dir

let init ~protocol_transition_level () =
  C.add_method0 "update_scroll_buttons" update_scroll_buttons ;
  C.add_method1 "scroll_to" scroll_to ;
  let data _ =
    object%js
      val tab_id_ = Js_of_ocaml.Js.string "consensus_progression_tab_id"

      val mutable current_tab_index_ = 0

      val mutable can_scroll_up_ = _false

      val mutable can_scroll_down_ = _false

      val mutable consensus_committee_size_ = undefined

      val mutable consensus_states_ = consensus_states_to_jsoo []

      val protocol_transition_level_ = protocol_transition_level
    end in
  let at_destroy = PendingFunctions.create () in
  C.make ~data
    ~lifecycle:
      [
        ( "beforeMount",
          fun app ->
            init_observer app at_destroy ;
            let consensus_states_connection_id =
              consensus_states_connection ~app in
            PendingFunctions.add at_destroy @@ fun () ->
            Listener.disconnect consensus_states_connection_id );
        ("destroyed", fun _app -> PendingFunctions.run at_destroy ());
      ]
    ()
