# How to make a release

Releases are generated automatically by gitlab's CI when a tag is created.

To make a release, add a tag in the form of a version.

Example: [1.0]

# How gitlab's CI generate the release

When a tag is created on a commit 3 jobs will successivly run:

 - build: will generate explorus.js and the all _www/ folder
 - upload: will zip the _www/ result and upload it on the [gitlab package repository](https://gitlab.com/functori/dev/explorus/-/packages)
 - release: create the release with the uploaded zip
