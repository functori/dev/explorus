#!/bin/sh

JSON_FILE="static/json/parameters.json"
echo "Enter your own node address for Weeklynet:"
read EXPLORUS_NODE_ADDRESS
TEZTNETS_NODE_ADDRESS=$(curl -s https://teztnets.com/teztnets.json | jq '.[] | select(.human_name == "Weeklynet")'.rpc_url | tr -d '"')

echo $(jq '.weeklynet.addresses = [$EXPLORUS_NODE_ADDRESS]' --arg EXPLORUS_NODE_ADDRESS $EXPLORUS_NODE_ADDRESS $JSON_FILE) > $JSON_FILE
echo $(jq '.weeklynet.addresses += [$TEZTNETS_NODE_ADDRESS]' --arg TEZTNETS_NODE_ADDRESS $TEZTNETS_NODE_ADDRESS $JSON_FILE) > $JSON_FILE 
npx prettier --write $JSON_FILE
