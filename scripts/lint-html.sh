#!/bin/sh

# Check if npx exists
if ! command -v npx
then
   echo "Command 'npx' could not be found"
   exit 1
fi

prettier_test=`npm list prettier | grep prettier`

# Check if prettier exists
if [ "$prettier_test" = "" ] ;
then
   echo "Command 'prettier' could not be found"
   exit 1
fi

# Check if git exists
if ! command -v git
then
   echo "Command 'git' could not be found"
   exit 1
fi

before_formatting=`git diff`
make format-html
after_formatting=`git diff`
fileonly=`git diff --name-only`

if [ "$after_formatting" = "$before_formatting" ] ; then
    echo "Success: all html files are well indented"
    exit 0
else
    echo "Failure: following html files are not well indented:"
    echo "$fileonly"
    exit 1
fi
