(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml

module Text = struct
  let before_copy = "Copy url & network"

  let after_copy = "Copied !"
end

type data = { text : string [@mutable] } [@@deriving jsoo]

module C = Vue_component.Make (struct
  class type data = data_jsoo

  class type all =
    object
      inherit data
    end

  let name = "v-share_url"

  let element =
    Vue_component.CRender
      (Render.share_url_render, Render.share_url_static_renders)

  let props = None
end)

let purge_sharp url =
  if String.last_is '#' url then
    String.without_last url
  else
    url

let copy_url app =
  let network_state = Central_state.Network.get_network_state () in
  let network =
    match network_state with
    | Central_state.Network.Customnet ->
      Central_state.Persistency.Address.get ()
    | _ -> Central_state.Network.network_to_string network_state in
  let url = purge_sharp @@ Custom_router.get_url () in
  let url_network =
    match Custom_router.(get_query () |> extract_network_query) with
    | None -> url ^ Custom_router.construct_network_param network
    | Some _ -> url in
  app##.text := Js.string Text.after_copy ;
  Js.Unsafe.global##.navigator##.clipboard##writeText url_network

let hover_copy app = app##.text := Js.string Text.before_copy

let init () =
  C.add_method0 "copy_url" copy_url ;
  C.add_method0 "hover_copy" hover_copy ;
  let data _ =
    object%js
      val mutable text = Js.string Text.before_copy
    end in
  C.make ~data ()
