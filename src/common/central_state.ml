(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2024, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* /!\ BE CAREFUL WHEN CHANGING THE WAY CENTRAL STATE MODULES WORK IT MAY (WILL
   MOST LIKELY) CHANGE (AND BREAK) THE BEHAVIOUR OF EXPLORUS /!\ *)

(* ================================ DESTRUCT ================================ *)

module DJson = struct
  open Json_types

  let parameters = Lib_js.destruct_json_lwt Paths.parameters parameters_enc

  let d_known_bakers =
    Lib_js.destruct_json_lwt Paths.known_bakers known_bakers_enc

  let known_bakers =
    let%lwt known_bakers = d_known_bakers in
    Lwt_list.map_p (fun kb -> Lwt.return (kb.pkh, kb.name)) known_bakers

  let components_states =
    Lib_js.destruct_json_lwt Paths.components_states components_states_enc

  let protocol_transitions =
    Lib_js.destruct_json_lwt Paths.protocol_transitions protocol_transitions_enc
end

(* ================================= MODE ================================= *)

module Mode = struct
  type mode =
    | Normal
    | Rolled

  let state = ref Normal

  let revert () =
    match !state with
    | Normal -> state := Rolled
    | Rolled -> state := Normal

  let current_state () = !state

  let rolled_bid = ref 0

  let get_rolled_bid () = Int.to_string !rolled_bid

  let set_rolled_bid bid = rolled_bid := bid

  module BState = struct
    let bid () =
      match current_state () with
      | Normal -> "head" (* always *)
      | Rolled -> get_rolled_bid ()
  end
end

(* ========================== INTERVALS/CALLBACKS ========================== *)

module Intervals : sig
  type t =
    | Explorer
    | Baking_rights_rounds
    | Unclearable (* when interval should be unclearable *)

  val clear_intervals : t -> unit

  val add_intervals :
    clearable_type:t -> callback:(unit -> unit) -> loop_frequency:float -> unit
end = struct
  module Clearable_Interval = struct
    type t =
      | Explorer
      | Baking_rights_rounds
      | Unclearable

    let compare = compare
  end

  include Clearable_Interval
  module IntervalsMap = Map.Make (Clearable_Interval)

  let intervals_types = [Baking_rights_rounds; Explorer]

  let intervals : Js_of_ocaml.Dom_html.interval_id list IntervalsMap.t ref =
    ref
    @@ List.fold_left
         (fun intervals_map interval_type ->
           IntervalsMap.add interval_type [] intervals_map)
         IntervalsMap.empty intervals_types

  let clear_intervals interval_type =
    intervals :=
      IntervalsMap.update interval_type
        (function
          | None -> None
          | Some interval_ids ->
            List.iter
              (fun interval_id ->
                Js_of_ocaml.Dom_html.window##clearInterval interval_id)
              interval_ids ;
            Some [])
        !intervals

  let add_intervals ~clearable_type ~callback ~loop_frequency =
    let interval =
      Js_of_ocaml.Dom_html.window##setInterval
        (Ezjs_min.wrap_callback callback)
        (Time.seconds_to_ms loop_frequency) in
    match clearable_type with
    | Unclearable -> ()
    | interval_type ->
      intervals :=
        IntervalsMap.update interval_type
          (function
            | None -> Some [interval]
            | Some interval_ids -> Some (interval :: interval_ids))
          !intervals
end

(* ================================= CACHES ================================= *)

module Cache = struct
  (* this module contains all current caches used through explorus'
     implementation *)
  let lr_blocks = ConcurrentValue.create Cache.LevelRoundBlocks.empty

  let hash_blocks = Cache.HashBlocks.create ~trigger_routine_from_start:true

  let participations =
    Cache.Participations.create ~trigger_routine_from_start:false

  let delegates : string list ConcurrentValue.t = ConcurrentValue.create []

  let delegates_info =
    Cache.DelegatesInfo.create ~trigger_routine_from_start:false

  let validators = ConcurrentValue.create Cache.Validators.empty

  let validators_by_fst_slot =
    ConcurrentValue.create Cache.ValidatorByFirstSlot.empty

  let votings = ConcurrentValue.create Cache.Votings.empty

  let empty_all () =
    (* clear all caches declared above *)
    let%lwt () =
      ConcurrentValue.update lr_blocks @@ fun _ ->
      Lwt.return Cache.LevelRoundBlocks.empty in
    let () = Cache.HashBlocks.clear hash_blocks in
    let () = Cache.Participations.clear participations in
    let%lwt () = ConcurrentValue.update delegates @@ fun _ -> Lwt.return_nil in
    let () = Cache.DelegatesInfo.clear delegates_info in
    let%lwt () =
      ConcurrentValue.update validators @@ fun _ ->
      Lwt.return Cache.Validators.empty in
    let%lwt () =
      ConcurrentValue.update validators_by_fst_slot @@ fun _ ->
      Lwt.return Cache.ValidatorByFirstSlot.empty in
    let%lwt () =
      ConcurrentValue.update votings @@ fun _ -> Lwt.return Cache.Votings.empty
    in
    Lwt.return_unit
end

(* ============================== PERSISTENCY ============================== *)

module Persistency = struct
  module Address = Lib_js.Storage.MakeString (struct
    let name = "persistent-address-explorus"
  end)

  module Network = Lib_js.Storage.MakeString (struct
    let name = "persistent-network-explorus"
  end)

  let clear_all_persistency_related_to_networks () =
    (* do not use localStorage.clear() directly : too aggressive *)
    Address.remove () ;
    Network.remove ()
end

module Settings = struct
  module HashShortening = struct
    type kind =
      | End
      | Middle

    let default = End

    let all_kind = [End; Middle]

    let to_string = function
      | End -> "End"
      | Middle -> "Middle"

    let of_string = function
      | "End" -> End
      | "Middle" -> Middle
      | kind ->
        failwith
        @@ Printf.sprintf "%s: persistency error, unknown hash shortening: %s"
             __FUNCTION__ kind

    let shorten_tz = function
      | End -> Pretty.shorten_end 7
      | Middle -> Pretty.shorten_middle 5 3

    let shorten_commit = function
      | End -> Pretty.shorten_end 8
      | Middle -> Pretty.shorten_middle 4 4

    let shorten_hash = function
      | End -> Pretty.shorten_end 10
      | Middle -> Pretty.shorten_middle 6 4

    let shorten_protocol = function
      | End | Middle -> Pretty.shorten_end 12

    module Persistency = Lib_js.Storage.Make (struct
      type t = kind

      let name = "persistent-setting-hash-shortening"

      let to_string = to_string

      let of_string = of_string
    end)

    let listener : kind Listener.t = Listener.create ()

    let current =
      let current = try Persistency.get () with _ -> default in
      let _init_listener = Listener.trigger listener current in
      ref current

    let set kind =
      Persistency.set kind ;
      current := kind ;
      Listener.trigger listener kind

    let get () = !current
  end
end

(* ================================= DEBUG ================================= *)

module Debug : sig
  type verbose =
    | Default
    | Dangerous_access
    | Monitor_ops
    | Process
    | Rpc

  val verbose : unit -> verbose

  val log : ?verbose:verbose -> string -> unit

  val log_f : ?verbose:verbose -> ('a, out_channel, unit) format -> 'a
end = struct
  (* All functions that contain a "dangerous" function like List.nth and do not
     have a default case value (i.e : it does not make sense to use
     List.nth_opt) will have a possibility to give more information about where
     they are called, just change the value below.

     verbose = Default --> no additional info

     verbose = Dangerous_access --> functions that use 'dangerous access'

     verbose = Monitor_ops --> monitor ops stream infos

     verbose = Process --> process infos

     verbose = Rpc --> ez_api rpc getters infos *)

  type verbose =
    | Default
    | Dangerous_access
    | Monitor_ops
    | Process
    | Rpc

  let verbose = ref Default

  module VerboseStorage = Lib_js.Storage.Make (struct
    type t = verbose

    let name = "persistent-debug"

    let to_string = function
      | Default -> "Default"
      | Dangerous_access -> "Dangerous_access"
      | Monitor_ops -> "Monitor_ops"
      | Process -> "Process"
      | Rpc -> "Rpc"

    let of_string = function
      | "Default" -> Default
      | "Dangerous_access" -> Dangerous_access
      | "Monitor_ops" -> Monitor_ops
      | "Process" -> Process
      | "Rpc" -> Rpc
      | _ -> Default
  end)

  let set_verbose () =
    verbose := Option.value ~default:Default @@ VerboseStorage.get_opt ()

  let _verbose_routine =
    Js_of_ocaml.Dom_html.window##setInterval
      (Ezjs_min.wrap_callback set_verbose)
      (Time.seconds_to_ms 5.0)
    |> ignore

  let verbose () = !verbose

  let log ?verbose:(v = Default) s = if verbose () = v then Log.log s

  let log_f ?verbose:(v = Default) format =
    if verbose () = v then
      Log.log_f format
    else
      (* Ignoring *)
      Printf.ifprintf stdout format
end

(* ================================ NETWORK ================================ *)

module Network = struct
  open DJson
  open Json_types

  let network_constants =
    ref
      Tezos.RPC.Types.
        {
          nc_blocks_per_cycle = 0;
          nc_baking_reward_fixed_portion = None;
          nc_baking_reward_bonus_per_slot = None;
          nc_issuance_weights = None;
          nc_liquidity_baking_subsidy = None;
          nc_consensus_committee_size = 0;
          nc_consensus_threshold = 0;
          nc_minimal_block_delay = 0;
          nc_delay_increment_per_round = 0;
          nc_dal_parametric =
            Some
              {
                dp_feature_enable = false;
                dp_number_of_slots = 0;
                dp_attestation_lag = 0;
              };
          (* @todo: remove the line below.

             Currently named [sc_rollup_commitment_period_in_blocks] on mainnet,
             thus this ugly hack: *)
          nc_smart_rollup_timeout_period_in_blocks = Some 500;
          nc_smart_rollup_commitment_period_in_blocks = Some 30;
          nc_smart_rollup_challenge_window_in_blocks = Some 80640;
          nc_adaptive_rewards_params = None;
        }

  let baking_reward_fixed_portion = ref 0.

  let baking_reward_bonus_per_slot = ref 0.

  let get_network_constants () = !network_constants

  let get_baking_reward_fixed_portion () = !baking_reward_fixed_portion

  let get_baking_reward_bonus_per_slot () = !baking_reward_bonus_per_slot

  let set_network_constants nc =
    network_constants := nc ;
    let open Tezos.RPC.Types in
    match nc with
    | {
     nc_minimal_block_delay;
     nc_consensus_committee_size;
     nc_consensus_threshold;
     nc_issuance_weights =
       Some
         {
           rw_base_total_issued_per_minute;
           rw_baking_reward_fixed_portion_weight;
           rw_baking_reward_bonus_weight;
           rw_attesting_reward_weight;
           rw_liquidity_baking_subsidy_weight;
           rw_seed_nonce_revelation_tip_weight;
           rw_vdf_revelation_tip_weight;
         };
     nc_liquidity_baking_subsidy;
     _;
    } ->
      let liquidity_baking_subsidy_weight =
        match
          (rw_liquidity_baking_subsidy_weight, nc_liquidity_baking_subsidy)
        with
        | _, Some lbs -> Z.to_float lbs
        | Some lbsw, _ -> lbsw
        | _ ->
          Log.log_exn ~msg:"No liquidity_baking_subsidy" ~__FUNCTION__
            ~exn:Not_found () ;
          0. in
      let minimal_block_delay = Float.of_int nc_minimal_block_delay in
      baking_reward_fixed_portion :=
        Lib_tz.Reward.baking_reward_fixed_portion ~minimal_block_delay
          ~base_total_rewards_per_minute:rw_base_total_issued_per_minute
          ~baking_reward_fixed_portion_weight:
            rw_baking_reward_fixed_portion_weight
          ~baking_reward_bonus_weight:rw_baking_reward_bonus_weight
          ~attesting_reward_weight:rw_attesting_reward_weight
          ~liquidity_baking_subsidy_weight
          ~seed_nonce_revelation_tip_weight:rw_seed_nonce_revelation_tip_weight
          ~vdf_revelation_tip_weight:rw_vdf_revelation_tip_weight ;
      let consensus_committee_size = Float.of_int nc_consensus_committee_size in
      let consensus_threshold = Float.of_int nc_consensus_threshold in
      baking_reward_bonus_per_slot :=
        Lib_tz.Reward.baking_reward_bonus_per_slot ~minimal_block_delay
          ~consensus_committee_size ~consensus_threshold
          ~base_total_rewards_per_minute:rw_base_total_issued_per_minute
          ~baking_reward_fixed_portion_weight:
            rw_baking_reward_fixed_portion_weight
          ~baking_reward_bonus_weight:rw_baking_reward_bonus_weight
          ~attesting_reward_weight:rw_attesting_reward_weight
          ~liquidity_baking_subsidy_weight
          ~seed_nonce_revelation_tip_weight:rw_seed_nonce_revelation_tip_weight
          ~vdf_revelation_tip_weight:rw_vdf_revelation_tip_weight
    | {
     nc_baking_reward_fixed_portion = Some fixed_portion;
     nc_baking_reward_bonus_per_slot = Some bonus_per_slot;
     _;
    } ->
      baking_reward_fixed_portion := fixed_portion ;
      baking_reward_bonus_per_slot := bonus_per_slot
    | _ ->
      baking_reward_fixed_portion := 0. ;
      baking_reward_bonus_per_slot := 0.

  type network =
    | Mainnet
    | Ghostnet
    | Weeklynet
    | Genericnet_one
    | Genericnet_two
    | Genericnet_three
    | Genericnet_four
    | Customnet

  let network_to_string = function
    | Mainnet -> "mainnet"
    | Ghostnet -> "ghostnet"
    | Weeklynet -> "weeklynet"
    | Genericnet_one -> "quebecnet"
    | Genericnet_two -> "rionet"
    | Genericnet_three -> "nextnet"
    | Genericnet_four | Customnet -> "customnet"

  exception Unknown_network

  let string_to_network = function
    | "mainnet" -> Mainnet
    | "ghostnet" -> Ghostnet
    | "weeklynet" -> Weeklynet
    | "quebecnet" -> Genericnet_one
    | "rionet" -> Genericnet_two
    | "nextnet" ->
      Genericnet_three
      (* All unknown network are considered as custom. The validity of the
         address must be verified *)
    | _ -> Customnet

  let chain_name_to_network chain_name =
    let%lwt parameters = parameters in
    let name_of_opt_network =
      Option.map @@ fun { chain_name; _ } -> chain_name in
    let networks_by_parameter =
      [
        (Some parameters.mainnet, Mainnet);
        (parameters.ghostnet, Ghostnet);
        (parameters.weeklynet, Weeklynet);
        (parameters.genericnet_one, Genericnet_one);
        (parameters.genericnet_two, Genericnet_two);
        (parameters.genericnet_three, Genericnet_three);
        (parameters.genericnet_four, Genericnet_four);
      ] in
    let networks_by_name =
      List.map
        (fun (infos, network) -> (name_of_opt_network infos, network))
        networks_by_parameter in
    match List.assoc_opt (Some chain_name) networks_by_name with
    | None -> Lwt.fail Unknown_network
    | Some network -> Lwt.return network

  let get_network_state_from_persistency () =
    Option.perform ~default:Mainnet ~perform:string_to_network
    @@ Persistency.Network.get_opt ()
  (* by default mainnet to avoid crashes *)

  let network_state = ref (get_network_state_from_persistency ())

  let get_network_state () = !network_state

  let set_network_state_from_persistency () =
    network_state := get_network_state_from_persistency ()

  let enabled_networks =
    let%lwt parameters = parameters in
    let networks = [] in
    let networks =
      if Option.is_some parameters.genericnet_four then
        Genericnet_four :: networks
      else
        networks in
    let networks =
      if Option.is_some parameters.genericnet_three then
        Genericnet_three :: networks
      else
        networks in
    let networks =
      if Option.is_some parameters.genericnet_two then
        Genericnet_two :: networks
      else
        networks in
    let networks =
      if Option.is_some parameters.genericnet_one then
        Genericnet_one :: networks
      else
        networks in
    let networks =
      if Option.is_some parameters.weeklynet then
        Weeklynet :: networks
      else
        networks in
    let networks =
      if Option.is_some parameters.ghostnet then
        Ghostnet :: networks
      else
        networks in
    Lwt.return (Mainnet :: networks)

  exception Reload

  (** [network_mutex] has for purpose to prevent unwanted/misleading quarantine
      triggers during the process of setting a new network *)
  let network_mutex = Lwt_mutex.create ()

  let set_network_state ?(force_reload = true) network =
    let%lwt () = Lwt_mutex.lock network_mutex in
    let set_and_reload network =
      network_state := network ;
      Persistency.Network.set (network_to_string network) ;
      if force_reload then (
        Custom_router.reload_with_current_path () ;
        Lwt.fail Reload
      ) else
        (* Normally we'd unlock the mutex by doing : [Lwt_mutex.unlock
           network_mutex], but the purpose here is to prevent any misleading
           quarantine during the network setting process *)
        Lwt.return_unit in
    match network with
    | Customnet -> set_and_reload Customnet
    | network ->
      (* safety : *)
      Persistency.Address.remove () ;
      set_and_reload network
end

(* ================================ PROTOCOL ================================ *)

module Protocol : sig
  val get_current : unit -> Protocol.t

  val set_current : Protocol.t -> unit

  val get_transition : unit -> int

  val set_transition : int -> unit
end = struct
  let current = ref Protocol.default

  let get_current () = !current

  let set_current p = current := p

  let transition = ref 0

  let get_transition () = !transition

  let set_transition p = transition := p
end

(* ================================== API ================================== *)

module API = struct
  open DJson
  open Json_types
  open Network

  module State = struct
    type 'a t = 'a CircularList.t networks

    let states =
      let%lwt parameters = parameters in
      let init_opt net =
        match net with
        | None -> None
        | Some net -> Some (CircularList.init net.addresses) in
      Lwt.return
      @@ ref
           {
             mainnet = CircularList.init parameters.mainnet.addresses;
             ghostnet = init_opt parameters.ghostnet;
             weeklynet = init_opt parameters.weeklynet;
             genericnet_one = init_opt parameters.genericnet_one;
             genericnet_two = init_opt parameters.genericnet_two;
             genericnet_three = init_opt parameters.genericnet_three;
             genericnet_four = init_opt parameters.genericnet_four;
           }

    let get () =
      let%lwt states = states in
      Lwt.return
      @@
      match get_network_state () with
      | Mainnet -> Some !states.mainnet
      | Ghostnet -> !states.ghostnet
      | Weeklynet -> !states.weeklynet
      | Genericnet_one -> !states.genericnet_one
      | Genericnet_two -> !states.genericnet_two
      | Genericnet_three -> !states.genericnet_three
      | Genericnet_four -> !states.genericnet_four
      | Customnet -> None

    let address () =
      match%lwt get () with
      (* None case is just for safety *)
      | None ->
        let%lwt states = states in
        Lwt.return @@ CircularList.get !states.mainnet
      | Some state -> Lwt.return @@ CircularList.get state

    module Quarantine : sig
      val is_not_triggered : unit -> bool

      val trigger : unit -> unit
    end = struct
      let max_time = 60

      let timer = ref 0

      (* or : has_already_finished *)
      let is_not_triggered () = !timer = 0

      let rec trigger () =
        if !timer = max_time then
          timer := 0
        else
          Lwt.async_exn ~__FUNCTION__ @@ fun _ ->
          let%lwt _sleep = Js_of_ocaml_lwt.Lwt_js.sleep 1. in
          incr timer ;
          Lwt.return @@ trigger ()
    end

    let network_abort = ref false

    let abort_if cond = if cond then network_abort := true

    let abort ?(force = false) () =
      if !network_abort || force then begin
        let force_msg =
          if force then
            "with"
          else
            "without" in
        Log.log_f "%s %s force\n" __FUNCTION__ force_msg ;
        Persistency.clear_all_persistency_related_to_networks () ;
        Custom_router.reload_with_current_path ()
      end

    let quarantine () =
      let%lwt () = Lwt_mutex.lock network_mutex in
      if Quarantine.is_not_triggered () then (
        Quarantine.trigger () ;
        let%lwt states = states in
        let next_opt st =
          match st with
          | None -> None
          | Some st ->
            (* @todo [1] : we have quarantine our last address, so we abort to
               avoid a potential infinite loop, a better thing to do will be to
               maintain a 'quarantined list' and see if there is still an
               address available in our address list that isn't in the
               'quarantined list' before aborting *)
            (* @todo [2] : we implicitly try to fallback on mainnet by doing so,
               but what if the same happens in mainnet, we need to pick randomly
               an other network to set to avoid infinite loop as well, this is a
               naive implementation to avoid infinite loop by analysing
               reccurent pattern on explorus, but it isn't resilient enough to
               completely avoid infinite loop crashes *)
            abort_if CircularList.(position st = length st - 1) ;
            Some (CircularList.next st) in
        let () =
          match get_network_state () with
          | Mainnet ->
            states :=
              { !states with mainnet = CircularList.next !states.mainnet }
          | Ghostnet ->
            states := { !states with ghostnet = next_opt !states.ghostnet }
          | Weeklynet ->
            states := { !states with weeklynet = next_opt !states.weeklynet }
          | Genericnet_one ->
            states :=
              { !states with genericnet_one = next_opt !states.genericnet_one }
          | Genericnet_two ->
            states :=
              { !states with genericnet_two = next_opt !states.genericnet_two }
          | Genericnet_three ->
            states :=
              {
                !states with
                genericnet_three = next_opt !states.genericnet_three;
              }
          | Genericnet_four ->
            states :=
              {
                !states with
                genericnet_four = next_opt !states.genericnet_four;
              }
          | Customnet ->
            Log.log @@ __FUNCTION__
            ^ " : trying to quarantine a custom address, try to change it in \
               settings or reset the address" in
        Lwt_mutex.unlock network_mutex ;
        Lwt.return_unit
      ) else (
        Lwt_mutex.unlock network_mutex ;
        Lwt.return_unit
      )
  end

  let set_custom_address_persistent_on_success address =
    Persistency.Network.set (network_to_string Customnet) ;
    Persistency.Address.set address

  let api () =
    match get_network_state () with
    | Customnet ->
      let persistent_address = Persistency.Address.get () in
      if persistent_address <> "" then
        Lwt.return persistent_address
      else
        (* avoid crash *)
        State.address ()
    | _ -> State.address ()

  let set_to_mainnet () = set_network_state Mainnet

  let fall_back_to_safenet () =
    Log.log @@ __FUNCTION__ ^ " : failed" ;
    set_to_mainnet ()

  let set_to_ghostnet () =
    let%lwt parameters = parameters in
    match parameters.ghostnet with
    | None -> fall_back_to_safenet ()
    | Some _ghostnet -> set_network_state Ghostnet

  let set_to_weeklynet () =
    let%lwt parameters = parameters in
    match parameters.weeklynet with
    | None -> fall_back_to_safenet ()
    | Some _weeklynet -> set_network_state Weeklynet

  let set_to_genericnet_one () =
    let%lwt parameters = parameters in
    match parameters.genericnet_one with
    | None -> fall_back_to_safenet ()
    | Some _genericnet_one -> set_network_state Genericnet_one

  let set_to_genericnet_two () =
    let%lwt parameters = parameters in
    match parameters.genericnet_two with
    | None -> fall_back_to_safenet ()
    | Some _genericnet_two -> set_network_state Genericnet_two

  let set_to_genericnet_three () =
    let%lwt parameters = parameters in
    match parameters.genericnet_three with
    | None -> fall_back_to_safenet ()
    | Some _genericnet_three -> set_network_state Genericnet_three

  let set_to_genericnet_four () =
    let%lwt parameters = parameters in
    match parameters.genericnet_four with
    | None -> fall_back_to_safenet ()
    | Some _genericnet_four -> set_network_state Genericnet_four

  let set_to_customnet () = set_network_state Customnet

  let reset_custom_address () =
    Lwt.async_exn ~__FUNCTION__ @@ fun _ ->
    Persistency.Address.remove () ;
    Persistency.Network.remove () ;
    Lwt.return_unit
end
