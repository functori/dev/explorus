# STATIC

This folder contains all the static files that will be used by the final app, it is split in 4
folders:

-   `css` contains all the CSS used in the `.html` files.
-   `img` contains either icons or logos.
-   `js` contains the generated runtime files in order for explorus to be launched on your browser.
-   `json` contains all the necessary configuration files that are automatically paste from the
    `json` folder at the root of this project.

This folder also contain the main page `index.html` of explorus that enable every static files.
