(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2022-2023, Functori <contact@functori.com>                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* may be worth checking :
   https://ocaml.janestreet.com/ocaml-core/109.20.00/doc/core_extended/Cache.S.html *)

module HashBlocks = Tempotbl.Make (struct
  type key = string

  type data = block

  let capacity = Konstant.medium_cache_size

  let duration = Konstant.upper_bound_duration

  let update_on_access = true

  let sleep = Js_of_ocaml_lwt.Lwt_js.sleep
end)

module LevelRoundBlocks = struct
  module LevelMap =
    BoundedMap.Make
      (LevelMap)
      (struct
        let capacity = Konstant.small_cache_size
      end)

  type t = block RoundMap.t LevelMap.t

  let empty : t = LevelMap.empty

  let update LevelRound.{ level; round } block : t -> t =
    LevelMap.update_with_default level ~default:RoundMap.empty
    @@ RoundMap.add round block

  let find_opt LevelRound.{ level; round } (cache : t) =
    Option.bind (LevelMap.find_opt level cache) (RoundMap.find_opt round)

  let has_been_seen cache level_round =
    Option.is_some @@ find_opt level_round cache

  exception No_block_found

  let get level_round ?search_for cache =
    match find_opt level_round cache with
    | None ->
      Option.perform_delayed
        ~default:(fun () -> raise No_block_found)
        ~perform:(fun search_for -> search_for level_round)
        search_for
    | Some block -> Lwt.return block

  let find_head ~default (cache : t) =
    match LevelMap.max_binding_opt cache with
    | None -> default ()
    | Some (_key, lvl_map) -> (
      match RoundMap.max_binding_opt lvl_map with
      | None -> default ()
      | Some (_key, block) -> Lwt.return block)

  let is_empty (cache : t) = LevelMap.cardinal cache > 0

  let iter_intra_lwt f (cache : t) =
    let cache_l = LevelMap.bindings cache in
    Lwt_list.iter_s
      (fun (_level, map) ->
        let map_l = RoundMap.bindings map in
        Lwt_list.iter_s (fun (_round, block) -> f block) map_l)
      cache_l
end

module Participations = Tempotbl.Make (struct
  type key = string

  type data = participation

  (* I based the capacity on the amount of bakers on mainnet *)
  let capacity = Konstant.extra_large_cache_size

  (* [x -. 5.] is just in case, it does not change the behaviour in any case,
     it's just to let a margin between attesting refresh cycle and cache
     update *)
  let duration = Konstant.attesting_refresh_cyle_duration -. 5.

  let update_on_access = false

  let sleep = Js_of_ocaml_lwt.Lwt_js.sleep
end)

module DelegatesInfo = Tempotbl.Make (struct
  type key = string

  type data = delegate_info

  (* I based the capacity on the amount of bakers on mainnet *)
  let capacity = Konstant.extra_large_cache_size

  (* [x -. 5.] is just in case, it does not change the behaviour in any case,
     it's just to let a margin between attesting refresh cycle and cache
     update *)
  let duration = Konstant.attesting_refresh_cyle_duration -. 5.

  let update_on_access = false

  let sleep = Js_of_ocaml_lwt.Lwt_js.sleep
end)

module Validators = struct
  module LevelMap =
    BoundedMap.Make
      (LevelMap)
      (struct
        let capacity = Konstant.medium_cache_size
      end)

  type t = validators LevelMap.t

  let empty : t = LevelMap.empty

  let update level validators cache = LevelMap.add level validators cache

  let get level search_for cache =
    match LevelMap.find_opt level cache with
    | None -> search_for level
    | Some validators -> Lwt.return validators
end

module ValidatorByFirstSlot = struct
  module LevelMap =
    BoundedMap.Make
      (LevelMap)
      (struct
        let capacity = Konstant.medium_cache_size
      end)

  module SlotMap = Map.Make (Int)

  type t = validator SlotMap.t LevelMap.t

  let empty : t = LevelMap.empty

  let update ~level first_slots cache = LevelMap.add level first_slots cache

  let map_validators_by_first_slots validators =
    List.fold_left
      (fun map validator ->
        match validator.val_slots with
        | [] -> map
        | slot :: _ -> SlotMap.add slot validator map)
      SlotMap.empty validators

  let get ~level search_for cache =
    match LevelMap.find_opt level cache with
    | None ->
      let%lwt validators = search_for level in
      let first_slots = map_validators_by_first_slots validators in
      Lwt.return first_slots
    | Some first_slots -> Lwt.return first_slots

  let find_validator ~slot first_slots = SlotMap.find_opt slot first_slots
end

module Ballots = struct
  type t = {
    counter : int;
    voters : AttesterSet.t;
  }

  let empty = { counter = 0; voters = AttesterSet.empty }

  let vote voter weight ballots =
    if AttesterSet.mem voter ballots.voters then
      ballots
    else
      {
        counter = ballots.counter + weight;
        voters = AttesterSet.add voter ballots.voters;
      }
end

module Voting = struct
  type t = Ballots.t AttesPreattes.t

  let empty =
    AttesPreattes.
      { preattestation = Ballots.empty; attestation = Ballots.empty }

  let vote ~kind { val_slots; val_delegate; _ } voting =
    let weight = List.length val_slots in
    AttesPreattes.map_with_kind ~kind (Ballots.vote val_delegate weight) voting
end

module Votings = struct
  module LevelMap =
    BoundedMap.Make
      (LevelMap)
      (struct
        let capacity = Konstant.medium_cache_size
      end)

  type t = Voting.t RoundMap.t LevelMap.t

  let get_voting LevelRound.{ level; round } votings =
    let roundmap_opt = LevelMap.find_opt level votings in
    let voting_opt = Option.bind roundmap_opt @@ RoundMap.find_opt round in
    Option.value ~default:Voting.empty voting_opt

  let vote ~kind LevelRound.{ level; round } validator =
    LevelMap.update_with_default level ~default:RoundMap.empty
    @@ RoundMap.update_with_default round ~default:Voting.empty
    @@ Voting.vote ~kind validator

  let empty : t = LevelMap.empty
end
