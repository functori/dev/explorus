#!/usr/bin/env python3

##############################################################################
#                                                                            #
# MIT License                                                                #
#                                                                            #
# Copyright (c) 2023, Functori <contact@functori.com>                        #
#                                                                            #
# Permission is hereby granted, free of charge, to any person obtaining a    #
# copy of this software and associated documentation files (the "Software"), #
# to deal in the Software without restriction, including without limitation  #
# the rights to use, copy, modify, merge, publish, distribute, sublicense,   #
# and/or sell copies of the Software, and to permit persons to whom the      #
# Software is furnished to do so, subject to the following conditions:       #
#                                                                            #
# The above copyright notice and this permission notice shall be included in #
# all copies or substantial portions of the Software.                        #
#                                                                            #
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR #
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   #
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    #
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER  #
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    #
# FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         #
# DEALINGS IN THE SOFTWARE.                                                  #
#                                                                            #
##############################################################################   

from __future__ import print_function
from docutils import core
from docutils.writers.html4css1 import Writer,HTMLTranslator
import sys, os

class HTMLFragmentTranslator( HTMLTranslator ):
    def __init__( self, document ):
        HTMLTranslator.__init__( self, document )
        self.head_prefix = ['','','','','']
        self.body_prefix = []
        self.body_suffix = []
        self.stylesheet = []
    def astext(self):
        return ''.join(self.body)

html_fragment_writer = Writer()
html_fragment_writer.translator_class = HTMLFragmentTranslator

def reST_to_html( s ):
    return core.publish_string( s, writer = html_fragment_writer )

if __name__ == '__main__':
    if len(sys.argv)>1:
        if sys.argv[1] != "":
            rstfile = open(sys.argv[1])
            text = rstfile.read()
            rstfile.close()
            if len(sys.argv)>2:
                if sys.argv[2] != "":
                    htmlfile = sys.argv[2]
            else:
                htmlfile = os.path.splitext(os.path.basename(sys.argv[1]))[0]+".html"
            result = reST_to_html(text)
            output = open(htmlfile, "wb")
            output.write(result)
            print("Your file was successfully converted.")
            output.close()  
    else:
        print("Usage:\rst2html.py docname.rst\nwhich results in => docname.html\ncompile_rst.py docname.rst desiredname.html\nwhich results in => desiredname.html")
